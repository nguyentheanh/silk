-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 28, 2019 at 04:31 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-10+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `silk`
--

-- --------------------------------------------------------

--
-- Table structure for table `ap_account`
--

CREATE TABLE `ap_account` (
  `id` int(11) UNSIGNED NOT NULL,
  `oauth_provider` varchar(50) DEFAULT NULL COMMENT 'Tên social',
  `oauth_uid` varchar(100) DEFAULT NULL COMMENT 'uid',
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL COMMENT '1:hoạt động: 0 Ngừng hoạt động,2:chuyển về thùng rác',
  `full_name` varchar(100) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `avatar` varchar(100) DEFAULT NULL,
  `social` varchar(255) DEFAULT NULL,
  `address` text,
  `gender` tinyint(3) DEFAULT '1' COMMENT '1: Nam 2: Nữ 3: Còn lại',
  `birthday` date DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `district` int(11) DEFAULT NULL,
  `introduce` text COMMENT 'Giới thiệu',
  `cmnd` varchar(50) DEFAULT NULL,
  `taxcode` varchar(100) DEFAULT NULL,
  `bank_account` varchar(200) DEFAULT NULL,
  `introduce_yourself` text,
  `experience` varchar(200) DEFAULT NULL,
  `send_mail` tinyint(1) DEFAULT '0' COMMENT '0 chưa gửi, 1 gửi rồi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ap_account_groups`
--

CREATE TABLE `ap_account_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL COMMENT '1:học viên, 2 giảng viên, 3 cộng tác viên'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ap_agency`
--

CREATE TABLE `ap_agency` (
  `id` int(11) NOT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `lat` varchar(25) DEFAULT NULL,
  `lng` varchar(25) DEFAULT NULL,
  `is_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_agency_translations`
--

CREATE TABLE `ap_agency_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(5) DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `address` varchar(500) NOT NULL,
  `slug` varchar(225) DEFAULT NULL,
  `title` varchar(225) DEFAULT NULL,
  `description` mediumtext,
  `content` longtext,
  `meta_title` varchar(225) DEFAULT NULL,
  `meta_description` varchar(225) DEFAULT NULL,
  `meta_keyword` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_banner`
--

CREATE TABLE `ap_banner` (
  `id` int(11) NOT NULL,
  `property_id` int(5) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(10) DEFAULT '0',
  `url_video` varchar(255) DEFAULT NULL,
  `script` text,
  `is_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '2' COMMENT 'trang thai',
  `displayed_time` datetime NOT NULL COMMENT 'ngay publish',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngay tao',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngay sua'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_banner`
--

INSERT INTO `ap_banner` (`id`, `property_id`, `thumbnail`, `url`, `order`, `url_video`, `script`, `is_status`, `displayed_time`, `created_time`, `updated_time`) VALUES
(1, 1, 'home.jpg', '', 0, '', NULL, 1, '0000-00-00 00:00:00', '2019-08-23 19:32:41', '2019-08-23 19:32:41'),
(2, 1, 'home.jpg', '', 0, '', NULL, 1, '0000-00-00 00:00:00', '2019-08-23 19:32:54', '2019-08-23 19:32:54'),
(3, 2, 'home.jpg', '', 0, '', NULL, 1, '0000-00-00 00:00:00', '2019-08-23 20:38:21', '2019-08-23 20:38:21');

-- --------------------------------------------------------

--
-- Table structure for table `ap_banner_translations`
--

CREATE TABLE `ap_banner_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(5) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `content` longtext,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(170) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_banner_translations`
--

INSERT INTO `ap_banner_translations` (`id`, `language_code`, `slug`, `title`, `description`, `content`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 'vi', NULL, 'Banner 1', 'Banner 1', NULL, NULL, NULL, NULL),
(1, 'en', NULL, 'Banner 1', 'Banner 1', NULL, NULL, NULL, NULL),
(2, 'vi', NULL, 'Banner 2', 'Banner 2', NULL, NULL, NULL, NULL),
(2, 'en', NULL, 'Banner 2', 'Banner 2', NULL, NULL, NULL, NULL),
(3, 'vi', NULL, 'Banner trang chủ1', 'Banner trang chủ1', NULL, NULL, NULL, NULL),
(3, 'en', NULL, 'Banner trang chủ1', 'Banner trang chủ1', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_category`
--

CREATE TABLE `ap_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `is_featured` varchar(255) DEFAULT NULL COMMENT 'Background for parent_id = 0',
  `files` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL COMMENT 'style html',
  `class` varchar(100) DEFAULT NULL,
  `type` varchar(255) DEFAULT 'post' COMMENT 'type theo đúng tên controller',
  `order` int(3) DEFAULT '0',
  `is_status` tinyint(2) NOT NULL DEFAULT '1',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL,
  `updated_by` varchar(255) NOT NULL,
  `ratting` mediumtext,
  `retionship` varchar(120) DEFAULT NULL,
  `question` mediumtext,
  `url_video` varchar(50) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `i_con` varchar(100) DEFAULT NULL,
  `icon_2` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_category`
--

INSERT INTO `ap_category` (`id`, `parent_id`, `thumbnail`, `banner`, `is_featured`, `files`, `style`, `class`, `type`, `order`, `is_status`, `created_time`, `updated_time`, `created_by`, `updated_by`, `ratting`, `retionship`, `question`, `url_video`, `link`, `i_con`, `icon_2`) VALUES
(1, 0, '', '', NULL, NULL, NULL, NULL, 'post', 1, 1, '2019-08-23 16:05:53', '2019-08-23 16:05:53', '', '', NULL, NULL, NULL, NULL, NULL, '', ''),
(2, 0, 'home.jpg', '', NULL, NULL, NULL, NULL, 'post', 2, 1, '2019-08-23 16:24:32', '2019-08-23 16:46:27', '', '', NULL, NULL, NULL, NULL, NULL, '', ''),
(3, 0, '', NULL, NULL, NULL, NULL, NULL, 'post', 3, 1, '2019-08-23 19:51:20', '2019-08-23 19:51:20', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 0, '69496333_473203900129947_795860631109500928_n.jpg', NULL, NULL, NULL, NULL, NULL, 'product', 1, 1, '2019-08-23 19:59:16', '2019-08-27 10:45:36', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 0, 'phuong_mai_silk_22.08.19_banner_lich_lam_hon_cung_ao_so_mi_lua_to_tam_no_prize.png', NULL, NULL, NULL, NULL, NULL, 'product', 2, 1, '2019-08-23 19:59:40', '2019-08-27 10:58:45', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 4, '02.jpg', NULL, NULL, NULL, NULL, NULL, 'product', 1, 1, '2019-08-23 20:00:03', '2019-08-23 20:00:03', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 4, '01.jpg', NULL, NULL, NULL, NULL, NULL, 'product', 2, 1, '2019-08-23 20:00:23', '2019-08-23 20:00:23', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 5, '01.jpg', NULL, NULL, NULL, NULL, NULL, 'product', 1, 1, '2019-08-23 20:00:50', '2019-08-23 20:00:50', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 5, '02.jpg', NULL, NULL, NULL, NULL, NULL, 'product', 2, 1, '2019-08-23 20:01:06', '2019-08-23 20:01:06', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 0, 'phuong_mai_silk_khan_choang_co_1x1_01_(1).png', NULL, NULL, NULL, NULL, NULL, 'product', 3, 1, '2019-08-23 20:01:55', '2019-08-27 11:25:17', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_category_translations`
--

CREATE TABLE `ap_category_translations` (
  `id` int(11) NOT NULL,
  `language_code` char(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `content` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_category_translations`
--

INSERT INTO `ap_category_translations` (`id`, `language_code`, `title`, `description`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `content`) VALUES
(1, 'en', 'Về chúng tôi', 'Về chúng tôi', 've-chung-toi', 'Về chúng tôi', 'Về chúng tôi', 'Về chúng tôi', ''),
(1, 'vi', 'Về chúng tôi', 'Về chúng tôi', 've-chung-toi', 'Về chúng tôi', 'Về chúng tôi', 'Về chúng tôi', ''),
(2, 'en', 'Tin tức', 'Tin tức', 'tin-tuc', 'Tin tức', 'Tin tức', 'Tin tức', ''),
(2, 'vi', 'Tin tức', 'Tin tức', 'tin-tuc', 'Tin tức', 'Tin tức', 'Tin tức', ''),
(3, 'en', 'Truyền thông nói gì về chúng tôi', 'Truyền thông nói gì về chúng tôi', 'truyen-thong-noi-gi-ve-chung-toi', 'Truyền thông nói gì về chúng tôi', 'Truyền thông nói gì về chúng tôi', 'Truyền thông nói gì về chúng tôi', ''),
(3, 'vi', 'Truyền thông nói gì về chúng tôi', 'Truyền thông nói gì về chúng tôi', 'truyen-thong-noi-gi-ve-chung-toi', 'Truyền thông nói gì về chúng tôi', 'Truyền thông nói gì về chúng tôi', 'Truyền thông nói gì về chúng tôi', ''),
(4, 'en', 'Danh mục sản phẩm 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-san-pham-1', 'Danh mục sản phẩm 1', 'Danh mục sản phẩm 1', 'Danh mục sản phẩm 1', ''),
(4, 'vi', 'Vải áo dài tơ tằm', 'Từ xa xưa, hình ảnh tà áo dài thướt tha đã gắn liền với nét duyên thầm của người con gái Việt Nam. Không chỉ là một điểm nhấn độc đáo trong văn hóa dân tộc, mà tà áo dài lụa là ấy còn khéo léo thể hiện dáng nét yêu kiều đặc trưng của người mặc.\r\n\r\nLà một chất liệu quý trong ngành dệt may, lụa tơ tằm được chứng minh là loại vải hàng đầu trong việc tạo nên được những chiếc áo dài đẹp nhất.', 'Danh mục vải áo dài tơ tằm', 'Vải lụa áo dài tơ tằm Phương Mai Silk toàn quốc', 'Danh mụcTừ xa xưa, hình ảnh tà áo dài thướt tha đã gắn liền với nét duyên thầm của người con gái Việt Nam. ', 'Áo dài tơ tằm', ''),
(5, 'en', 'Danh mục sản phẩm 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-san-pham-2', 'Danh mục sản phẩm 2', 'Danh mục sản phẩm 2', 'Danh mục sản phẩm 2', ''),
(5, 'vi', 'Áo Sơ Mi Nam tơ tằm', 'Chiếc áo sơ mi đã trở thành một trang phục quen thuộc, góp phần hoàn thiện cho phong cách và sự chỉnh chu của phái mạnh. Nếu cotton là chất liệu giúp quý ông tạm quên đi sự cẩn trọng đối với trang phục, thì một chiếc áo sơ mi lụa tơ tằm yêu cầu phải tỉ mỉ hơn rất nhiều.\r\nMang đặc trưng mềm mịn và có phần bóng bẩy, những chiếc áo sơ mi lụa tơ tằm độc đáo chính là thước đo nói lên sự thành thạo trong cách ăn mặc và chuẩn mực thời trang của phái mạnh.', 'Áo sơ mi nam tơ tằm Phương Mai Silk phân phối', 'Áo sơ mi nam tơ tằm Phương Mai Silk phân phối', 'Áo sơ mi nam tơ tằm Phương Mai Silk phân phối. Cam kết 100% tơ tằm nguyên chất, trân trọng giá trị truyền thống ngành tơ tằm', 'Sơ mi tơ tằm', ''),
(6, 'en', 'Danh mục con sản phẩm 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-con-san-pham-1', 'Danh mục con sản phẩm 1', 'Danh mục con sản phẩm 1', 'Danh mục con sản phẩm 1', ''),
(6, 'vi', 'Danh mục con sản phẩm 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-con-san-pham-1', 'Danh mục con sản phẩm 1', 'Danh mục con sản phẩm 1', 'Danh mục con sản phẩm 1,tag2', ''),
(7, 'en', 'Danh mục con sản phẩm 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-con-san-pham-2', 'Danh mục con sản phẩm 2', 'Danh mục con sản phẩm 2', 'Danh mục con sản phẩm 2', ''),
(7, 'vi', 'Danh mục con sản phẩm 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-con-san-pham-2', 'Danh mục con sản phẩm 2', 'Danh mục con sản phẩm 2', 'Danh mục con sản phẩm 2,tag2', ''),
(8, 'en', 'Danh mục con sản phẩm 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-con-san-pham-2', 'Danh mục con sản phẩm 2', 'Danh mục con sản phẩm 2', 'Danh mục con sản phẩm 2', ''),
(8, 'vi', 'Danh mục con sản phẩm 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-con-san-pham-2', 'Danh mục con sản phẩm 2', 'Danh mục con sản phẩm 2', 'Danh mục con sản phẩm 2', ''),
(9, 'en', 'Danh mục con sản phẩm 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-con-san-pham-3', 'Danh mục con sản phẩm 3', 'Danh mục con sản phẩm 3', 'Danh mục con sản phẩm 3', ''),
(9, 'vi', 'Danh mục con sản phẩm 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-con-san-pham-3', 'Danh mục con sản phẩm 3', 'Danh mục con sản phẩm 3', 'Danh mục con sản phẩm 3,tag2', ''),
(10, 'en', 'Danh mục sản phẩm 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'danh-muc-san-pham-3', 'Danh mục sản phẩm 3', 'Danh mục sản phẩm 3', 'Danh mục sản phẩm 3', ''),
(10, 'vi', 'Khăn lụa tơ tằm', 'Xuất phát là một phụ kiện thời trang cao cấp với họa tiết đa dạng, sành điệu, kết hợp cùng cùng công nghệ in hiện đại trên chất liệu lụa tơ tằm 100%. Khăn lụa tơ tằm Phương Mai Silk đại diện cho chuẩn mực mới của một phụ kiện thời trang, một điểm nhấn nổi bật không thể thiếu trong phong cách phục sức của phái đẹp.', 'Khăn lụa tơ tằm', 'Khăn lụa tơ tằm Phương Mai Silk', 'Khăn lụa tơ tằm Phương Mai Silk đại diện cho chuẩn mực mới của một phụ kiện thời trang, một điểm nhấn nổi bật không thể thiếu của phái đẹp.', 'Khăn lụa tơ tằm', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_contact`
--

CREATE TABLE `ap_contact` (
  `id` int(11) NOT NULL,
  `fullname` varchar(250) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `content` text,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_contact`
--

INSERT INTO `ap_contact` (`id`, `fullname`, `email`, `phone`, `content`, `created_time`, `title`) VALUES
(1, 'aaaaaa', NULL, '0968848712', '', '2019-08-23 14:59:20', NULL),
(2, 'âaaaa', NULL, '0968848712', '', '2019-08-23 15:02:10', NULL),
(3, 'âaaaa', NULL, '0968848712', '', '2019-08-23 15:02:11', NULL),
(4, 'âaaaa', NULL, '0968848712', '', '2019-08-23 15:02:11', NULL),
(5, 'aaaa', NULL, '0968848712', '', '2019-08-23 15:02:27', NULL),
(6, 'kexno', NULL, '0974301136', 'test', '2019-08-25 18:11:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_faqs`
--

CREATE TABLE `ap_faqs` (
  `id` int(11) NOT NULL,
  `is_status` tinyint(4) NOT NULL DEFAULT '2',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_faqs_translations`
--

CREATE TABLE `ap_faqs_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(5) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `content` longtext,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_groups`
--

CREATE TABLE `ap_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `permission` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_groups`
--

INSERT INTO `ap_groups` (`id`, `name`, `description`, `permission`) VALUES
(1, 'Admin', 'Administrator', '{\"banner\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"groups\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"media\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"menus\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"newsletter\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"page\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"post\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"product\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"property\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"setting\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"users\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"video\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"}}'),
(2, 'Biên tập viên', 'Nhóm biên tập quản trị nội dung web', '{\"banner\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"groups\":{\"view\":\"1\"},\"media\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"menus\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"page\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"post\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"setting\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"users\":{\"view\":\"1\"}}');

-- --------------------------------------------------------

--
-- Table structure for table `ap_group_members`
--

CREATE TABLE `ap_group_members` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `is_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '2' COMMENT 'trang thai 0:Huy 1:Hoat dong 2:Nhap ',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngay tao',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngay sua'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ap_location_city`
--

CREATE TABLE `ap_location_city` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `name_with_type` varchar(250) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `slug` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ap_location_district`
--

CREATE TABLE `ap_location_district` (
  `id` int(11) UNSIGNED NOT NULL,
  `city_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Tỉnh / TP Trực thuộc TW',
  `title` varchar(250) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `name_with_type` varchar(250) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `path_with_type` varchar(255) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `is_featured` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ap_location_ward`
--

CREATE TABLE `ap_location_ward` (
  `id` int(11) NOT NULL,
  `district_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Quận huyện',
  `title` varchar(250) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `name_with_type` varchar(250) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `path_with_type` varchar(255) DEFAULT NULL,
  `order` tinyint(1) DEFAULT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ap_logged_device`
--

CREATE TABLE `ap_logged_device` (
  `user_id` bigint(20) NOT NULL,
  `ip_address` varchar(45) NOT NULL COMMENT 'IP client',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `device_code` char(32) NOT NULL COMMENT 'Mã md5 thiết bị + key secret',
  `user_agent` varchar(255) NOT NULL COMMENT 'Tên thiết bị'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ap_login_attempts`
--

CREATE TABLE `ap_login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ap_log_action`
--

CREATE TABLE `ap_log_action` (
  `id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL,
  `uid` int(11) NOT NULL,
  `created_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_log_action`
--

INSERT INTO `ap_log_action` (`id`, `action`, `note`, `uid`, `created_time`) VALUES
(1, 'room', 'Update room: 2', 1, '2019-04-08 02:28:48'),
(2, 'property', 'Update property: 3', 1, '2019-04-08 04:20:27'),
(3, 'property', 'Update property: 4', 1, '2019-04-08 04:20:30'),
(4, 'property', 'Update property: 5', 1, '2019-04-08 04:20:33'),
(5, 'banner', 'Sửa Banner có id là 2', 1, '2019-04-09 10:46:37'),
(6, 'room', 'Update room: 1', 1, '2019-04-09 02:00:41'),
(7, 'room', 'Update room: 2', 1, '2019-04-09 02:00:46'),
(8, 'room', 'Update room: 16', 1, '2019-04-10 04:52:39'),
(9, 'renter', 'Update renter: 1', 1, '2019-04-11 11:05:20'),
(10, 'users', 'Insert users: 0', 1, '2019-04-11 02:36:28'),
(11, 'users', 'Update users: 37', 37, '2019-04-11 02:53:43'),
(12, 'users', 'Insert users: 0', 37, '2019-04-11 03:26:42'),
(13, 'groups', 'Insert groups: 3', 37, '2019-04-11 05:15:57'),
(14, 'groups', 'Update groups: 3', 37, '2019-04-11 05:20:39'),
(15, 'groups', 'Update groups: 3', 37, '2019-04-11 05:20:51'),
(16, 'groups', 'Insert groups: 4', 37, '2019-04-11 05:25:49'),
(17, 'groups', 'Update groups: 4', 37, '2019-04-11 05:25:57'),
(18, 'groups', 'Update groups: 3', 37, '2019-04-11 05:25:57'),
(19, 'groups', 'Insert groups: 5', 37, '2019-04-11 05:26:11'),
(20, 'groups', 'Update groups: 5', 37, '2019-04-11 05:26:16'),
(21, 'groups', 'Insert groups: 6', 37, '2019-04-11 05:27:18'),
(22, 'groups', 'Insert groups: 7', 37, '2019-04-11 05:27:20'),
(23, 'groups', 'Insert groups: 8', 37, '2019-04-11 05:27:23'),
(24, 'groups', 'Insert groups: 9', 37, '2019-04-11 05:27:26'),
(25, 'groups', 'Insert groups: 10', 37, '2019-04-11 05:27:28'),
(26, 'groups', 'Insert groups: 11', 37, '2019-04-11 05:27:37'),
(27, 'groups', 'Insert groups: 12', 37, '2019-04-11 05:27:40'),
(28, 'groups', 'Insert groups: 13', 37, '2019-04-11 05:27:43'),
(29, 'groups', 'Insert groups: 14', 37, '2019-04-11 05:27:46'),
(30, 'groups', 'Update groups: 14', 37, '2019-04-11 05:28:06'),
(31, 'groups', 'Update groups: 6', 37, '2019-04-11 05:28:25'),
(32, 'groups', 'Update groups: 7', 37, '2019-04-11 05:28:25'),
(33, 'groups', 'Update groups: 8', 37, '2019-04-11 05:28:25'),
(34, 'groups', 'Update groups: 9', 37, '2019-04-11 05:28:25'),
(35, 'groups', 'Update groups: 10', 37, '2019-04-11 05:28:25'),
(36, 'groups', 'Update groups: 11', 37, '2019-04-11 05:28:25'),
(37, 'groups', 'Update groups: 12', 37, '2019-04-11 05:28:25'),
(38, 'groups', 'Update groups: 13', 37, '2019-04-11 05:28:25'),
(39, 'groups', 'Insert groups: 15', 37, '2019-04-11 05:50:44'),
(40, 'groups', 'Update groups: 15', 37, '2019-04-11 05:55:52'),
(41, 'groups', 'Update groups: 15', 37, '2019-04-11 05:56:52'),
(42, 'groups', 'Update groups: 15', 37, '2019-04-11 05:58:08'),
(43, 'users', 'Update users: 38', 1, '2019-04-12 01:47:19'),
(44, 'users', 'Update users: 38', 38, '2019-04-12 01:47:48'),
(45, 'users', 'delete users: 37', 38, '2019-04-12 01:48:12'),
(46, 'unit', 'Insert unit: 1', 1, '2019-04-12 01:56:52'),
(47, 'users', 'Update users: 36', 1, '2019-04-12 02:04:50'),
(48, 'users', 'Update users: 38', 1, '2019-04-12 02:04:55'),
(49, 'users', 'Update users: 38', 1, '2019-04-12 02:17:37'),
(50, 'users', 'Update users: 38', 38, '2019-04-12 02:18:07'),
(51, 'users', 'Insert users: 0', 38, '2019-04-12 02:19:13'),
(52, 'users', 'Insert users: 0', 38, '2019-04-12 02:19:47'),
(53, 'users', 'Insert users: 0', 38, '2019-04-12 02:19:55'),
(54, 'users', 'Insert users: 0', 38, '2019-04-12 02:20:07'),
(55, 'users', 'Insert users: 0', 38, '2019-04-12 02:20:17'),
(56, 'users', 'Insert users: 0', 38, '2019-04-12 02:20:26'),
(57, 'users', 'Insert users: 0', 38, '2019-04-12 02:20:36'),
(58, 'users', 'delete users: 45', 38, '2019-04-12 02:21:21'),
(59, 'users', 'delete users: 44', 38, '2019-04-12 02:21:21'),
(60, 'users', 'delete users: 43', 38, '2019-04-12 02:21:21'),
(61, 'users', 'delete users: 42', 38, '2019-04-12 02:21:21'),
(62, 'users', 'delete users: 41', 38, '2019-04-12 02:21:21'),
(63, 'users', 'delete users: 40', 38, '2019-04-12 02:21:21'),
(64, 'users', 'delete users: 39', 38, '2019-04-12 02:21:45'),
(65, 'account', 'Update account: 19', 1, '2019-04-12 02:40:51'),
(66, 'account', 'Update account: 19', 1, '2019-04-12 02:41:09'),
(67, 'account', 'Insert account: 23', 1, '2019-04-12 02:49:24'),
(68, 'account', 'Update account: 23', 1, '2019-04-12 02:52:36'),
(69, 'account', 'Update account: 23', 1, '2019-04-12 02:53:10'),
(70, 'account', 'Update account: 23', 1, '2019-04-12 02:53:22'),
(71, 'account', 'Insert account: 24', 1, '2019-04-12 02:54:17'),
(72, 'account', 'Update account: 24', 1, '2019-04-12 02:54:59'),
(73, 'account', 'Update account: 24', 1, '2019-04-12 02:55:43'),
(74, 'account', 'Update account: 24', 1, '2019-04-12 02:56:07'),
(75, 'account', 'Update account: 23', 1, '2019-04-12 02:56:45'),
(76, 'account', 'Update account: 23', 1, '2019-04-12 02:57:43'),
(77, 'account', 'Update account: 24', 1, '2019-04-12 03:15:31'),
(78, 'account', 'Update account: 24', 1, '2019-04-12 03:17:26'),
(79, 'account', 'Update account: 24', 1, '2019-04-12 03:22:03'),
(80, 'account', 'Insert account: 25', 1, '2019-04-12 03:32:08'),
(81, 'account', 'Insert account: 26', 1, '2019-04-12 03:40:13'),
(82, 'account', 'Insert account: 27', 1, '2019-04-12 03:40:41'),
(83, 'account', 'Insert account: 28', 1, '2019-04-12 03:41:13'),
(84, 'account', 'Insert account: 29', 1, '2019-04-12 03:41:57'),
(85, 'account', 'Update account: 29', 1, '2019-04-12 03:47:53'),
(86, 'account', 'delete account: 29', 1, '2019-04-12 03:54:10'),
(87, 'account', 'delete account: 28', 1, '2019-04-12 03:54:10'),
(88, 'account', 'delete account: 27', 1, '2019-04-12 03:54:10'),
(89, 'account', 'delete account: 26', 1, '2019-04-12 03:54:16'),
(90, 'property', 'Update property: 5', 1, '2019-04-16 09:18:56'),
(91, 'property', 'Insert property: 0', 1, '2019-04-16 09:19:18'),
(92, 'property', 'Insert property: 0', 1, '2019-04-16 09:21:03'),
(93, 'property', 'Update property: 9', 1, '2019-04-16 09:21:12'),
(94, 'property', 'Update property: 9', 1, '2019-04-16 09:21:21'),
(95, 'property', 'Insert property: 0', 1, '2019-04-16 09:22:48'),
(96, 'property', 'Insert property: 0', 1, '2019-04-16 09:22:53'),
(97, 'property', 'Insert property: 0', 1, '2019-04-16 09:23:54'),
(98, 'property', 'Insert property: 0', 1, '2019-04-16 09:24:09'),
(99, 'property', 'Insert property: 0', 1, '2019-04-16 09:24:14'),
(100, 'property', 'Insert property: 0', 1, '2019-04-16 09:24:19'),
(101, 'property', 'Update property: 15', 1, '2019-04-16 09:25:20'),
(102, 'property', 'Update property: 14', 1, '2019-04-16 09:25:20'),
(103, 'property', 'Update property: 13', 1, '2019-04-16 09:25:20'),
(104, 'property', 'Update property: 12', 1, '2019-04-16 09:25:25'),
(105, 'property', 'Update property: 5', 1, '2019-04-16 09:25:53'),
(106, 'property', 'Update property: 5', 1, '2019-04-16 09:26:22'),
(107, 'property', 'Update property: 10', 1, '2019-04-16 09:33:06'),
(108, 'property', 'Update property: 11', 1, '2019-04-16 09:35:02'),
(109, 'property', 'Insert property: 0', 1, '2019-04-16 09:35:47'),
(110, 'property', 'Update property: 11', 1, '2019-04-16 09:35:54'),
(111, 'property', 'Insert property: 0', 1, '2019-04-16 09:37:38'),
(112, 'property', 'Insert property: 0', 1, '2019-04-16 09:38:18'),
(113, 'property', 'Insert property: 0', 1, '2019-04-16 09:39:48'),
(114, 'property', 'Insert property: 0', 1, '2019-04-16 09:40:31'),
(115, 'property', 'Update property: 5', 1, '2019-04-16 09:44:25'),
(116, 'property', 'Update property: 4', 1, '2019-04-16 09:44:56'),
(117, 'property', 'Insert property: 0', 1, '2019-04-16 09:45:24'),
(118, 'property', 'Update property: 21', 1, '2019-04-16 09:49:13'),
(119, 'property', 'Update property: 5', 1, '2019-04-16 09:49:52'),
(120, 'property', 'Update property: 4', 1, '2019-04-16 09:49:57'),
(121, 'category', 'Insert category: 3', 1, '2019-04-16 09:52:21'),
(122, 'category', 'Insert category: 4', 1, '2019-04-16 09:54:07'),
(123, 'category', 'Insert category: 5', 1, '2019-04-16 09:55:39'),
(124, 'category', 'Insert category: 6', 1, '2019-04-16 09:57:05'),
(125, 'category', 'Insert category: 7', 1, '2019-04-16 09:59:33'),
(126, 'category', 'Insert category: 8', 1, '2019-04-16 10:00:32'),
(127, 'category', 'Update category: 8', 1, '2019-04-16 10:01:40'),
(128, 'category', 'Insert category: 9', 1, '2019-04-16 10:02:14'),
(129, 'category', 'Insert category: 10', 1, '2019-04-16 10:02:29'),
(130, 'category', 'Update category: 10', 1, '2019-04-16 10:02:34'),
(131, 'category', 'Update category: 9', 1, '2019-04-16 10:02:34'),
(132, 'category', 'Update category: 8', 1, '2019-04-16 10:02:40'),
(133, 'category', 'Insert category: 11', 1, '2019-04-16 10:02:59'),
(134, 'category', 'Insert category: 12', 1, '2019-04-16 10:03:11'),
(135, 'category', 'Insert category: 13', 1, '2019-04-16 10:03:22'),
(136, 'category', 'Insert category: 14', 1, '2019-04-16 10:03:33'),
(137, 'category', 'Insert category: 15', 1, '2019-04-16 10:03:49'),
(138, 'category', 'Update category: 15', 1, '2019-04-16 10:04:44'),
(139, 'category', 'Update category: 14', 1, '2019-04-16 10:04:44'),
(140, 'category', 'Update category: 13', 1, '2019-04-16 10:04:44'),
(141, 'category', 'Update category: 12', 1, '2019-04-16 10:04:44'),
(142, 'category', 'Update category: 11', 1, '2019-04-16 10:04:44'),
(143, 'category', 'Update category: 2', 1, '2019-04-16 10:05:44'),
(144, 'category', 'Update category: 2', 1, '2019-04-16 10:05:53'),
(145, 'category', 'Update category: 2', 1, '2019-04-16 10:06:05'),
(146, 'category', 'Update category: 2', 1, '2019-04-16 10:06:13'),
(147, 'property', 'Insert property: 0', 1, '2019-04-16 10:50:13'),
(148, 'property', 'Update property: 22', 1, '2019-04-16 10:50:21'),
(149, 'property', 'Insert property: 0', 1, '2019-04-16 10:52:38'),
(150, 'property', 'Update property: 23', 1, '2019-04-16 10:52:45'),
(151, 'property', 'Insert property: 0', 1, '2019-04-16 10:53:19'),
(152, 'property', 'Insert property: 0', 1, '2019-04-16 10:53:55'),
(153, 'property', 'Insert property: 0', 1, '2019-04-16 10:54:43'),
(154, 'property', 'Insert property: 0', 1, '2019-04-16 10:55:06'),
(155, 'category', 'Insert category: 16', 1, '2019-04-16 10:59:33'),
(156, 'room', 'Update room: 1', 1, '2019-04-16 11:04:56'),
(157, 'room', 'Update room: 2', 1, '2019-04-16 11:16:58'),
(158, 'room', 'Update room: 2', 1, '2019-04-16 11:17:25'),
(159, 'category', 'Insert category: 17', 1, '2019-04-16 11:29:42'),
(160, 'category', 'Update category: 17', 1, '2019-04-16 11:32:17'),
(161, 'category', 'Update category: 1', 1, '2019-04-16 11:33:17'),
(162, 'category', 'Update category: 1', 1, '2019-04-16 11:33:26'),
(163, 'category', 'Insert category: 18', 1, '2019-04-16 11:34:08'),
(164, 'category', 'Insert category: 19', 1, '2019-04-16 11:34:42'),
(165, 'category', 'Insert category: 20', 1, '2019-04-16 11:35:11'),
(166, 'post', 'Update post: 1', 1, '2019-04-16 11:37:35'),
(167, 'post', 'Insert post: 2', 1, '2019-04-16 11:39:12'),
(168, 'post', 'Insert post: 3', 1, '2019-04-16 11:40:15'),
(169, 'post', 'Insert post: 4', 1, '2019-04-16 11:40:53'),
(170, 'post', 'Insert post: 5', 1, '2019-04-16 11:41:38'),
(171, 'post', 'Update post: 1', 1, '2019-04-16 11:42:40'),
(172, 'account', 'Update account: 25', 1, '2019-04-16 02:11:41'),
(173, 'account', 'Insert account: 30', 1, '2019-04-16 02:29:32'),
(174, 'account', 'Insert account: 31', 1, '2019-04-16 02:58:17'),
(175, 'account', 'Update account: 31', 1, '2019-04-16 03:02:15'),
(176, 'account', 'Update account: 30', 1, '2019-04-16 03:02:36'),
(177, 'account', 'Update account: 31', 1, '2019-04-16 03:02:41'),
(178, 'account', 'Update account: 31', 1, '2019-04-16 03:03:01'),
(179, 'account', 'Update account: 31', 1, '2019-04-16 03:03:08'),
(180, 'account', 'Update account: 31', 1, '2019-04-16 03:03:14'),
(181, 'account', 'Update account: 25', 1, '2019-04-16 03:10:55'),
(182, 'account', 'Update account: 31', 1, '2019-04-16 03:11:29'),
(183, 'account', 'Update account: 31', 1, '2019-04-16 03:11:59'),
(184, 'account', 'Update account: 31', 1, '2019-04-16 03:12:31'),
(185, 'account', 'Update account: 31', 1, '2019-04-16 03:18:50'),
(186, 'account', 'Update account: 30', 1, '2019-04-16 03:20:48'),
(187, 'account', 'Update account: 30', 1, '2019-04-16 03:20:58'),
(188, 'account', 'Update account: 30', 1, '2019-04-16 03:21:23'),
(189, 'account', 'Update account: 30', 1, '2019-04-16 03:21:52'),
(190, 'category', 'Insert category: 21', 1, '2019-04-16 04:13:47'),
(191, 'category', 'Update category: 21', 1, '2019-04-16 04:14:46'),
(192, 'category', 'Insert category: 22', 1, '2019-04-16 04:15:17'),
(193, 'category', 'Insert category: 23', 1, '2019-04-16 04:15:33'),
(194, 'category', 'Update category: 23', 1, '2019-04-16 04:15:39'),
(195, 'category', 'Update category: 22', 1, '2019-04-16 04:15:39'),
(196, 'category', 'Update category: 21', 1, '2019-04-16 04:15:43'),
(197, 'category', 'Insert category: 24', 1, '2019-04-16 04:17:42'),
(198, 'category', 'Insert category: 25', 1, '2019-04-16 04:17:52'),
(199, 'category', 'Insert category: 26', 1, '2019-04-16 04:18:02'),
(200, 'category', 'Insert category: 27', 1, '2019-04-16 04:18:12'),
(201, 'category', 'Insert category: 28', 1, '2019-04-16 04:18:24'),
(202, 'category', 'Insert category: 29', 1, '2019-04-16 04:18:34'),
(203, 'category', 'Update category: 29', 1, '2019-04-16 04:19:30'),
(204, 'category', 'Update category: 28', 1, '2019-04-16 04:19:30'),
(205, 'category', 'Update category: 27', 1, '2019-04-16 04:19:30'),
(206, 'category', 'Update category: 26', 1, '2019-04-16 04:19:30'),
(207, 'category', 'Update category: 25', 1, '2019-04-16 04:19:30'),
(208, 'category', 'Update category: 24', 1, '2019-04-16 04:19:30'),
(209, 'post', 'Insert post: 6', 1, '2019-04-16 04:32:05'),
(210, 'post', 'Insert post: 7', 1, '2019-04-16 04:33:50'),
(211, 'post', 'Insert post: 8', 1, '2019-04-16 04:34:03'),
(212, 'post', 'Insert post: 9', 1, '2019-04-16 04:34:22'),
(213, 'post', 'Insert post: 10', 1, '2019-04-16 04:34:35'),
(214, 'post', 'Insert post: 11', 1, '2019-04-16 04:34:50'),
(215, 'post', 'Update post: 11', 1, '2019-04-16 04:36:09'),
(216, 'post', 'Update post: 10', 1, '2019-04-16 04:36:20'),
(217, 'post', 'Update post: 9', 1, '2019-04-16 04:36:31'),
(218, 'post', 'Update post: 8', 1, '2019-04-16 04:36:40'),
(219, 'post', 'Update post: 7', 1, '2019-04-16 04:36:40'),
(220, 'post', 'Update post: 6', 1, '2019-04-16 04:36:40'),
(221, 'message_system', 'Sửa message_system có id là 6', 1, '2019-04-16 05:08:29'),
(222, 'message_system', 'Sửa message_system có id là 6', 1, '2019-04-16 05:08:35'),
(223, 'message_system', 'Sửa message_system có id là 7', 1, '2019-04-16 05:21:19'),
(224, 'message_system', 'Sửa message_system có id là 8', 1, '2019-04-16 05:22:15'),
(225, 'message_system', 'Sửa message_system có id là 8', 1, '2019-04-16 05:22:24'),
(226, 'message_system', 'Sửa message_system có id là 9', 1, '2019-04-16 05:43:38'),
(227, 'message_system', 'Sửa message_system có id là 9', 1, '2019-04-16 05:59:07'),
(228, 'message_system', 'Sửa message_system có id là 10', 1, '2019-04-17 09:30:38'),
(229, 'page', 'Insert page: 0', 1, '2019-04-17 09:44:44'),
(230, 'page', 'Insert page: 0', 1, '2019-04-17 09:45:01'),
(231, 'page', 'Update page: 14', 1, '2019-04-17 09:45:10'),
(232, 'page', 'Update page: 13', 1, '2019-04-17 09:45:16'),
(233, 'page', 'Insert page: 0', 1, '2019-04-17 09:57:55'),
(234, 'page', 'Update page: 15', 1, '2019-04-17 09:58:14'),
(235, 'page', 'Update page: 15', 1, '2019-04-17 09:58:37'),
(236, 'groups', 'Insert groups: 16', 1, '2019-04-17 11:32:08'),
(237, 'groups', 'Update groups: 16', 1, '2019-04-17 11:32:26'),
(238, 'groups', 'Update groups: 16', 1, '2019-04-17 11:32:33'),
(239, 'groups', 'Insert groups: 17', 1, '2019-04-17 11:33:18'),
(240, 'groups', 'Insert groups: 18', 1, '2019-04-17 11:33:45'),
(241, 'users', 'Update users: 38', 1, '2019-04-17 11:41:12'),
(242, 'users', 'Update users: 38', 1, '2019-04-17 11:43:43'),
(243, 'users', 'Update users: 38', 1, '2019-04-17 11:43:51'),
(244, 'groups', 'Update groups: 18', 1, '2019-04-17 11:50:34'),
(245, 'users', 'Update users: 38', 1, '2019-04-17 11:51:03'),
(246, 'groups', 'Insert groups: 19', 1, '2019-04-17 11:52:32'),
(247, 'groups', 'Update groups: 19', 1, '2019-04-17 01:33:01'),
(248, 'users', 'Insert users: 0', 1, '2019-04-17 01:35:33'),
(249, 'account', 'Update account: 31', 1, '2019-04-17 01:47:20'),
(250, 'account', 'Update account: 31', 1, '2019-04-17 01:47:47'),
(251, 'account', 'Update account: 31', 1, '2019-04-17 01:48:03'),
(252, 'account', 'Update account: 31', 1, '2019-04-17 01:48:24'),
(253, 'account', 'Update account: 31', 1, '2019-04-17 01:48:31'),
(254, 'account', 'Update account: 31', 1, '2019-04-17 01:48:45'),
(255, 'account', 'Insert account: 45', 1, '2019-04-17 01:50:07'),
(256, 'account', 'delete account: 45', 1, '2019-04-17 02:10:28'),
(257, 'account', 'delete account: 31', 1, '2019-04-17 02:14:37'),
(258, 'account', 'delete account: 30', 1, '2019-04-17 02:14:37'),
(259, 'account', 'delete account: 25', 1, '2019-04-17 02:14:37'),
(260, 'account', 'delete account: 24', 1, '2019-04-17 02:14:37'),
(261, 'account', 'delete account: 23', 1, '2019-04-17 02:14:37'),
(262, 'account', 'Insert account: 46', 1, '2019-04-17 02:15:16'),
(263, 'contact', 'Update contact: 44', 1, '2019-04-17 03:03:08'),
(264, 'contact', 'Update contact: 41', 1, '2019-04-17 03:04:15'),
(265, 'contact', 'Update contact: 40', 1, '2019-04-17 03:04:15'),
(266, 'property', 'Insert property: 0', 1, '2019-04-18 03:08:39'),
(267, 'property', 'Update property: 28', 1, '2019-04-18 03:10:54'),
(268, 'property', 'Insert property: 0', 1, '2019-04-18 03:11:13'),
(269, 'property', 'Insert property: 0', 1, '2019-04-18 03:11:39'),
(270, 'property', 'Insert property: 0', 1, '2019-04-18 03:12:21'),
(271, 'property', 'Update property: 31', 1, '2019-04-18 03:12:36'),
(272, 'property', 'Insert property: 0', 1, '2019-04-18 03:13:53'),
(273, 'property', 'Update property: 32', 1, '2019-04-18 03:14:01'),
(274, 'property', 'Update property: 32', 1, '2019-04-18 03:16:43'),
(275, 'property', 'Update property: 30', 1, '2019-04-18 03:16:43'),
(276, 'property', 'Update property: 29', 1, '2019-04-18 03:16:48'),
(277, 'post', 'Thêm Banner có id là 8', 1, '2019-04-18 03:29:12'),
(278, 'post', 'Thêm Banner có id là 9', 1, '2019-04-18 03:34:23'),
(279, 'property', 'Insert property: 0', 1, '2019-04-18 03:40:55'),
(280, 'property', 'Insert property: 0', 1, '2019-04-18 03:46:16'),
(281, 'property', 'Insert property: 0', 1, '2019-04-18 03:46:24'),
(282, 'post', 'Thêm Banner có id là 10', 1, '2019-04-19 09:25:56'),
(283, 'post', 'Thêm Banner có id là 11', 1, '2019-04-19 09:27:27'),
(284, 'post', 'Thêm Banner có id là 12', 1, '2019-04-19 09:28:25'),
(285, 'post', 'Thêm Banner có id là 13', 1, '2019-04-19 09:48:32'),
(286, 'post', 'Thêm Banner có id là 14', 1, '2019-04-19 09:48:43'),
(287, 'post', 'Thêm Banner có id là 15', 1, '2019-04-19 09:52:27'),
(288, 'post', 'Thêm Banner có id là 16', 1, '2019-04-19 09:53:41'),
(289, 'property', 'Insert property: 0', 1, '2019-04-19 09:54:14'),
(290, 'post', 'Thêm Banner có id là 17', 1, '2019-04-19 09:55:53'),
(291, 'post', 'Thêm Banner có id là 18', 1, '2019-04-19 10:04:40'),
(292, 'property', 'Insert property: 0', 1, '2019-04-19 10:33:16'),
(293, 'property', 'Insert property: 0', 1, '2019-04-19 10:33:35'),
(294, 'property', 'Insert property: 0', 1, '2019-04-19 10:33:53'),
(295, 'property', 'Insert property: 0', 1, '2019-04-19 10:34:10'),
(296, 'property', 'Insert property: 0', 1, '2019-04-19 10:34:23'),
(297, 'property', 'Insert property: 0', 1, '2019-04-19 10:34:38'),
(298, 'property', 'Insert property: 0', 1, '2019-04-19 10:34:54'),
(299, 'property', 'Insert property: 0', 1, '2019-04-19 10:35:13'),
(300, 'property', 'Insert property: 0', 1, '2019-04-19 10:35:28'),
(301, 'property', 'Insert property: 0', 1, '2019-04-19 10:35:41'),
(302, 'property', 'Insert property: 0', 1, '2019-04-19 10:35:52'),
(303, 'property', 'Insert property: 0', 1, '2019-04-19 10:36:06'),
(304, 'property', 'Update property: 3', 1, '2019-04-19 10:37:37'),
(305, 'property', 'Insert property: 0', 1, '2019-04-19 10:38:14'),
(306, 'property', 'Insert property: 0', 1, '2019-04-19 10:38:42'),
(307, 'property', 'Update property: 5', 1, '2019-04-19 10:39:47'),
(308, 'property', 'Update property: 4', 1, '2019-04-19 10:39:47'),
(309, 'property', 'Update property: 3', 1, '2019-04-19 10:39:47'),
(310, 'property', 'Update property: 21', 1, '2019-04-19 10:39:59'),
(311, 'property', 'Update property: 20', 1, '2019-04-19 10:39:59'),
(312, 'property', 'Update property: 19', 1, '2019-04-19 10:39:59'),
(313, 'property', 'Update property: 18', 1, '2019-04-19 10:39:59'),
(314, 'property', 'Update property: 17', 1, '2019-04-19 10:39:59'),
(315, 'property', 'Update property: 16', 1, '2019-04-19 10:39:59'),
(316, 'property', 'Update property: 11', 1, '2019-04-19 10:39:59'),
(317, 'property', 'Update property: 10', 1, '2019-04-19 10:39:59'),
(318, 'property', 'Update property: 9', 1, '2019-04-19 10:39:59'),
(319, 'property', 'Update property: 8', 1, '2019-04-19 10:39:59'),
(320, 'property', 'Update property: 27', 1, '2019-04-19 10:40:32'),
(321, 'property', 'Update property: 26', 1, '2019-04-19 10:40:32'),
(322, 'property', 'Update property: 25', 1, '2019-04-19 10:40:32'),
(323, 'property', 'Update property: 24', 1, '2019-04-19 10:40:32'),
(324, 'property', 'Update property: 23', 1, '2019-04-19 10:40:32'),
(325, 'property', 'Update property: 22', 1, '2019-04-19 10:40:32'),
(326, 'property', 'Insert property: 0', 1, '2019-04-19 10:41:13'),
(327, 'property', 'Insert property: 0', 1, '2019-04-19 10:41:29'),
(328, 'property', 'Insert property: 0', 1, '2019-04-19 10:43:20'),
(329, 'property', 'Insert property: 0', 1, '2019-04-19 10:43:52'),
(330, 'property', 'Insert property: 0', 1, '2019-04-19 10:44:26'),
(331, 'property', 'Insert property: 0', 1, '2019-04-19 10:44:54'),
(332, 'property', 'Insert property: 0', 1, '2019-04-19 10:45:16'),
(333, 'property', 'Insert property: 0', 1, '2019-04-19 10:45:29'),
(334, 'property', 'Update property: 58', 1, '2019-04-19 10:45:35'),
(335, 'property', 'Insert property: 0', 1, '2019-04-19 10:46:08'),
(336, 'property', 'Insert property: 0', 1, '2019-04-19 10:46:30'),
(337, 'property', 'Insert property: 0', 1, '2019-04-19 10:46:45'),
(338, 'property', 'Insert property: 0', 1, '2019-04-19 10:47:16'),
(339, 'property', 'Insert property: 0', 1, '2019-04-19 10:47:41'),
(340, 'property', 'Insert property: 0', 1, '2019-04-19 10:48:46'),
(341, 'property', 'Insert property: 0', 1, '2019-04-19 10:49:20'),
(342, 'property', 'Insert property: 0', 1, '2019-04-19 10:49:56'),
(343, 'property', 'Insert property: 0', 1, '2019-04-19 10:50:19'),
(344, 'category', 'Update category: 5', 1, '2019-04-19 10:52:21'),
(345, 'category', 'Update category: 16', 1, '2019-04-19 10:53:21'),
(346, 'category', 'Update category: 16', 1, '2019-04-19 10:53:37'),
(347, 'category', 'Update category: 7', 1, '2019-04-19 10:53:56'),
(348, 'category', 'Update category: 6', 1, '2019-04-19 10:53:56'),
(349, 'category', 'Update category: 4', 1, '2019-04-19 10:53:56'),
(350, 'category', 'Update category: 2', 1, '2019-04-19 10:53:56'),
(351, 'category', 'Insert category: 30', 1, '2019-04-19 10:57:52'),
(352, 'category', 'Update category: 3', 1, '2019-04-19 10:58:34'),
(353, 'category', 'Update category: 5', 1, '2019-04-19 10:58:40'),
(354, 'category', 'Update category: 16', 1, '2019-04-19 10:58:47'),
(355, 'category', 'Update category: 30', 1, '2019-04-19 10:58:52'),
(356, 'post', 'Thêm Banner có id là 19', 1, '2019-04-19 11:01:16'),
(357, 'post', 'Thêm Banner có id là 20', 1, '2019-04-19 11:01:23'),
(358, 'post', 'Thêm Banner có id là 21', 1, '2019-04-19 11:01:30'),
(359, 'post', 'Thêm Banner có id là 22', 1, '2019-04-19 11:01:38'),
(360, 'post', 'Thêm Banner có id là 23', 1, '2019-04-19 11:01:48'),
(361, 'post', 'Thêm Banner có id là 24', 1, '2019-04-19 11:01:57'),
(362, 'currency', 'Insert currency: 113', 1, '2019-04-19 11:27:03'),
(363, 'currency', 'Insert currency: 114', 1, '2019-04-19 11:31:05'),
(364, 'currency', 'Update currency: 113', 1, '2019-04-19 11:31:46'),
(365, 'currency', 'Update currency: 113', 1, '2019-04-19 11:33:32'),
(366, 'currency', 'Update currency: 113', 1, '2019-04-19 11:34:22'),
(367, 'search', 'Update search: 9', 1, '2019-04-19 11:40:18'),
(368, 'search', 'Update search: 9', 1, '2019-04-19 11:52:26'),
(369, 'search', 'Update search: 9', 1, '2019-04-19 11:54:08'),
(370, 'search', 'Update search: 10', 1, '2019-04-19 11:54:08'),
(371, 'search', 'Update search: 11', 1, '2019-04-19 11:54:34'),
(372, 'search', 'Update search: 12', 1, '2019-04-19 11:54:38'),
(373, 'search', 'Update search: 13', 1, '2019-04-19 11:54:42'),
(374, 'search', 'Update search: 14', 1, '2019-04-19 11:54:42'),
(375, 'search', 'Update search: 15', 1, '2019-04-19 11:54:42'),
(376, 'search', 'Update search: 16', 1, '2019-04-19 11:54:42'),
(377, 'search', 'Update search: 17', 1, '2019-04-19 11:54:42'),
(378, 'search', 'Update search: 18', 1, '2019-04-19 11:54:42'),
(379, 'search', 'Update search: 19', 1, '2019-04-19 11:54:42'),
(380, 'property', 'Insert property: 0', 1, '2019-04-19 02:04:44'),
(381, 'property', 'Update property: 68', 1, '2019-04-19 02:04:55'),
(382, 'property', 'Update property: 7', 1, '2019-04-19 02:05:14'),
(383, 'property', 'Update property: 7', 1, '2019-04-19 02:05:33'),
(384, 'property', 'Update property: 7', 1, '2019-04-19 02:05:58'),
(385, 'property', 'Update property: 68', 1, '2019-04-19 02:09:24'),
(386, 'room', 'Update room: 2', 1, '2019-04-19 02:10:23'),
(387, 'room', 'Insert room: 17', 1, '2019-04-19 02:35:18'),
(388, 'room', 'Insert room: 18', 1, '2019-04-19 02:35:46'),
(389, 'room', 'Insert room: 19', 1, '2019-04-19 02:44:07'),
(390, 'room', 'Update room: 19', 1, '2019-04-19 02:44:31'),
(391, 'room', 'Update room: 19', 1, '2019-04-19 02:44:47'),
(392, 'room', 'Update room: 19', 1, '2019-04-19 02:46:42'),
(393, 'room', 'Update room: 18', 1, '2019-04-19 02:46:42'),
(394, 'room', 'Update room: 17', 1, '2019-04-19 02:46:45'),
(395, 'room', 'Insert room: 20', 1, '2019-04-19 02:47:25'),
(396, 'room', 'Update room: 2', 1, '2019-04-21 11:50:56'),
(397, 'room', 'Update room: 1', 1, '2019-04-21 11:52:18'),
(398, 'room', 'Update room: 1', 1, '2019-04-21 11:52:23'),
(399, 'room', 'Update room: 22', 46, '2019-04-22 09:58:38'),
(400, 'room', 'Update room: 22', 46, '2019-04-22 10:11:04'),
(401, 'room', 'Update room: 22', 46, '2019-04-22 10:11:22'),
(402, 'room', 'Update room: 22', 46, '2019-04-22 10:17:05'),
(403, 'room', 'Update room: 22', 46, '2019-04-22 10:17:33'),
(404, 'room', 'Insert room: 23', 46, '2019-04-22 10:22:54'),
(405, 'room', 'Update room: 23', 46, '2019-04-22 10:24:13'),
(406, 'room', 'Insert room: 24', 46, '2019-04-22 10:25:05'),
(407, 'room', 'Update room: 24', 46, '2019-04-22 10:26:05'),
(408, 'room', 'Update room: 24', 46, '2019-04-22 10:27:08'),
(409, 'room', 'Update room: 23', 46, '2019-04-22 10:27:28'),
(410, 'room', 'Insert room: 25', 46, '2019-04-22 10:29:24'),
(411, 'account', 'delete account: 46', 46, '2019-04-22 11:35:33'),
(412, 'users', 'Insert users: 0', 1, '2019-04-22 04:02:32'),
(413, 'users', 'delete users: 38', 1, '2019-04-22 04:02:46'),
(414, 'users', 'Update users: 46', 1, '2019-04-22 04:02:57'),
(415, 'search', 'Insert search: 23', 1, '2019-04-23 08:53:20'),
(416, 'users', 'Update users: 47', 1, '2019-04-23 08:54:21'),
(417, 'users', 'Insert users: 0', 1, '2019-04-23 08:59:56'),
(418, 'account', 'Insert account: 65', 1, '2019-04-23 09:03:24'),
(419, 'account', 'Update account: 65', 1, '2019-04-23 09:03:40'),
(420, 'users', 'Update users: 46', 1, '2019-04-23 09:30:28'),
(421, 'users', 'Insert users: 0', 46, '2019-04-23 09:34:16'),
(422, 'users', 'Insert users: 0', 46, '2019-04-23 09:35:44'),
(423, 'property', 'Insert property: 0', 46, '2019-04-23 10:27:05'),
(424, 'property', 'Insert property: 0', 46, '2019-04-23 10:28:21'),
(425, 'property', 'Insert property: 0', 46, '2019-04-23 10:29:05'),
(426, 'account', 'delete account: 65', 46, '2019-04-23 11:20:18'),
(427, 'account', 'delete account: 64', 46, '2019-04-23 11:20:18'),
(428, 'account', 'delete account: 63', 46, '2019-04-23 11:20:18'),
(429, 'account', 'delete account: 62', 46, '2019-04-23 11:20:18'),
(430, 'account', 'delete account: 61', 46, '2019-04-23 11:20:18'),
(431, 'account', 'delete account: 60', 46, '2019-04-23 11:20:18'),
(432, 'room', 'Update room: 28', 1, '2019-04-23 11:46:34'),
(433, 'account', 'Insert account: 66', 1, '2019-04-23 02:18:50'),
(434, 'account', 'delete account: 66', 1, '2019-04-23 02:20:12'),
(435, 'account', 'Insert account: 68', 1, '2019-04-23 02:27:55'),
(436, 'property', 'Update property: 67', 1, '2019-04-23 03:12:29'),
(437, 'property', 'Update property: 67', 1, '2019-04-23 03:26:02'),
(438, 'property', 'Update property: 57', 1, '2019-04-23 03:26:28'),
(439, 'property', 'Update property: 51', 1, '2019-04-23 03:26:42'),
(440, 'property', 'Update property: 53', 1, '2019-04-23 03:26:57'),
(441, 'property', 'Update property: 60', 1, '2019-04-23 03:28:32'),
(442, 'room', 'Update room: 20', 1, '2019-04-23 03:33:52'),
(443, 'room', 'Update room: 2', 1, '2019-04-23 03:33:57'),
(444, 'room', 'Update room: 2', 1, '2019-04-23 03:35:53'),
(445, 'account', 'Update account: 67', 1, '2019-04-23 04:24:36'),
(446, 'account', 'Update account: 68', 1, '2019-04-23 04:43:46'),
(447, 'room', 'Insert room: 29', 1, '2019-04-24 09:55:56'),
(448, 'room', 'Update room: 29', 1, '2019-04-24 09:56:43'),
(449, 'users', 'Insert users: 0', 1, '2019-04-24 10:31:48'),
(450, 'account', 'Update account: 70', 1, '2019-04-24 10:56:57'),
(451, 'account', 'delete account: 70', 1, '2019-04-24 10:59:44'),
(452, 'account', 'delete account: 72', 1, '2019-04-24 11:26:45'),
(453, 'account', 'Update account: 73', 1, '2019-04-24 11:33:44'),
(454, 'account', 'Update account: 73', 1, '2019-04-24 11:34:46'),
(455, 'account', 'Update account: 73', 1, '2019-04-24 11:34:59'),
(456, 'account', 'Update account: 73', 1, '2019-04-24 11:35:34'),
(457, 'account', 'delete account: 73', 1, '2019-04-24 11:37:47'),
(458, 'account', 'Insert account: 74', 1, '2019-04-24 11:39:04'),
(459, 'unit', 'Update unit: 1', 1, '2019-04-24 02:25:22'),
(460, 'unit', 'Insert unit: 2', 1, '2019-04-24 02:26:37'),
(461, 'account', 'delete account: 71', 1, '2019-04-24 02:34:17'),
(462, 'account', 'delete account: 69', 1, '2019-04-24 02:34:20'),
(463, 'account', 'Insert account: 75', 1, '2019-04-24 02:35:19'),
(464, 'account', 'delete account: 75', 1, '2019-04-24 02:36:32'),
(465, 'account', 'Insert account: 76', 1, '2019-04-24 02:38:36'),
(466, 'account', 'Update account: 77', 1, '2019-04-24 09:55:28'),
(467, 'account', 'Update account: 77', 1, '2019-04-24 09:55:32'),
(468, 'room', 'Insert room: 30', 1, '2019-04-25 10:17:05'),
(469, 'room', 'Update room: 30', 1, '2019-04-25 10:17:27'),
(470, 'property', 'Update property: 71', 1, '2019-04-25 10:57:04'),
(471, 'property', 'Update property: 70', 1, '2019-04-25 10:57:06'),
(472, 'property', 'Update property: 69', 1, '2019-04-25 10:57:09'),
(473, 'property', 'Update property: 36', 1, '2019-04-25 10:57:11'),
(474, 'property', 'Update property: 35', 1, '2019-04-25 10:57:14'),
(475, 'property', 'Update property: 34', 1, '2019-04-25 10:57:16'),
(476, 'property', 'Update property: 33', 1, '2019-04-25 10:57:18'),
(477, 'room', 'Update room: 30', 1, '2019-04-25 02:06:13'),
(478, 'room', 'Update room: 30', 1, '2019-04-25 02:25:30'),
(479, 'room', 'Update room: 30', 1, '2019-04-25 02:26:34'),
(480, 'room', 'Update room: 30', 1, '2019-04-25 02:29:53'),
(481, 'room', 'Update room: 30', 1, '2019-04-25 02:30:56'),
(482, 'property', 'Update property: 67', 1, '2019-04-25 02:44:01'),
(483, 'property', 'Update property: 57', 1, '2019-04-25 02:44:46'),
(484, 'property', 'Update property: 51', 1, '2019-04-25 02:45:12'),
(485, 'property', 'Update property: 53', 1, '2019-04-25 02:45:33'),
(486, 'property', 'Update property: 60', 1, '2019-04-25 02:45:52'),
(487, 'account', 'Update account: 68', 1, '2019-04-25 02:48:33'),
(488, 'account', 'Update account: 81', 1, '2019-04-25 02:51:41'),
(489, 'account', 'delete account: 81', 1, '2019-04-25 02:56:37'),
(490, 'account', 'Insert account: 82', 1, '2019-04-25 02:59:15'),
(491, 'account', 'Insert account: 83', 1, '2019-04-25 03:02:04'),
(492, 'account', 'Update account: 68', 1, '2019-04-25 04:35:29'),
(493, 'room', 'Update room: 30', 1, '2019-04-25 10:18:25'),
(494, 'account', 'Insert account: 84', 1, '2019-04-26 09:40:20'),
(495, 'account', 'delete account: 84', 1, '2019-04-26 09:40:46'),
(496, 'account', 'Insert account: 85', 1, '2019-04-26 09:42:38'),
(497, 'room', 'Update room: 30', 1, '2019-04-26 11:05:52'),
(498, 'room', 'Update room: 1', 1, '2019-04-26 11:06:08'),
(499, 'room', 'Update room: 2', 1, '2019-04-26 11:06:10'),
(500, 'room', 'Update room: 20', 1, '2019-04-26 11:06:13'),
(501, 'room', 'Update room: 22', 1, '2019-04-26 11:06:19'),
(502, 'room', 'Update room: 23', 1, '2019-04-26 11:06:22'),
(503, 'room', 'Update room: 24', 1, '2019-04-26 11:06:24'),
(504, 'room', 'Update room: 28', 1, '2019-04-26 11:06:35'),
(505, 'page', 'Update page: 12', 1, '2019-04-26 11:07:27'),
(506, 'page', 'Update page: 2', 1, '2019-04-26 11:07:30'),
(507, 'page', 'Update page: 7', 1, '2019-04-26 11:07:37'),
(508, 'page', 'Update page: 8', 1, '2019-04-26 11:07:40'),
(509, 'page', 'Update page: 9', 1, '2019-04-26 11:07:43'),
(510, 'page', 'Update page: 10', 1, '2019-04-26 11:07:46'),
(511, 'page', 'Update page: 11', 1, '2019-04-26 11:07:49'),
(512, 'page', 'Update page: 12', 1, '2019-04-26 11:07:54'),
(513, 'room', 'Update room: 31', 1, '2019-04-26 02:08:08'),
(514, 'room', 'Update room: 31', 1, '2019-04-26 02:08:33'),
(515, 'account', 'delete account: 85', 1, '2019-04-26 02:13:36'),
(516, 'account', 'Insert account: 88', 1, '2019-04-26 02:14:55'),
(517, 'users', 'Insert users: 0', 1, '2019-04-26 02:26:51'),
(518, 'account', 'delete account: 82', 1, '2019-04-26 09:46:07'),
(519, 'room', 'Update room: 30', 1, '2019-04-26 10:04:07'),
(520, 'room', 'Update room: 30', 1, '2019-04-26 10:06:08'),
(521, 'room', 'Update room: 32', 1, '2019-04-26 10:06:37'),
(522, 'room', 'Update room: 31', 1, '2019-04-26 10:06:39'),
(523, 'room', 'Insert room: 33', 1, '2019-04-26 10:24:53'),
(524, 'room', 'Insert room: 34', 1, '2019-04-26 11:45:43'),
(525, 'room', 'Update room: 34', 1, '2019-04-26 11:46:01'),
(526, 'message_system', 'Sửa message_system có id là 11', 1, '2019-04-27 10:25:59'),
(527, 'search', 'Update search: 20', 1, '2019-04-28 01:21:22'),
(528, 'search', 'Update search: 21', 1, '2019-04-28 01:21:25'),
(529, 'search', 'Update search: 23', 1, '2019-04-28 01:21:28'),
(530, 'search', 'Update search: 22', 1, '2019-04-28 01:21:31'),
(531, 'room', 'Update room: 38', 1, '2019-04-30 11:54:58'),
(532, 'room', 'Update room: 38', 1, '2019-04-30 11:56:52'),
(533, 'room', 'Update room: 38', 1, '2019-05-01 12:00:36'),
(534, 'room', 'Update room: 38', 1, '2019-05-01 12:00:49'),
(535, 'account', 'delete account: 105', 1, '2019-05-02 09:50:35'),
(536, 'account', 'Insert account: 106', 1, '2019-05-02 10:00:10'),
(537, 'account', 'delete account: 19', 1, '2019-05-02 11:04:39'),
(538, 'account', 'delete account: 20', 1, '2019-05-02 11:05:53'),
(539, 'account', 'delete account: 109', 1, '2019-05-02 11:58:13'),
(540, 'room', 'Update room: 33', 1, '2019-05-02 01:40:53'),
(541, 'room', 'Update room: 33', 1, '2019-05-02 01:43:22'),
(542, 'room', 'Update room: 33', 1, '2019-05-02 02:17:14'),
(543, 'room', 'Update room: 30', 1, '2019-05-02 02:18:09'),
(544, 'room', 'Update room: 30', 1, '2019-05-02 02:18:22'),
(545, 'room', 'Update room: 1', 1, '2019-05-02 02:19:28'),
(546, 'room', 'Update room: 2', 1, '2019-05-02 02:20:03'),
(547, 'room', 'Update room: 28', 1, '2019-05-02 02:21:04'),
(548, 'room', 'Update room: 33', 1, '2019-05-02 02:22:02'),
(549, 'room', 'Update room: 20', 1, '2019-05-02 02:22:34'),
(550, 'room', 'Update room: 1', 1, '2019-05-02 02:24:31'),
(551, 'room', 'Update room: 1', 1, '2019-05-02 02:30:55'),
(552, 'room', 'Update room: 2', 1, '2019-05-02 02:33:35'),
(553, 'room', 'Update room: 33', 1, '2019-05-02 02:39:56'),
(554, 'room', 'Update room: 20', 1, '2019-05-02 02:42:45'),
(555, 'room', 'Update room: 23', 1, '2019-05-02 02:45:19'),
(556, 'room', 'Update room: 24', 1, '2019-05-02 02:46:55'),
(557, 'room', 'Update room: 25', 1, '2019-05-02 02:47:57'),
(558, 'room', 'Update room: 29', 1, '2019-05-02 02:55:57'),
(559, 'room', 'Update room: 30', 1, '2019-05-02 03:02:20'),
(560, 'room', 'Update room: 29', 1, '2019-05-02 03:02:34'),
(561, 'room', 'Update room: 30', 1, '2019-05-02 03:20:11'),
(562, 'room', 'Update room: 34', 1, '2019-05-02 03:22:47'),
(563, 'room', 'Update room: 34', 1, '2019-05-02 05:15:04'),
(564, 'room', 'Update room: 34', 1, '2019-05-02 05:27:56'),
(565, 'room', 'Update room: 2', 1, '2019-05-02 05:43:49'),
(566, 'system_menu', 'Update system_menu: 1', 1, '2019-05-03 09:32:19'),
(567, 'system_menu', 'Update system_menu: 1', 1, '2019-05-03 09:32:36'),
(568, 'page', 'Update page: 10', 1, '2019-05-03 10:49:51'),
(569, 'room', 'Update room: 37', 1, '2019-05-03 02:11:36'),
(570, 'room', 'Update room: 36', 1, '2019-05-03 02:11:40'),
(571, 'room', 'Update room: 21', 1, '2019-05-03 02:12:17'),
(572, 'room', 'Update room: 28', 1, '2019-05-03 02:13:41'),
(573, 'room', 'Update room: 20', 1, '2019-05-03 02:18:02'),
(574, 'room', 'Update room: 39', 1, '2019-05-03 02:19:00'),
(575, 'room', 'Update room: 24', 1, '2019-05-03 02:22:50'),
(576, 'room', 'Update room: 24', 1, '2019-05-03 02:23:26'),
(577, 'room', 'Update room: 24', 1, '2019-05-03 02:23:59'),
(578, 'room', 'Update room: 24', 1, '2019-05-03 02:24:46'),
(579, 'room', 'Update room: 24', 1, '2019-05-03 02:28:31'),
(580, 'room', 'Update room: 34', 1, '2019-05-03 03:05:28'),
(581, 'room', 'Update room: 30', 1, '2019-05-03 03:06:34'),
(582, 'room', 'Update room: 30', 1, '2019-05-03 03:06:59'),
(583, 'room', 'Update room: 29', 1, '2019-05-03 03:07:14'),
(584, 'room', 'Update room: 25', 1, '2019-05-03 03:08:16'),
(585, 'room', 'Update room: 24', 1, '2019-05-03 03:08:42'),
(586, 'room', 'Update room: 23', 1, '2019-05-03 03:08:54'),
(587, 'room', 'Update room: 20', 1, '2019-05-03 03:09:13'),
(588, 'room', 'Update room: 2', 1, '2019-05-03 03:09:22'),
(589, 'room', 'Update room: 1', 1, '2019-05-03 03:09:34'),
(590, 'room', 'Update room: 34', 1, '2019-05-03 04:08:42'),
(591, 'room', 'Update room: 33', 1, '2019-05-03 04:09:14'),
(592, 'room', 'Update room: 30', 1, '2019-05-03 04:09:49'),
(593, 'room', 'Update room: 29', 1, '2019-05-03 04:10:30'),
(594, 'room', 'Update room: 28', 1, '2019-05-03 04:10:46'),
(595, 'room', 'Update room: 25', 1, '2019-05-03 04:11:10'),
(596, 'room', 'Update room: 24', 1, '2019-05-03 04:11:30'),
(597, 'room', 'Update room: 20', 1, '2019-05-03 04:11:54'),
(598, 'room', 'Update room: 2', 1, '2019-05-03 04:12:23'),
(599, 'room', 'Update room: 1', 1, '2019-05-03 04:12:50'),
(600, 'property', 'Insert property: 0', 1, '2019-05-03 04:23:53'),
(601, 'property', 'Insert property: 0', 1, '2019-05-03 04:30:10'),
(602, 'property', 'Insert property: 0', 1, '2019-05-03 04:30:53'),
(603, 'property', 'Insert property: 0', 1, '2019-05-03 04:35:34'),
(604, 'property', 'Update property: 75', 1, '2019-05-03 04:35:47'),
(605, 'property', 'Insert property: 0', 1, '2019-05-03 04:37:30'),
(606, 'property', 'Insert property: 0', 1, '2019-05-03 04:38:20'),
(607, 'property', 'Insert property: 0', 1, '2019-05-03 04:40:21'),
(608, 'room', 'Update room: 34', 1, '2019-05-03 04:43:07'),
(609, 'room', 'Update room: 33', 1, '2019-05-03 04:48:22'),
(610, 'property', 'Update property: 78', 1, '2019-05-03 04:48:36'),
(611, 'room', 'Update room: 30', 1, '2019-05-03 04:49:46'),
(612, 'room', 'Update room: 29', 1, '2019-05-03 04:50:42'),
(613, 'room', 'Update room: 28', 1, '2019-05-03 04:51:50'),
(614, 'room', 'Update room: 25', 1, '2019-05-03 04:52:49'),
(615, 'room', 'Update room: 24', 1, '2019-05-03 04:53:53'),
(616, 'room', 'Update room: 23', 1, '2019-05-03 04:55:24'),
(617, 'room', 'Update room: 20', 1, '2019-05-03 04:56:14'),
(618, 'room', 'Update room: 2', 1, '2019-05-03 04:57:26'),
(619, 'room', 'Update room: 1', 1, '2019-05-03 04:58:18'),
(620, 'property', 'Update property: 51', 1, '2019-05-03 05:01:22'),
(621, 'property', 'Update property: 60', 1, '2019-05-03 05:02:40'),
(622, 'property', 'Update property: 57', 1, '2019-05-03 05:02:51'),
(623, 'property', 'Update property: 53', 1, '2019-05-03 05:03:09'),
(624, 'room', 'Update room: 27', 1, '2019-05-03 05:07:28'),
(625, 'room', 'Update room: 35', 1, '2019-05-03 05:39:48'),
(626, 'room', 'Update room: 38', 1, '2019-05-03 05:43:30'),
(627, 'account', 'delete account: 110', 1, '2019-05-03 05:50:02'),
(628, 'account', 'Insert account: 115', 1, '2019-05-03 05:50:50'),
(629, 'account', 'delete account: 115', 1, '2019-05-03 05:54:29'),
(630, 'account', 'Update account: 116', 1, '2019-05-03 05:57:58'),
(631, 'users', 'Insert users: 0', 1, '2019-05-03 05:59:42'),
(632, 'users', 'delete users: 53', 1, '2019-05-03 05:59:54'),
(633, 'users', 'Insert users: 0', 1, '2019-05-03 06:00:40'),
(634, 'users', 'delete users: 54', 1, '2019-05-04 09:56:12'),
(635, 'users', 'Insert users: 0', 1, '2019-05-04 09:56:39'),
(636, 'users', 'Update users: ', 55, '2019-05-04 09:58:17'),
(637, 'account', 'delete account: 93', 1, '2019-05-04 03:49:50'),
(638, 'users', 'Update users: 55', 1, '2019-05-06 10:42:18'),
(639, 'users', 'Update users: 55', 1, '2019-05-06 10:43:22'),
(640, 'currency', 'Update currency: 114', 1, '2019-05-06 10:44:39'),
(641, 'currency', 'Update currency: 2', 1, '2019-05-06 10:46:14'),
(642, 'users', 'Insert users: 0', 1, '2019-05-06 10:57:47'),
(643, 'account', 'Update account: 68', 1, '2019-05-06 11:04:58'),
(644, 'account', 'Update account: 122', 1, '2019-05-06 01:50:11'),
(645, 'account', 'Insert account: 123', 1, '2019-05-06 01:57:40'),
(646, 'account', 'Update account: 123', 1, '2019-05-06 02:07:36'),
(647, 'account', 'Insert account: 124', 1, '2019-05-06 02:11:07'),
(648, 'category', 'Insert category: 31', 1, '2019-05-06 02:33:49'),
(649, 'account', 'Update account: 117', 1, '2019-05-06 02:37:08'),
(650, 'account', 'Update account: 117', 1, '2019-05-06 02:37:45'),
(651, 'account', 'Insert account: 125', 1, '2019-05-06 02:42:14'),
(652, 'search', 'Update search: 20', 1, '2019-05-06 02:43:22'),
(653, 'search', 'Update search: 21', 1, '2019-05-06 02:43:36'),
(654, 'search', 'Update search: 22', 1, '2019-05-06 02:44:25'),
(655, 'search', 'Update search: 23', 1, '2019-05-06 02:45:00'),
(656, 'account', 'Update account: 125', 1, '2019-05-06 02:45:36'),
(657, 'account', 'delete account: 125', 1, '2019-05-06 02:45:41'),
(658, 'search', 'Insert search: 24', 1, '2019-05-06 02:47:05'),
(659, 'search', 'Insert search: 25', 1, '2019-05-06 02:48:14'),
(660, 'renter', 'Update renter: 6', 1, '2019-05-06 02:48:31'),
(661, 'search', 'Update search: 20', 1, '2019-05-06 02:51:43'),
(662, 'search', 'Update search: 21', 1, '2019-05-06 02:51:57'),
(663, 'search', 'Update search: 22', 1, '2019-05-06 02:52:07'),
(664, 'search', 'Update search: 23', 1, '2019-05-06 02:52:10'),
(665, 'search', 'Update search: 24', 1, '2019-05-06 02:52:19'),
(666, 'search', 'Update search: 25', 1, '2019-05-06 02:52:37'),
(667, 'renter', 'Update renter: 7', 1, '2019-05-06 02:54:49'),
(668, 'category', 'Insert category: 32', 1, '2019-05-06 02:56:09'),
(669, 'category', 'Update category: 32', 1, '2019-05-06 02:56:13'),
(670, 'category', 'Update category: 31', 1, '2019-05-06 02:56:20'),
(671, 'contact', 'Update contact: 52', 1, '2019-05-06 03:06:33'),
(672, 'contact', 'Update contact: 53', 1, '2019-05-06 03:07:50'),
(673, 'message_system', 'Sửa message_system có id là 24', 1, '2019-05-06 03:12:39'),
(674, 'property', 'Insert property: 0', 1, '2019-05-06 03:19:06'),
(675, 'property', 'Insert property: 0', 1, '2019-05-06 03:19:51'),
(676, 'property', 'Update property: 80', 1, '2019-05-06 03:19:57'),
(677, 'property', 'Update property: 79', 1, '2019-05-06 03:20:15'),
(678, 'property', 'Update property: 79', 1, '2019-05-06 03:20:20'),
(679, 'post', 'Thêm Banner có id là 25', 1, '2019-05-06 03:21:14'),
(680, 'post', 'Thêm Banner có id là 26', 1, '2019-05-06 03:21:42'),
(681, 'page', 'Insert page: 0', 1, '2019-05-06 03:28:28'),
(682, 'page', 'Insert page: 0', 1, '2019-05-06 03:28:47'),
(683, 'page', 'Update page: 17', 1, '2019-05-06 03:28:53'),
(684, 'page', 'Update page: 16', 1, '2019-05-06 03:28:59'),
(685, 'search', 'Insert search: 26', 1, '2019-05-06 03:36:45'),
(686, 'search', 'Update search: 26', 1, '2019-05-06 03:37:05'),
(687, 'currency', 'Insert currency: 115', 1, '2019-05-06 03:42:23'),
(688, 'page', 'Update page: 10', 1, '2019-05-06 03:42:36'),
(689, 'currency', 'Insert currency: 116', 1, '2019-05-06 03:42:36'),
(690, 'currency', 'Update currency: 116', 1, '2019-05-06 03:42:51'),
(691, 'currency', 'Update currency: 115', 1, '2019-05-06 03:42:58'),
(692, 'page', 'Update page: 8', 1, '2019-05-06 03:43:50'),
(693, 'page', 'Update page: 8', 1, '2019-05-06 03:45:32'),
(694, 'category', 'Insert category: 33', 1, '2019-05-06 03:46:47'),
(695, 'category', 'Insert category: 34', 1, '2019-05-06 03:47:32'),
(696, 'category', 'Update category: 33', 1, '2019-05-06 03:47:39'),
(697, 'category', 'Update category: 34', 1, '2019-05-06 03:47:46'),
(698, 'property', 'Insert property: 0', 1, '2019-05-06 03:48:18'),
(699, 'page', 'Update page: 8', 1, '2019-05-06 03:48:25'),
(700, 'property', 'Insert property: 0', 1, '2019-05-06 03:48:28'),
(701, 'page', 'Update page: 10', 1, '2019-05-06 03:51:07'),
(702, 'page', 'Update page: 10', 1, '2019-05-06 03:51:56'),
(703, 'page', 'Update page: 8', 1, '2019-05-06 03:53:26'),
(704, 'page', 'Update page: 10', 1, '2019-05-06 03:53:51'),
(705, 'page', 'Update page: 11', 1, '2019-05-06 03:55:28'),
(706, 'property', 'Update property: 82', 1, '2019-05-06 03:57:08'),
(707, 'property', 'Update property: 81', 1, '2019-05-06 03:57:14'),
(708, 'page', 'Update page: 11', 1, '2019-05-06 03:57:31'),
(709, 'page', 'Update page: 9', 1, '2019-05-06 03:58:16'),
(710, 'page', 'Update page: 9', 1, '2019-05-06 04:00:22'),
(711, 'page', 'Update page: 9', 1, '2019-05-06 04:01:40'),
(712, 'property', 'Insert property: 0', 1, '2019-05-06 04:02:57'),
(713, 'property', 'Insert property: 0', 1, '2019-05-06 04:03:04'),
(714, 'property', 'Update property: 84', 1, '2019-05-06 04:04:35'),
(715, 'property', 'Update property: 83', 1, '2019-05-06 04:04:43'),
(716, 'page', 'Update page: 7', 1, '2019-05-06 04:05:38'),
(717, 'page', 'Update page: 7', 1, '2019-05-06 04:17:21'),
(718, 'room', 'Insert room: 42', 1, '2019-05-06 04:21:29'),
(719, 'room', 'Update room: 42', 1, '2019-05-06 04:24:29'),
(720, 'room', 'Update room: 42', 1, '2019-05-06 04:25:16'),
(721, 'page', 'Update page: 10', 1, '2019-05-06 04:37:43'),
(722, 'page', 'Update page: 10', 1, '2019-05-06 04:39:00'),
(723, 'room', 'Update room: 42', 1, '2019-05-06 04:48:50'),
(724, 'room', 'Update room: 42', 1, '2019-05-07 09:43:11'),
(725, 'users', 'delete users: 50', 1, '2019-05-07 10:46:14'),
(726, 'users', 'delete users: 47', 1, '2019-05-07 10:47:03'),
(727, 'users', 'delete users: 48', 1, '2019-05-07 10:47:05'),
(728, 'users', 'Update users: 46', 1, '2019-05-07 10:55:11'),
(729, 'account', 'Update account: 122', 1, '2019-05-07 10:57:12'),
(730, 'account', 'Update account: 121', 1, '2019-05-07 10:57:18'),
(731, 'account', 'Update account: 120', 1, '2019-05-07 10:57:24'),
(732, 'account', 'Update account: 119', 1, '2019-05-07 10:57:39'),
(733, 'account', 'Update account: 117', 1, '2019-05-07 10:57:52'),
(734, 'renter', 'Update renter: 8', 1, '2019-05-07 11:17:13'),
(735, 'room', 'Update room: 41', 1, '2019-05-07 11:28:06'),
(736, 'room', 'Update room: 40', 1, '2019-05-07 11:28:08'),
(737, 'room', 'Update room: 42', 1, '2019-05-07 11:28:22'),
(738, 'account', 'delete account: 126', 1, '2019-05-07 03:54:01'),
(739, 'account', 'delete account: 55', 1, '2019-05-07 03:54:30'),
(740, 'account', 'delete account: 54', 1, '2019-05-07 03:54:33'),
(741, 'account', 'delete account: 53', 1, '2019-05-07 03:54:35'),
(742, 'account', 'delete account: 52', 1, '2019-05-07 03:54:43'),
(743, 'account', 'delete account: 51', 1, '2019-05-07 03:54:44'),
(744, 'account', 'delete account: 50', 1, '2019-05-07 03:54:44'),
(745, 'account', 'delete account: 49', 1, '2019-05-07 03:54:52'),
(746, 'account', 'delete account: 48', 1, '2019-05-07 03:54:52'),
(747, 'account', 'delete account: 47', 1, '2019-05-07 03:54:52'),
(748, 'account', 'Update account: 120', 1, '2019-05-08 09:32:57'),
(749, 'contact', 'Update contact: 47', 1, '2019-05-08 09:42:55'),
(750, 'message_system', 'Sửa message_system có id là 21', 1, '2019-05-08 09:47:37'),
(751, 'message_system', 'Sửa message_system có id là 19', 1, '2019-05-08 09:47:48'),
(752, 'message_system', 'Sửa message_system có id là 18', 1, '2019-05-08 09:47:55'),
(753, 'message_system', 'Sửa message_system có id là 21', 1, '2019-05-08 09:48:13'),
(754, 'message_system', 'Sửa message_system có id là 19', 1, '2019-05-08 09:48:18'),
(755, 'message_system', 'Sửa message_system có id là 18', 1, '2019-05-08 09:48:24'),
(756, 'account', 'Update account: 107', 1, '2019-05-08 02:19:34'),
(757, 'room', 'Update room: 23', 1, '2019-05-08 03:24:15'),
(758, 'system_menu', 'Update system_menu: 19', 1, '2019-05-08 05:46:56'),
(759, 'system_menu', 'Update system_menu: 18', 1, '2019-05-08 05:47:01'),
(760, 'system_menu', 'Update system_menu: 10', 1, '2019-05-08 05:47:42'),
(761, 'system_menu', 'Update system_menu: 11', 1, '2019-05-08 05:48:01'),
(762, 'system_menu', 'Update system_menu: 12', 1, '2019-05-08 05:48:04'),
(763, 'account', 'Update account: 68', 1, '2019-05-09 09:35:47'),
(764, 'room', 'Update room: 46', 1, '2019-05-09 12:08:28'),
(765, 'room', 'Update room: 34', 1, '2019-05-09 01:50:20'),
(766, 'room', 'Update room: 34', 1, '2019-05-09 01:50:31'),
(767, 'room', 'Update room: 33', 1, '2019-05-09 01:51:06'),
(768, 'room', 'Update room: 30', 1, '2019-05-09 01:51:24'),
(769, 'room', 'Update room: 30', 1, '2019-05-09 01:51:30'),
(770, 'room', 'Update room: 25', 1, '2019-05-09 01:51:54'),
(771, 'room', 'Update room: 28', 1, '2019-05-09 01:52:08'),
(772, 'room', 'Update room: 24', 1, '2019-05-09 01:52:35'),
(773, 'room', 'Update room: 23', 1, '2019-05-09 01:52:50'),
(774, 'room', 'Update room: 20', 1, '2019-05-09 01:53:01'),
(775, 'room', 'Update room: 2', 1, '2019-05-09 01:53:11'),
(776, 'room', 'Update room: 1', 1, '2019-05-09 01:53:23'),
(777, 'review', 'Update review: 17', 1, '2019-05-09 02:32:05'),
(778, 'review', 'Update review: 14', 1, '2019-05-09 02:32:14'),
(779, 'page', 'Update page: 7', 1, '2019-05-09 03:08:21'),
(780, 'account', 'Insert account: 133', 1, '2019-05-09 03:40:01'),
(781, 'account', 'delete account: 133', 1, '2019-05-09 03:42:29'),
(782, 'room', 'Update room: 45', 1, '2019-05-09 03:56:29'),
(783, 'room', 'Update room: 44', 1, '2019-05-09 03:56:32'),
(784, 'room', 'Update room: 34', 1, '2019-05-09 03:56:56'),
(785, 'room', 'Update room: 33', 1, '2019-05-09 03:57:12'),
(786, 'room', 'Update room: 34', 1, '2019-05-09 03:57:24'),
(787, 'room', 'Update room: 34', 1, '2019-05-09 03:58:31'),
(788, 'room', 'Update room: 25', 1, '2019-05-09 04:08:23'),
(789, 'room', 'Update room: 25', 1, '2019-05-09 04:09:05'),
(790, 'message_system', 'Sửa message_system có id là 27', 1, '2019-05-09 04:11:03'),
(791, 'room', 'Update room: 2', 1, '2019-05-09 05:07:59'),
(792, 'account', 'Update account: 117', 1, '2019-05-09 05:38:01'),
(793, 'room', 'Update room: 24', 1, '2019-05-09 07:07:05'),
(794, 'users', 'Update users: ', 1, '2019-05-09 07:15:50'),
(795, 'renter', 'Update renter: 3', 1, '2019-05-09 09:22:05'),
(796, 'renter', 'Update renter: 2', 1, '2019-05-09 09:22:08');
INSERT INTO `ap_log_action` (`id`, `action`, `note`, `uid`, `created_time`) VALUES
(797, 'room', 'Update room: 2', 1, '2019-05-10 10:21:35'),
(798, 'users', 'delete users: 49', 1, '2019-05-10 10:30:10'),
(799, 'users', 'delete users: 46', 1, '2019-05-10 10:30:12'),
(800, 'users', 'delete users: 52', 1, '2019-05-10 10:30:17'),
(801, 'account', 'delete account: 117', 1, '2019-05-10 11:21:36'),
(802, 'room', 'Update room: 49', 1, '2019-05-10 11:50:53'),
(803, 'message_system', 'Sửa message_system có id là 33', 1, '2019-05-10 03:51:21'),
(804, 'message_system', 'Sửa message_system có id là 33', 1, '2019-05-10 03:51:39'),
(805, 'room', 'Update room: 34', 1, '2019-05-11 12:22:23'),
(806, 'room', 'Insert room: 52', 1, '2019-05-11 01:45:05'),
(807, 'room', 'Update room: 52', 1, '2019-05-11 01:49:32'),
(808, 'room', 'Update room: 52', 1, '2019-05-11 02:34:40'),
(809, 'room', 'Update room: 52', 1, '2019-05-11 02:36:48'),
(810, 'room', 'Update room: 52', 1, '2019-05-11 02:37:33'),
(811, 'room', 'Update room: 52', 1, '2019-05-11 02:37:48'),
(812, 'room', 'Update room: 52', 1, '2019-05-11 02:43:45'),
(813, 'property', 'Update property: 52', 1, '2019-05-11 05:31:25'),
(814, 'room', 'Update room: 34', 1, '2019-05-11 05:32:30'),
(815, 'room', 'Update room: 33', 1, '2019-05-11 05:33:26'),
(816, 'users', 'Update users: 56', 1, '2019-05-11 05:38:18'),
(817, 'property', 'Update property: 78', 56, '2019-05-11 05:39:10'),
(818, 'property', 'Update property: 77', 56, '2019-05-11 05:39:12'),
(819, 'property', 'Update property: 66', 56, '2019-05-11 05:40:04'),
(820, 'property', 'Update property: 65', 56, '2019-05-11 05:40:16'),
(821, 'property', 'Update property: 64', 56, '2019-05-11 05:40:54'),
(822, 'property', 'Update property: 63', 56, '2019-05-11 05:41:26'),
(823, 'property', 'Update property: 62', 56, '2019-05-11 05:41:47'),
(824, 'property', 'Update property: 59', 56, '2019-05-11 05:42:53'),
(825, 'property', 'Update property: 58', 56, '2019-05-11 05:43:02'),
(826, 'property', 'Update property: 56', 56, '2019-05-11 05:43:33'),
(827, 'property', 'Update property: 55', 56, '2019-05-11 05:43:45'),
(828, 'property', 'Update property: 48', 56, '2019-05-11 05:48:33'),
(829, 'property', 'Update property: 47', 56, '2019-05-11 05:48:44'),
(830, 'property', 'Update property: 46', 56, '2019-05-11 05:48:57'),
(831, 'property', 'Update property: 45', 56, '2019-05-11 05:49:24'),
(832, 'property', 'Update property: 44', 56, '2019-05-11 05:49:36'),
(833, 'property', 'Update property: 43', 56, '2019-05-11 05:49:51'),
(834, 'property', 'Update property: 42', 56, '2019-05-11 05:50:36'),
(835, 'property', 'Update property: 41', 56, '2019-05-11 05:50:46'),
(836, 'property', 'Update property: 40', 56, '2019-05-11 05:51:00'),
(837, 'property', 'Update property: 39', 56, '2019-05-11 05:51:15'),
(838, 'property', 'Update property: 38', 56, '2019-05-11 05:51:29'),
(839, 'property', 'Update property: 37', 56, '2019-05-11 05:51:38'),
(840, 'property', 'Update property: 49', 56, '2019-05-11 05:57:47'),
(841, 'property', 'Update property: 50', 56, '2019-05-11 05:58:17'),
(842, 'property', 'Update property: 54', 56, '2019-05-11 05:58:40'),
(843, 'property', 'Update property: 42', 56, '2019-05-11 05:58:59'),
(844, 'property', 'Update property: 51', 1, '2019-05-11 05:59:36'),
(845, 'property', 'Update property: 75', 1, '2019-05-11 06:00:22'),
(846, 'property', 'Update property: 74', 1, '2019-05-11 06:02:49'),
(847, 'property', 'Update property: 73', 1, '2019-05-11 06:03:10'),
(848, 'property', 'Update property: 61', 1, '2019-05-11 06:03:34'),
(849, 'property', 'Update property: 76', 56, '2019-05-11 11:39:42'),
(850, 'property', 'Update property: 72', 56, '2019-05-11 11:40:14'),
(851, 'property', 'Update property: 67', 56, '2019-05-11 11:40:44'),
(852, 'property', 'Update property: 61', 56, '2019-05-11 11:47:35'),
(853, 'property', 'Update property: 55', 56, '2019-05-11 11:48:17'),
(854, 'room', 'Update room: 34', 56, '2019-05-12 12:33:55'),
(855, 'room', 'Update room: 53', 1, '2019-05-12 12:45:38'),
(856, 'room', 'Insert room: 55', 56, '2019-05-13 10:52:06'),
(857, 'room', 'Update room: 55', 56, '2019-05-13 11:15:55'),
(858, 'room', 'Insert room: 56', 1, '2019-05-13 11:25:46'),
(859, 'room', 'Update room: 56', 1, '2019-05-13 11:32:39'),
(860, 'room', 'Update room: 55', 1, '2019-05-13 12:31:59'),
(861, 'room', 'Update room: 51', 1, '2019-05-13 12:32:12'),
(862, 'room', 'Update room: 57', 56, '2019-05-13 01:56:23'),
(863, 'room', 'Update room: 57', 56, '2019-05-13 01:56:36'),
(864, 'room', 'Update room: 56', 56, '2019-05-13 01:57:35'),
(865, 'room', 'Insert room: 58', 56, '2019-05-13 02:00:49'),
(866, 'room', 'Update room: 58', 56, '2019-05-13 02:01:44'),
(867, 'room', 'Update room: 49', 1, '2019-05-13 02:22:28'),
(868, 'room', 'Update room: 56', 1, '2019-05-13 02:29:50'),
(869, 'room', 'Update room: 56', 1, '2019-05-13 02:31:08'),
(870, 'room', 'Update room: 56', 1, '2019-05-13 02:47:26'),
(871, 'category', 'Insert category: 35', 56, '2019-05-13 02:58:28'),
(872, 'room', 'Update room: 56', 1, '2019-05-13 03:01:25'),
(873, 'category', 'Update category: 35', 56, '2019-05-13 03:01:31'),
(874, 'room', 'Update room: 56', 1, '2019-05-13 03:02:29'),
(875, 'room', 'Update room: 56', 1, '2019-05-13 03:03:26'),
(876, 'room', 'Update room: 58', 1, '2019-05-13 03:03:33'),
(877, 'room', 'Update room: 57', 1, '2019-05-13 03:03:40'),
(878, 'room', 'Update room: 55', 1, '2019-05-13 03:03:43'),
(879, 'room', 'Update room: 54', 1, '2019-05-13 03:03:46'),
(880, 'category', 'Update category: 30', 56, '2019-05-13 03:04:17'),
(881, 'category', 'Update category: 30', 56, '2019-05-13 03:05:27'),
(882, 'room', 'Update room: 52', 1, '2019-05-13 03:05:54'),
(883, 'room', 'Update room: 50', 1, '2019-05-13 03:06:01'),
(884, 'room', 'Update room: 48', 1, '2019-05-13 03:06:03'),
(885, 'room', 'Update room: 47', 1, '2019-05-13 03:06:05'),
(886, 'room', 'Update room: 43', 1, '2019-05-13 03:06:07'),
(887, 'room', 'Update room: 42', 1, '2019-05-13 03:06:11'),
(888, 'property', 'Insert property: 0', 56, '2019-05-13 03:09:59'),
(889, 'property', 'Update property: 85', 56, '2019-05-13 03:10:30'),
(890, 'room', 'Update room: 33', 56, '2019-05-13 03:10:54'),
(891, 'property', 'Insert property: 0', 56, '2019-05-13 03:11:48'),
(892, 'property', 'Update property: 86', 56, '2019-05-13 03:13:22'),
(893, 'property', 'Update property: 86', 56, '2019-05-13 03:14:45'),
(894, 'room', 'Update room: 34', 1, '2019-05-13 03:22:56'),
(895, 'room', 'Update room: 33', 1, '2019-05-13 03:25:57'),
(896, 'room', 'Update room: 30', 1, '2019-05-13 03:28:01'),
(897, 'room', 'Update room: 29', 1, '2019-05-13 03:29:52'),
(898, 'room', 'Update room: 26', 1, '2019-05-13 03:30:09'),
(899, 'room', 'Update room: 30', 1, '2019-05-13 03:30:32'),
(900, 'room', 'Update room: 28', 1, '2019-05-13 03:30:40'),
(901, 'room', 'Update room: 1', 1, '2019-05-13 03:32:10'),
(902, 'room', 'Update room: 1', 1, '2019-05-13 03:33:07'),
(903, 'room', 'Update room: 2', 1, '2019-05-13 03:34:37'),
(904, 'room', 'Update room: 20', 1, '2019-05-13 03:37:31'),
(905, 'room', 'Update room: 23', 1, '2019-05-13 03:39:09'),
(906, 'room', 'Update room: 24', 1, '2019-05-13 03:42:23'),
(907, 'room', 'Update room: 25', 1, '2019-05-13 03:45:22'),
(908, 'room', 'Update room: 1', 1, '2019-05-13 04:12:33'),
(909, 'room', 'Update room: 33', 1, '2019-05-13 04:17:02'),
(910, 'room', 'Update room: 30', 56, '2019-05-13 04:18:39'),
(911, 'room', 'Update room: 30', 1, '2019-05-13 04:21:33'),
(912, 'room', 'Update room: 56', 1, '2019-05-13 04:43:43'),
(913, 'room', 'Update room: 56', 1, '2019-05-13 04:45:24'),
(914, 'account', 'Update account: 139', 56, '2019-05-13 05:43:38'),
(915, 'account', 'Update account: 131', 56, '2019-05-13 05:44:03'),
(916, 'account', 'delete account: 32', 1, '2019-05-13 08:11:34'),
(917, 'review', 'Update review: 13', 1, '2019-05-13 08:25:29'),
(918, 'review', 'Update review: 14', 1, '2019-05-13 08:25:35'),
(919, 'review', 'Update review: 1', 1, '2019-05-13 08:25:44'),
(920, 'review', 'Update review: 19', 1, '2019-05-13 08:25:50'),
(921, 'category', 'Update category: 30', 1, '2019-05-14 12:36:09'),
(922, 'category', 'Update category: 16', 1, '2019-05-14 12:36:19'),
(923, 'category', 'Update category: 5', 1, '2019-05-14 12:36:25'),
(924, 'category', 'Update category: 3', 1, '2019-05-14 12:36:31'),
(925, 'property', 'Update property: 53', 56, '2019-05-14 09:00:20'),
(926, 'property', 'Update property: 57', 56, '2019-05-14 09:00:41'),
(927, 'property', 'Update property: 60', 56, '2019-05-14 09:01:02'),
(928, 'room', 'Update room: 25', 1, '2019-05-14 09:17:58'),
(929, 'room', 'Update room: 25', 1, '2019-05-14 09:18:18'),
(930, 'category', 'Update category: 16', 1, '2019-05-14 09:24:38'),
(931, 'account', 'Update account: 77', 1, '2019-05-14 09:35:12'),
(932, 'review', 'Update review: 40', 1, '2019-05-14 09:44:40'),
(933, 'review', 'Update review: 38', 1, '2019-05-14 09:44:43'),
(934, 'review', 'Update review: 37', 1, '2019-05-14 09:44:47'),
(935, 'review', 'Update review: 36', 1, '2019-05-14 09:44:52'),
(936, 'review', 'Update review: 35', 1, '2019-05-14 09:44:56'),
(937, 'review', 'Update review: 28', 1, '2019-05-14 09:45:00'),
(938, 'review', 'Update review: 27', 1, '2019-05-14 09:45:04'),
(939, 'review', 'Update review: 1', 1, '2019-05-14 09:45:11'),
(940, 'review', 'Update review: 18', 1, '2019-05-14 09:45:14'),
(941, 'review', 'Update review: 15', 1, '2019-05-14 09:45:17'),
(942, 'review', 'Update review: 14', 1, '2019-05-14 09:45:20'),
(943, 'review', 'Update review: 13', 1, '2019-05-14 09:45:23'),
(944, 'review', 'Update review: 21', 1, '2019-05-14 09:45:27'),
(945, 'review', 'Update review: 20', 1, '2019-05-14 09:45:30'),
(946, 'review', 'Update review: 19', 1, '2019-05-14 09:45:34'),
(947, 'room', 'Update room: 23', 1, '2019-05-14 09:48:36'),
(948, 'room', 'Update room: 2', 56, '2019-05-14 11:24:35'),
(949, 'room', 'Update room: 52', 56, '2019-05-14 11:39:37'),
(950, 'room', 'Update room: 62', 1, '2019-05-14 12:32:15'),
(951, 'room', 'Update room: 63', 1, '2019-05-14 12:32:18'),
(952, 'room', 'Update room: 64', 1, '2019-05-14 12:32:20'),
(953, 'room', 'Update room: 65', 1, '2019-05-14 12:32:23'),
(954, 'room', 'Update room: 66', 1, '2019-05-14 12:38:59'),
(955, 'room', 'Update room: 61', 56, '2019-05-14 12:44:02'),
(956, 'account', 'Update account: 135', 1, '2019-05-14 05:17:43'),
(957, 'room', 'Update room: 69', 56, '2019-05-15 09:51:16'),
(958, 'room', 'Update room: 68', 56, '2019-05-15 09:51:18'),
(959, 'room', 'Update room: 67', 56, '2019-05-15 09:51:20'),
(960, 'room', 'Update room: 60', 56, '2019-05-15 09:51:24'),
(961, 'room', 'Update room: 59', 56, '2019-05-15 09:51:27'),
(962, 'room', 'Update room: 25', 56, '2019-05-15 10:03:22'),
(963, 'account', 'Update account: 135', 56, '2019-05-15 11:06:09'),
(964, 'account', 'Update account: 135', 56, '2019-05-15 11:06:41'),
(965, 'account', 'Update account: 135', 56, '2019-05-15 11:06:58'),
(966, 'account', 'Update account: 135', 56, '2019-05-15 11:07:15'),
(967, 'account', 'delete account: 104', 56, '2019-05-15 11:51:52'),
(968, 'account', 'delete account: 104', 56, '2019-05-15 11:52:13'),
(969, 'account', 'delete account: 104', 56, '2019-05-15 11:57:47'),
(970, 'room', 'Update room: 24', 56, '2019-05-15 01:34:10'),
(971, 'room', 'Update room: 52', 56, '2019-05-15 04:26:20'),
(972, 'account', 'Update account: 135', 56, '2019-05-15 04:39:28'),
(973, 'account', 'Insert account: 142', 56, '2019-05-15 04:43:07'),
(974, 'account', 'Update account: 135', 56, '2019-05-15 04:52:29'),
(975, 'users', 'Insert users: 0', 56, '2019-05-16 04:11:02'),
(976, 'users', 'delete users: 57', 56, '2019-05-16 04:11:37'),
(977, 'account', 'delete account: 142', 56, '2019-05-16 05:18:23'),
(978, 'account', 'Update account: 135', 56, '2019-05-17 09:31:44'),
(979, 'contact', 'Update contact: 57', 56, '2019-05-17 01:57:25'),
(980, 'page', 'Update page: 7', 56, '2019-05-18 09:35:54'),
(981, 'page', 'Update page: 7', 56, '2019-05-18 09:45:33'),
(982, 'room', 'Update room: 72', 56, '2019-05-18 09:56:56'),
(983, 'room', 'Update room: 2', 56, '2019-05-18 09:57:52'),
(984, 'property', 'Update property: 61', 56, '2019-05-18 11:01:55'),
(985, 'room', 'Update room: 73', 56, '2019-05-18 01:53:47'),
(986, 'room', 'Update room: 74', 56, '2019-05-18 01:53:50'),
(987, 'room', 'Update room: 75', 56, '2019-05-18 01:53:52'),
(988, 'room', 'Update room: 76', 56, '2019-05-18 01:53:55'),
(989, 'room', 'Update room: 77', 56, '2019-05-18 01:53:58'),
(990, 'room', 'Update room: 71', 56, '2019-05-18 01:54:01'),
(991, 'message_system', 'Sửa message_system có id là 43', 56, '2019-05-18 02:25:35'),
(992, 'room', 'Update room: 2', 56, '2019-05-18 03:28:12'),
(993, 'system_menu', 'Update system_menu: 9', 1, '2019-05-18 04:39:05'),
(994, 'system_menu', 'Update system_menu: 20', 1, '2019-05-18 04:39:05'),
(995, 'system_menu', 'Update system_menu: 21', 1, '2019-05-18 04:39:05'),
(996, 'system_menu', 'Update system_menu: 24', 1, '2019-05-18 04:39:05'),
(997, 'system_menu', 'Update system_menu: 25', 1, '2019-05-18 04:39:05'),
(998, 'system_menu', 'Update system_menu: 26', 1, '2019-05-18 04:39:05'),
(999, 'system_menu', 'Update system_menu: 27', 1, '2019-05-18 04:39:05'),
(1000, 'system_menu', 'Update system_menu: 8', 1, '2019-05-18 04:39:16'),
(1001, 'system_menu', 'Update system_menu: 34', 1, '2019-05-18 04:39:16'),
(1002, 'system_menu', 'Update system_menu: 35', 1, '2019-05-18 04:39:16'),
(1003, 'users', 'Update users: ', 1, '2019-05-19 11:47:51'),
(1004, 'system_menu', 'Update system_menu: 7', 1, '2019-05-21 11:16:39'),
(1005, 'system_menu', 'Insert system_menu: 26', 1, '2019-05-21 11:27:25'),
(1006, 'system_menu', 'Insert system_menu: 27', 1, '2019-05-21 11:32:38'),
(1007, 'system_menu', 'Update system_menu: 17', 1, '2019-05-21 11:33:23'),
(1008, 'system_menu', 'Update system_menu: 27', 1, '2019-05-21 11:33:33'),
(1009, 'doc', 'Update doc: 2', 1, '2019-05-21 11:30:07'),
(1010, 'doc', 'Update doc: 2', 1, '2019-05-21 11:32:11'),
(1011, 'doc', 'Update doc: 2', 1, '2019-05-21 11:48:45'),
(1012, 'doc', 'Update doc: 2', 1, '2019-05-22 12:01:13'),
(1013, 'doc', 'Update doc: 2', 1, '2019-05-22 12:02:48'),
(1014, 'doc', 'Update doc: 2', 1, '2019-05-22 12:09:28'),
(1015, 'doc', 'Update doc: 2', 1, '2019-05-22 12:09:46'),
(1016, 'doc', 'Update doc: 2', 1, '2019-05-22 12:10:57'),
(1017, 'doc', 'Insert doc: 3', 1, '2019-05-22 12:20:09'),
(1018, 'doc', 'Update doc: 3', 1, '2019-05-22 12:54:49'),
(1019, 'doc', 'Update doc: 3', 1, '2019-05-22 12:55:21'),
(1020, 'system_menu', 'Insert system_menu: 28', 1, '2019-05-22 01:56:45'),
(1021, 'doc', 'Update doc: 2', 1, '2019-05-22 10:29:50'),
(1022, 'doc', 'Update doc: 2', 1, '2019-05-22 10:29:52'),
(1023, 'doc', 'Update doc: 2', 1, '2019-05-22 10:29:53'),
(1024, 'doc', 'Update doc: 2', 1, '2019-05-22 10:30:49'),
(1025, 'doc', 'Update doc: 2', 1, '2019-05-22 10:31:14'),
(1026, 'doc', 'Update doc: 2', 1, '2019-05-22 10:32:34'),
(1027, 'doc', 'Update doc: 2', 1, '2019-05-22 11:15:02'),
(1028, 'doc', 'Update doc: 2', 1, '2019-05-22 11:16:22'),
(1029, 'doc', 'Update doc: 1', 1, '2019-05-22 11:17:08'),
(1030, 'doc', 'Update doc: 2', 1, '2019-05-22 11:29:03'),
(1031, 'doc', 'Update doc: 2', 1, '2019-05-22 11:47:18'),
(1032, 'doc', 'Update doc: 2', 1, '2019-05-22 11:48:41'),
(1033, 'doc', 'Update doc: 2', 1, '2019-05-22 11:57:43'),
(1034, 'doc', 'Update doc: 2', 1, '2019-05-22 12:04:32'),
(1035, 'doc', 'Update doc: 2', 1, '2019-05-22 02:06:59'),
(1036, 'doc', 'Update doc: 2', 1, '2019-05-22 02:08:27'),
(1037, 'doc', 'Update doc: 2', 1, '2019-05-22 02:11:43'),
(1038, 'doc', 'Update doc: 2', 1, '2019-05-22 02:12:05'),
(1039, 'doc', 'Update doc: 2', 1, '2019-05-22 02:14:16'),
(1040, 'doc', 'Update doc: 2', 1, '2019-05-22 02:24:59'),
(1041, 'doc', 'Update doc: 2', 1, '2019-05-22 02:25:34'),
(1042, 'doc', 'Update doc: 2', 1, '2019-05-22 02:36:22'),
(1043, 'doc', 'Update doc: 2', 1, '2019-05-22 02:38:06'),
(1044, 'doc', 'Update doc: 2', 1, '2019-05-22 02:40:29'),
(1045, 'doc', 'Update doc: 2', 1, '2019-05-22 02:40:42'),
(1046, 'doc', 'Update doc: 2', 1, '2019-05-22 03:06:08'),
(1047, 'doc', 'Update doc: 2', 1, '2019-05-22 03:19:08'),
(1048, 'doc', 'Insert doc: 4', 1, '2019-05-22 03:48:07'),
(1049, 'doc', 'Update doc: 4', 1, '2019-05-22 04:00:20'),
(1050, 'doc', 'Update doc: 2', 1, '2019-05-22 04:00:53'),
(1051, 'doc', 'Update doc: 2', 1, '2019-05-22 04:15:26'),
(1052, 'doc', 'Update doc: 1', 1, '2019-05-22 04:18:41'),
(1053, 'doc', 'Insert doc: 5', 1, '2019-05-22 04:40:39'),
(1054, 'doc', 'Update doc: 5', 1, '2019-05-22 04:41:01'),
(1055, 'doc', 'Update doc: 5', 1, '2019-05-22 04:41:41'),
(1056, 'doc', 'Update doc: 5', 1, '2019-05-22 04:41:59'),
(1057, 'doc', 'Update doc: 2', 1, '2019-05-22 04:59:44'),
(1058, 'doc', 'Update doc: 2', 1, '2019-05-22 05:00:19'),
(1059, 'doc', 'Update doc: 5', 1, '2019-05-22 05:01:02'),
(1060, 'type_doc', 'Insert type_doc: 4', 1, '2019-05-23 03:56:37'),
(1061, 'doc', 'Update doc: 4', 1, '2019-05-23 03:57:54'),
(1062, 'doc', 'Update doc: 4', 1, '2019-05-23 03:58:28'),
(1063, 'doc', 'Update doc: 4', 1, '2019-05-23 04:01:35'),
(1064, 'doc', 'Update doc: 4', 1, '2019-05-23 04:01:56'),
(1065, 'doc', 'Update doc: 4', 1, '2019-05-23 04:06:47'),
(1066, 'type_doc', 'Insert type_doc: 5', 1, '2019-05-23 04:07:43'),
(1067, 'doc', 'Update doc: 1', 1, '2019-05-23 04:08:12'),
(1068, 'doc', 'Update doc: 1', 1, '2019-05-23 04:09:55'),
(1069, 'doc', 'Update doc: 5', 1, '2019-05-23 04:13:03'),
(1070, 'doc', 'Update doc: 5', 1, '2019-05-23 04:15:22'),
(1071, 'doc', 'Update doc: 2', 1, '2019-05-23 04:16:04'),
(1072, 'doc', 'Update doc: 2', 1, '2019-05-23 04:16:45'),
(1073, 'doc', 'Update doc: 2', 1, '2019-05-23 04:16:46'),
(1074, 'doc', 'Update doc: 1', 1, '2019-05-23 04:19:29'),
(1075, 'doc', 'Update doc: 1', 1, '2019-05-23 04:19:56'),
(1076, 'doc', 'Update doc: 1', 1, '2019-05-23 04:20:17'),
(1077, 'doc', 'Update doc: 1', 1, '2019-05-23 04:22:41'),
(1078, 'doc', 'Update doc: 1', 1, '2019-05-23 04:22:55'),
(1079, 'doc', 'Update doc: 1', 1, '2019-05-23 04:23:02'),
(1080, 'doc', 'Update doc: 1', 1, '2019-05-23 04:23:21'),
(1081, 'doc', 'Update doc: 5', 1, '2019-05-23 04:31:50'),
(1082, 'doc', 'Update doc: 5', 1, '2019-05-23 04:32:11'),
(1083, 'doc', 'Update doc: 5', 1, '2019-05-23 04:32:34'),
(1084, 'doc', 'Update doc: 2', 1, '2019-05-23 04:33:18'),
(1085, 'doc', 'Update doc: 2', 1, '2019-05-23 04:33:36'),
(1086, 'doc', 'Update doc: 2', 1, '2019-05-23 04:36:04'),
(1087, 'doc', 'Update doc: 2', 1, '2019-05-23 04:36:54'),
(1088, 'doc', 'Update doc: 2', 1, '2019-05-23 04:37:08'),
(1089, 'doc', 'Update doc: 2', 1, '2019-05-23 04:37:14'),
(1090, 'doc', 'Update doc: 2', 1, '2019-05-23 04:37:29'),
(1091, 'doc', 'Update doc: 5', 1, '2019-05-23 04:39:58'),
(1092, 'type_doc', 'Insert type_doc: 6', 1, '2019-05-23 04:45:27'),
(1093, 'doc', 'Insert doc: 6', 1, '2019-05-23 04:46:27'),
(1094, 'doc', 'Update doc: 6', 1, '2019-05-23 04:47:51'),
(1095, 'doc', 'Update doc: 5', 1, '2019-05-23 04:48:04'),
(1096, 'doc', 'Update doc: 2', 1, '2019-05-23 04:51:03'),
(1097, 'doc', 'Update doc: 2', 1, '2019-05-23 04:52:13'),
(1098, 'doc', 'Update doc: 2', 1, '2019-05-23 04:52:57'),
(1099, 'doc', 'Update doc: 2', 1, '2019-05-23 04:53:32'),
(1100, 'doc', 'Update doc: 2', 1, '2019-05-23 04:55:03'),
(1101, 'doc', 'Update doc: 2', 1, '2019-05-23 05:04:23'),
(1102, 'doc', 'Update doc: 2', 1, '2019-05-23 05:04:39'),
(1103, 'doc', 'Update doc: 2', 1, '2019-05-23 05:04:44'),
(1104, 'doc', 'Update doc: 2', 1, '2019-05-23 05:06:27'),
(1105, 'doc', 'Update doc: 2', 1, '2019-05-23 05:08:43'),
(1106, 'doc', 'Update doc: 2', 1, '2019-05-23 05:09:26'),
(1107, 'doc', 'Update doc: 2', 1, '2019-05-23 05:10:49'),
(1108, 'doc', 'Update doc: 2', 1, '2019-05-23 05:11:18'),
(1109, 'doc', 'Update doc: 6', 1, '2019-05-23 05:11:40'),
(1110, 'doc', 'Update doc: 5', 1, '2019-05-23 05:15:07'),
(1111, 'doc', 'Update doc: 5', 1, '2019-05-23 11:09:43'),
(1112, 'doc', 'Update doc: 5', 1, '2019-05-23 11:10:24'),
(1113, 'doc', 'Update doc: 5', 1, '2019-05-23 11:10:57'),
(1114, 'doc', 'Update doc: 5', 1, '2019-05-23 11:11:16'),
(1115, 'doc', 'Update doc: 5', 1, '2019-05-23 11:15:23'),
(1116, 'doc', 'Update doc: 2', 1, '2019-05-23 11:15:39'),
(1117, 'doc', 'Update doc: 6', 1, '2019-05-23 11:17:41'),
(1118, 'doc', 'Update doc: 6', 1, '2019-05-23 11:18:00'),
(1119, 'doc', 'Update doc: 5', 1, '2019-05-23 11:22:09'),
(1120, 'doc', 'Update doc: 2', 1, '2019-05-23 11:22:23'),
(1121, 'doc', 'Update doc: 5', 1, '2019-05-23 11:41:02'),
(1122, 'doc', 'Update doc: 5', 1, '2019-05-23 11:44:11'),
(1123, 'doc', 'Update doc: 5', 1, '2019-05-23 11:44:31'),
(1124, 'doc', 'Update doc: 5', 1, '2019-05-23 11:46:57'),
(1125, 'doc', 'Update doc: 5', 1, '2019-05-23 11:47:11'),
(1126, 'doc', 'Update doc: 5', 1, '2019-05-23 11:49:56'),
(1127, 'doc', 'Update doc: 2', 1, '2019-05-23 11:50:05'),
(1128, 'doc', 'Update doc: 5', 1, '2019-05-23 11:50:16'),
(1129, 'doc', 'Update doc: 5', 1, '2019-05-23 11:53:08'),
(1130, 'doc', 'Update doc: 2', 1, '2019-05-23 11:53:17'),
(1131, 'doc', 'Update doc: 5', 1, '2019-05-24 12:02:09'),
(1132, 'doc', 'Update doc: 2', 1, '2019-05-24 12:08:10'),
(1133, 'doc', 'Update doc: 2', 1, '2019-05-24 12:08:13'),
(1134, 'doc', 'Update doc: 6', 1, '2019-05-24 12:08:32'),
(1135, 'doc', 'Update doc: 6', 1, '2019-05-24 12:08:42'),
(1136, 'doc', 'Update doc: 6', 1, '2019-05-24 12:08:58'),
(1137, 'doc', 'Update doc: 6', 1, '2019-05-24 12:10:41'),
(1138, 'doc', 'Update doc: 6', 1, '2019-05-24 12:10:44'),
(1139, 'doc', 'Update doc: 6', 1, '2019-05-24 12:10:47'),
(1140, 'doc', 'Update doc: 6', 1, '2019-05-24 08:33:59'),
(1141, 'doc', 'Update doc: 6', 1, '2019-05-24 08:41:22'),
(1142, 'system_menu', 'Insert system_menu: 29', 1, '2019-05-24 08:47:22'),
(1143, 'system_menu', 'Insert system_menu: 30', 1, '2019-05-24 08:47:57'),
(1144, 'system_menu', 'Insert system_menu: 31', 1, '2019-05-24 08:48:18'),
(1145, 'system_menu', 'Update system_menu: 29', 1, '2019-05-24 08:50:11'),
(1146, 'system_menu', 'Insert system_menu: 32', 1, '2019-05-24 08:50:52'),
(1147, 'system_menu', 'Insert system_menu: 33', 1, '2019-05-24 08:51:45'),
(1148, 'system_menu', 'Update system_menu: 32', 1, '2019-05-24 08:52:02'),
(1149, 'system_menu', 'Update system_menu: 2', 1, '2019-05-24 08:57:21'),
(1150, 'system_menu', 'Update system_menu: 17', 1, '2019-05-24 08:58:10'),
(1151, 'system_menu', 'Insert system_menu: 34', 1, '2019-05-24 08:58:56'),
(1152, 'system_menu', 'Insert system_menu: 35', 1, '2019-05-24 08:59:18'),
(1153, 'system_menu', 'Insert system_menu: 36', 1, '2019-05-24 09:00:36'),
(1154, 'system_menu', 'Insert system_menu: 37', 1, '2019-05-24 09:01:44'),
(1155, 'system_menu', 'Insert system_menu: 38', 1, '2019-05-24 09:02:22'),
(1156, 'system_menu', 'Insert system_menu: 39', 1, '2019-05-24 09:02:25'),
(1157, 'system_menu', 'Insert system_menu: 40', 1, '2019-05-24 09:02:28'),
(1158, 'system_menu', 'Insert system_menu: 41', 1, '2019-05-24 09:02:32'),
(1159, 'system_menu', 'Insert system_menu: 42', 1, '2019-05-24 09:02:36'),
(1160, 'system_menu', 'Insert system_menu: 43', 1, '2019-05-24 09:02:39'),
(1161, 'system_menu', 'Insert system_menu: 44', 1, '2019-05-24 09:02:43'),
(1162, 'system_menu', 'Insert system_menu: 45', 1, '2019-05-24 09:02:51'),
(1163, 'system_menu', 'Update system_menu: 37', 1, '2019-05-24 09:03:29'),
(1164, 'system_menu', 'Insert system_menu: 46', 1, '2019-05-24 09:20:42'),
(1165, 'system_menu', 'Insert system_menu: 47', 1, '2019-05-24 09:21:46'),
(1166, 'system_menu', 'Insert system_menu: 48', 1, '2019-05-24 09:23:02'),
(1167, 'system_menu', 'Insert system_menu: 49', 1, '2019-05-24 09:24:46'),
(1168, 'system_menu', 'Update system_menu: 36', 1, '2019-05-24 09:25:18'),
(1169, 'system_menu', 'Update system_menu: 16', 1, '2019-05-24 09:26:38'),
(1170, 'system_menu', 'Update system_menu: 15', 1, '2019-05-24 09:26:46'),
(1171, 'system_menu', 'Update system_menu: 4', 1, '2019-05-24 09:27:27'),
(1172, 'system_menu', 'Update system_menu: 4', 1, '2019-05-24 09:29:46'),
(1173, 'system_menu', 'Insert system_menu: 51', 1, '2019-05-24 10:10:19'),
(1174, 'system_menu', 'Insert system_menu: 52', 1, '2019-05-24 10:31:58'),
(1175, 'system_menu', 'Insert system_menu: 53', 1, '2019-05-24 10:32:19'),
(1176, 'system_menu', 'Insert system_menu: 54', 1, '2019-05-24 10:33:17'),
(1177, 'system_menu', 'Insert system_menu: 55', 1, '2019-05-24 10:34:21'),
(1178, 'system_menu', 'Insert system_menu: 56', 1, '2019-05-24 10:35:16'),
(1179, 'system_menu', 'Insert system_menu: 57', 1, '2019-05-24 10:35:41'),
(1180, 'system_menu', 'Insert system_menu: 58', 1, '2019-05-24 10:35:58'),
(1181, 'system_menu', 'Insert system_menu: 59', 1, '2019-05-24 10:36:12'),
(1182, 'system_menu', 'Insert system_menu: 60', 1, '2019-05-24 10:36:23'),
(1183, 'type_doc', 'Update type_doc: 2', 1, '2019-05-24 10:38:24'),
(1184, 'type_doc', 'Update type_doc: 4', 1, '2019-05-24 10:38:31'),
(1185, 'type_doc', 'Update type_doc: 5', 1, '2019-05-24 10:38:38'),
(1186, 'type_doc', 'Update type_doc: 6', 1, '2019-05-24 10:38:47'),
(1187, 'type_doc', 'Insert type_doc: 7', 1, '2019-05-24 10:38:56'),
(1188, 'type_doc', 'Insert type_doc: 8', 1, '2019-05-24 10:39:05'),
(1189, 'type_doc', 'Insert type_doc: 9', 1, '2019-05-24 10:39:12'),
(1190, 'type_doc', 'Insert type_doc: 10', 1, '2019-05-24 10:39:20'),
(1191, 'type_doc', 'Insert type_doc: 11', 1, '2019-05-24 10:39:30'),
(1192, 'system_menu', 'Insert system_menu: 61', 1, '2019-05-24 10:41:25'),
(1193, 'users', 'Update users: ', 1, '2019-05-24 01:34:48'),
(1194, 'groups', 'Update groups: 18', 1, '2019-05-24 01:42:18'),
(1195, 'groups', 'Update groups: 17', 1, '2019-05-24 01:42:23'),
(1196, 'groups', 'Update groups: 16', 1, '2019-05-24 01:42:29'),
(1197, 'users', 'delete users: 56', 1, '2019-05-24 01:42:40'),
(1198, 'users', 'delete users: 55', 1, '2019-05-24 01:42:42'),
(1199, 'users', 'delete users: 51', 1, '2019-05-24 01:42:45'),
(1200, 'users', 'delete users: 36', 1, '2019-05-24 01:42:47'),
(1201, 'users', 'delete users: 34', 1, '2019-05-24 01:42:50'),
(1202, 'dm_chuc_vu', 'Insert dm_chuc_vu: 1', 1, '2019-05-29 09:10:00'),
(1203, 'dm_chuc_vu', 'Insert dm_chuc_vu: 2', 1, '2019-05-29 09:13:08'),
(1204, 'dm_chuc_vu', 'Insert dm_chuc_vu: 3', 1, '2019-05-29 09:13:17'),
(1205, 'dm_chuc_vu', 'Insert dm_chuc_vu: 4', 1, '2019-05-29 09:13:27'),
(1206, 'system_menu', 'Update system_menu: 30', 1, '2019-05-29 09:16:30'),
(1207, 'dm_don_vi_tinh', 'Insert dm_don_vi_tinh: 1', 1, '2019-05-29 09:32:36'),
(1208, 'dm_don_vi_tinh', 'Insert dm_don_vi_tinh: 2', 1, '2019-05-29 09:32:44'),
(1209, 'dm_ky_bao_cao', 'Insert dm_ky_bao_cao: 1', 1, '2019-05-29 09:41:29'),
(1210, 'dm_ky_bao_cao', 'Insert dm_ky_bao_cao: 2', 1, '2019-05-29 09:41:34'),
(1211, 'dm_ky_bao_cao', 'Insert dm_ky_bao_cao: 3', 1, '2019-05-29 09:41:41'),
(1212, 'dm_trinh_do', 'Insert dm_trinh_do: 1', 1, '2019-05-29 09:45:30'),
(1213, 'dm_trinh_do', 'Insert dm_trinh_do: 2', 1, '2019-05-29 09:45:37'),
(1214, 'dm_trinh_do', 'Update dm_trinh_do: 1', 1, '2019-05-29 09:46:04'),
(1215, 'dm_trinh_do', 'Update dm_trinh_do: 1', 1, '2019-05-29 09:46:27'),
(1216, 'dm_trinh_do', 'Update dm_trinh_do: 2', 1, '2019-05-29 09:46:38'),
(1217, 'dm_trinh_do', 'Insert dm_trinh_do: 3', 1, '2019-05-29 09:46:44'),
(1218, 'dm_trinh_do', 'Insert dm_trinh_do: 4', 1, '2019-05-29 09:46:50'),
(1219, 'dm_trinh_do', 'Insert dm_trinh_do: 5', 1, '2019-05-29 09:46:58'),
(1220, 'dm_don_vi_hc', 'Update dm_don_vi_hc: 97', 1, '2019-05-29 10:09:13'),
(1221, 'dm_ton_giao', 'Insert dm_ton_giao: 1', 1, '2019-05-29 11:48:51'),
(1222, 'dm_ton_giao', 'Insert dm_ton_giao: 2', 1, '2019-05-29 11:48:58'),
(1223, 'dm_chi_tieu', 'Insert dm_chi_tieu: 1', 1, '2019-05-29 11:51:45'),
(1224, 'dm_chi_tieu', 'Insert dm_chi_tieu: 2', 1, '2019-05-29 11:52:10'),
(1225, 'dm_chi_tieu', 'Insert dm_chi_tieu: 3', 1, '2019-05-29 11:52:20'),
(1226, 'dm_chi_tieu', 'Insert dm_chi_tieu: 4', 1, '2019-05-29 11:52:52'),
(1227, 'dm_chi_tieu', 'Insert dm_chi_tieu: 5', 1, '2019-05-29 02:22:58'),
(1228, 'dm_chi_tieu', 'Insert dm_chi_tieu: 6', 1, '2019-05-29 02:23:23'),
(1229, 'dm_chi_tieu', 'Insert dm_chi_tieu: 7', 1, '2019-05-29 02:23:48'),
(1230, 'dm_chi_tieu', 'Insert dm_chi_tieu: 8', 1, '2019-05-29 02:24:09'),
(1231, 'dm_chi_tieu', 'Insert dm_chi_tieu: 9', 1, '2019-05-29 02:24:26'),
(1232, 'dm_chi_tieu', 'Insert dm_chi_tieu: 10', 1, '2019-05-29 02:24:44'),
(1233, 'dm_chi_tieu', 'Insert dm_chi_tieu: 11', 1, '2019-05-29 02:24:56'),
(1234, 'dm_nhom_tuoi', 'Insert dm_nhom_tuoi: 1', 1, '2019-05-29 03:37:12'),
(1235, 'dm_nhom_tuoi', 'Insert dm_nhom_tuoi: 2', 1, '2019-05-29 03:37:23'),
(1236, 'dm_nhom_tuoi', 'Insert dm_nhom_tuoi: 3', 1, '2019-05-29 03:38:51'),
(1237, 'dm_nhom_tuoi', 'Insert dm_nhom_tuoi: 4', 1, '2019-05-29 03:39:11'),
(1238, 'dm_nhom_tuoi', 'Insert dm_nhom_tuoi: 5', 1, '2019-05-29 03:39:23'),
(1239, 'dm_nhom_tuoi', 'Insert dm_nhom_tuoi: 6', 1, '2019-05-29 03:39:39'),
(1240, 'loai_chi_tieu', 'Insert loai_chi_tieu: 7', 1, '2019-05-29 04:10:30'),
(1241, 'dm_trinh_do', 'Update dm_trinh_do: 2', 1, '2019-05-29 04:12:53'),
(1242, 'doc', 'Update doc: 1', 1, '2019-05-29 04:49:15'),
(1243, 'chi_tieu', 'Update chi_tieu: 00103', 1, '2019-05-30 01:08:08'),
(1244, 'chi_tieu', 'Update chi_tieu: 00102', 1, '2019-05-30 01:08:15'),
(1245, 'system_menu', 'Insert system_menu: 62', 1, '2019-05-30 09:14:45'),
(1246, 'system_menu', 'Insert system_menu: 63', 1, '2019-05-30 09:17:39'),
(1247, 'system_menu', 'Insert system_menu: 64', 1, '2019-05-30 09:18:29'),
(1248, 'system_menu', 'Update system_menu: 37', 1, '2019-05-30 09:25:23'),
(1249, 'system_menu', 'Update system_menu: 37', 1, '2019-05-30 09:26:01'),
(1250, 'doc_groups', 'Insert doc_groups: 1', 1, '2019-05-30 10:21:57'),
(1251, 'doc_groups', 'Insert doc_groups: 2', 1, '2019-05-30 10:22:09'),
(1252, 'system_menu', 'Insert system_menu: 65', 1, '2019-05-30 10:43:40'),
(1253, 'system_menu', 'Insert system_menu: 66', 1, '2019-05-30 10:44:56'),
(1254, 'system_menu', 'Insert system_menu: 67', 1, '2019-05-30 10:59:14'),
(1255, 'bieu_mau', 'Update bieu_mau: 0', 1, '2019-05-30 02:58:56'),
(1256, 'bieu_mau', 'Update bieu_mau: 0', 1, '2019-05-30 02:59:11'),
(1257, 'bieu_mau', 'Insert bieu_mau: 3', 1, '2019-05-30 03:06:24'),
(1258, 'bieu_mau', 'Insert bieu_mau: 4', 1, '2019-05-30 03:09:29'),
(1259, 'users', 'Insert users: 0', 1, '2019-05-30 03:22:00'),
(1260, 'users', 'Insert users: 0', 1, '2019-05-30 03:35:33'),
(1261, 'doc', 'Update doc: 5', 59, '2019-05-30 04:11:43'),
(1262, 'doc', 'Update doc: 5', 59, '2019-05-30 04:11:45'),
(1263, 'bieu_mau', 'Insert bieu_mau: 5', 1, '2019-05-30 04:40:56'),
(1264, 'chi_tieu', 'Update chi_tieu: 00105', 1, '2019-05-30 10:45:09'),
(1265, 'system_menu', 'Update system_menu: 37', 1, '2019-05-30 10:59:03'),
(1266, 'bao_cao', 'Insert bao_cao: 2', 1, '2019-05-31 12:56:02'),
(1267, 'bieu_mau', 'Insert bieu_mau: 6', 1, '2019-05-31 09:13:16'),
(1268, 'bieu_mau', 'Update bieu_mau: 6', 1, '2019-05-31 09:13:41'),
(1269, 'bieu_mau', 'Update bieu_mau: 6', 1, '2019-05-31 09:14:15'),
(1270, 'system_menu', 'Insert system_menu: 68', 1, '2019-05-31 05:19:01'),
(1271, 'system_menu', 'Delete system_menu: 68', 1, '2019-05-31 05:20:07'),
(1272, 'system_menu', 'Insert system_menu: 69', 1, '2019-05-31 05:20:30'),
(1273, 'system_menu', 'Insert system_menu: 70', 1, '2019-05-31 05:21:21'),
(1274, 'system_menu', 'Insert system_menu: 71', 1, '2019-05-31 05:22:07'),
(1275, 'system_menu', 'Delete system_menu: 69', 1, '2019-05-31 05:22:30'),
(1276, 'system_menu', 'Delete system_menu: 69', 1, '2019-05-31 05:22:30'),
(1277, 'system_menu', 'Delete system_menu: 69', 1, '2019-05-31 05:22:30'),
(1278, 'bao_cao', 'Insert bao_cao: 3', 1, '2019-05-31 05:26:05'),
(1279, 'bieu_mau', 'Update bieu_mau: 5', 1, '2019-05-31 05:26:50'),
(1280, 'bao_cao', 'Update bao_cao: 3', 1, '2019-05-31 05:27:08'),
(1281, 'system_menu', 'Delete system_menu: 37', 1, '2019-06-05 02:59:23'),
(1282, 'system_menu', 'Delete system_menu: 2', 1, '2019-06-05 03:01:00'),
(1283, 'system_menu', 'Delete system_menu: 2', 1, '2019-06-05 03:01:00'),
(1284, 'system_menu', 'Delete system_menu: 2', 1, '2019-06-05 03:01:00'),
(1285, 'system_menu', 'Delete system_menu: 2', 1, '2019-06-05 03:01:00'),
(1286, 'system_menu', 'Delete system_menu: 2', 1, '2019-06-05 03:01:00'),
(1287, 'system_menu', 'Delete system_menu: 2', 1, '2019-06-05 03:01:00'),
(1288, 'system_menu', 'Delete system_menu: 17', 1, '2019-06-05 03:01:03'),
(1289, 'system_menu', 'Delete system_menu: 17', 1, '2019-06-05 03:01:03'),
(1290, 'system_menu', 'Delete system_menu: 17', 1, '2019-06-05 03:01:03'),
(1291, 'system_menu', 'Delete system_menu: 17', 1, '2019-06-05 03:01:03'),
(1292, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1293, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1294, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1295, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1296, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1297, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1298, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1299, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1300, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1301, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1302, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1303, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1304, 'system_menu', 'Delete system_menu: 29', 1, '2019-06-05 03:01:05'),
(1305, 'system_menu', 'Delete system_menu: 36', 1, '2019-06-05 03:01:09'),
(1306, 'system_menu', 'Delete system_menu: 36', 1, '2019-06-05 03:01:09'),
(1307, 'system_menu', 'Delete system_menu: 36', 1, '2019-06-05 03:01:09'),
(1308, 'system_menu', 'Delete system_menu: 36', 1, '2019-06-05 03:01:09'),
(1309, 'system_menu', 'Delete system_menu: 36', 1, '2019-06-05 03:01:09'),
(1310, 'system_menu', 'Delete system_menu: 36', 1, '2019-06-05 03:01:09'),
(1311, 'system_menu', 'Delete system_menu: 62', 1, '2019-06-05 03:01:11'),
(1312, 'system_menu', 'Delete system_menu: 62', 1, '2019-06-05 03:01:11'),
(1313, 'system_menu', 'Delete system_menu: 62', 1, '2019-06-05 03:01:11'),
(1314, 'system_menu', 'Delete system_menu: 62', 1, '2019-06-05 03:01:11'),
(1315, 'system_menu', 'Delete system_menu: 65', 1, '2019-06-05 03:01:13'),
(1316, 'system_menu', 'Delete system_menu: 65', 1, '2019-06-05 03:01:13'),
(1317, 'system_menu', 'Delete system_menu: 65', 1, '2019-06-05 03:01:13'),
(1318, 'system_menu', 'Insert system_menu: 68', 1, '2019-06-05 03:02:06'),
(1319, 'system_menu', 'Update system_menu: ', 1, '2019-06-05 03:02:21'),
(1320, 'system_menu', 'Update system_menu: ', 1, '2019-06-05 03:02:25'),
(1321, 'system_menu', 'Insert system_menu: 69', 1, '2019-06-05 03:03:52'),
(1322, 'system_menu', 'Insert system_menu: 70', 1, '2019-06-05 03:04:40'),
(1323, 'system_menu', 'Insert system_menu: 71', 1, '2019-06-05 03:05:23'),
(1324, 'system_menu', 'Insert system_menu: 72', 1, '2019-06-05 03:06:00'),
(1325, 'category', 'Insert category: 108', 1, '2019-06-05 09:04:18'),
(1326, 'post', 'Insert post: 22', 1, '2019-06-05 09:05:36'),
(1327, 'exchange_currency', 'Insert exchange_currency: 21', 1, '2019-06-05 10:07:54'),
(1328, 'exchange_currency', 'Delete exchange_currency: 21', 1, '2019-06-05 10:10:15'),
(1329, 'exchange_currency', 'Insert exchange_currency: 22', 1, '2019-06-05 10:10:52'),
(1330, 'exchange_currency', 'Update exchange_currency: ', 1, '2019-06-05 10:23:48'),
(1331, 'exchange_currency', 'Update exchange_currency: 22', 1, '2019-06-05 10:25:15'),
(1332, 'exchange_currency', 'Update exchange_currency: 22', 1, '2019-06-05 10:25:21'),
(1333, 'exchange_currency', 'Update exchange_currency: 22', 1, '2019-06-05 10:25:36'),
(1334, 'exchange_currency', 'Insert exchange_currency: 23', 1, '2019-06-05 10:31:21'),
(1335, 'exchange_currency', 'Insert exchange_currency: 24', 1, '2019-06-05 10:35:57'),
(1336, 'exchange_currency', 'Insert exchange_currency: 1', 1, '2019-06-05 10:41:53'),
(1337, 'exchange_currency', 'Insert exchange_currency: 25', 1, '2019-06-05 10:41:53'),
(1338, 'exchange_currency', 'Delete exchange_currency: 25', 1, '2019-06-05 10:42:34'),
(1339, 'exchange_currency', 'Insert exchange_currency: 1', 1, '2019-06-05 10:42:48'),
(1340, 'exchange_currency', 'Delete exchange_currency: 24', 1, '2019-06-05 10:42:57'),
(1341, 'exchange_currency', 'Update exchange_currency: 20', 1, '2019-06-05 10:43:41'),
(1342, 'exchange_currency', 'Update exchange_currency: 20', 1, '2019-06-05 10:43:48'),
(1343, 'exchange_currency', 'Delete exchange_currency: 20', 1, '2019-06-05 10:51:39'),
(1344, 'exchange_currency', 'Delete exchange_currency: 21', 1, '2019-06-06 08:43:11'),
(1345, 'exchange_currency', 'Delete exchange_currency: 19', 1, '2019-06-06 08:43:11'),
(1346, 'exchange_currency', 'Delete exchange_currency: 18', 1, '2019-06-06 08:43:12'),
(1347, 'exchange_currency', 'Delete exchange_currency: 17', 1, '2019-06-06 08:43:12'),
(1348, 'exchange_currency', 'Delete exchange_currency: 16', 1, '2019-06-06 08:43:12'),
(1349, 'exchange_currency', 'Delete exchange_currency: 15', 1, '2019-06-06 08:43:12'),
(1350, 'exchange_currency', 'Delete exchange_currency: 14', 1, '2019-06-06 08:43:12'),
(1351, 'exchange_currency', 'Delete exchange_currency: 13', 1, '2019-06-06 08:43:12'),
(1352, 'exchange_currency', 'Delete exchange_currency: 12', 1, '2019-06-06 08:43:12'),
(1353, 'exchange_currency', 'Delete exchange_currency: 11', 1, '2019-06-06 08:43:12'),
(1354, 'exchange_currency', 'Delete exchange_currency: 10', 1, '2019-06-06 08:43:20'),
(1355, 'exchange_currency', 'Delete exchange_currency: 9', 1, '2019-06-06 08:43:20'),
(1356, 'exchange_currency', 'Delete exchange_currency: 8', 1, '2019-06-06 08:43:20'),
(1357, 'exchange_currency', 'Delete exchange_currency: 7', 1, '2019-06-06 08:43:20'),
(1358, 'exchange_currency', 'Delete exchange_currency: 6', 1, '2019-06-06 08:43:20'),
(1359, 'exchange_currency', 'Delete exchange_currency: 5', 1, '2019-06-06 08:43:21'),
(1360, 'exchange_currency', 'Delete exchange_currency: 4', 1, '2019-06-06 08:43:21'),
(1361, 'exchange_currency', 'Delete exchange_currency: 3', 1, '2019-06-06 08:43:21'),
(1362, 'exchange_currency', 'Delete exchange_currency: 2', 1, '2019-06-06 08:43:21'),
(1363, 'exchange_currency', 'Delete exchange_currency: 1', 1, '2019-06-06 08:43:21'),
(1364, 'exchange_currency', 'Delete exchange_currency: 41', 1, '2019-06-06 08:44:49'),
(1365, 'exchange_currency', 'Delete exchange_currency: 40', 1, '2019-06-06 08:44:49'),
(1366, 'exchange_currency', 'Delete exchange_currency: 39', 1, '2019-06-06 08:44:49'),
(1367, 'exchange_currency', 'Delete exchange_currency: 38', 1, '2019-06-06 08:44:50'),
(1368, 'exchange_currency', 'Delete exchange_currency: 37', 1, '2019-06-06 08:44:50'),
(1369, 'exchange_currency', 'Delete exchange_currency: 36', 1, '2019-06-06 08:44:50'),
(1370, 'exchange_currency', 'Delete exchange_currency: 35', 1, '2019-06-06 08:44:50'),
(1371, 'exchange_currency', 'Delete exchange_currency: 34', 1, '2019-06-06 08:44:50'),
(1372, 'exchange_currency', 'Delete exchange_currency: 33', 1, '2019-06-06 08:44:50'),
(1373, 'exchange_currency', 'Delete exchange_currency: 32', 1, '2019-06-06 08:44:50'),
(1374, 'exchange_currency', 'Delete exchange_currency: 31', 1, '2019-06-06 08:44:53'),
(1375, 'exchange_currency', 'Delete exchange_currency: 30', 1, '2019-06-06 08:44:53'),
(1376, 'exchange_currency', 'Delete exchange_currency: 29', 1, '2019-06-06 08:44:54'),
(1377, 'exchange_currency', 'Delete exchange_currency: 28', 1, '2019-06-06 08:44:54'),
(1378, 'exchange_currency', 'Delete exchange_currency: 27', 1, '2019-06-06 08:44:54'),
(1379, 'exchange_currency', 'Delete exchange_currency: 26', 1, '2019-06-06 08:44:54'),
(1380, 'exchange_currency', 'Delete exchange_currency: 25', 1, '2019-06-06 08:44:54'),
(1381, 'exchange_currency', 'Delete exchange_currency: 24', 1, '2019-06-06 08:44:54'),
(1382, 'exchange_currency', 'Delete exchange_currency: 23', 1, '2019-06-06 08:44:54'),
(1383, 'exchange_currency', 'Delete exchange_currency: 22', 1, '2019-06-06 08:44:54'),
(1384, 'exchange_currency', 'Delete exchange_currency: 18', 1, '2019-06-06 09:14:52'),
(1385, 'exchange_currency', 'Delete exchange_currency: 17', 1, '2019-06-06 09:14:52'),
(1386, 'exchange_currency', 'Delete exchange_currency: 16', 1, '2019-06-06 09:14:52'),
(1387, 'exchange_currency', 'Delete exchange_currency: 15', 1, '2019-06-06 09:14:53'),
(1388, 'exchange_currency', 'Delete exchange_currency: 14', 1, '2019-06-06 09:14:53'),
(1389, 'exchange_currency', 'Delete exchange_currency: 13', 1, '2019-06-06 09:14:53'),
(1390, 'exchange_currency', 'Delete exchange_currency: 12', 1, '2019-06-06 09:14:53'),
(1391, 'exchange_currency', 'Delete exchange_currency: 11', 1, '2019-06-06 09:14:53'),
(1392, 'exchange_currency', 'Delete exchange_currency: 10', 1, '2019-06-06 09:14:53'),
(1393, 'exchange_currency', 'Delete exchange_currency: 9', 1, '2019-06-06 09:14:53'),
(1394, 'property', 'Insert property: 0', 1, '2019-06-06 04:26:19'),
(1395, 'property', 'Update property: 104', 1, '2019-06-06 04:44:24'),
(1396, 'property', 'Insert property: 0', 1, '2019-06-06 04:45:55'),
(1397, 'property', 'Update property: 105', 1, '2019-06-06 04:48:13'),
(1398, 'property', 'Insert property: 0', 1, '2019-06-06 04:49:56'),
(1399, 'property', 'Insert property: 0', 1, '2019-06-06 04:54:59'),
(1400, 'property', 'Update property: 104', 1, '2019-06-06 04:57:23'),
(1401, 'property', 'Insert property: 0', 1, '2019-06-06 05:03:36'),
(1402, 'property', 'Update property: 107', 1, '2019-06-06 05:08:41'),
(1403, 'property', 'Insert property: 0', 1, '2019-06-06 05:17:38'),
(1404, 'category', 'Insert category: 109', 1, '2019-06-06 05:19:48'),
(1405, 'property', 'Insert property: 0', 1, '2019-06-06 05:44:57'),
(1406, 'property', 'Update property: 110', 1, '2019-06-06 05:45:05'),
(1407, 'property', 'Insert property: 0', 1, '2019-06-06 05:52:03'),
(1408, 'product', 'Insert product: 2', 1, '2019-06-07 09:37:11'),
(1409, 'product', 'Insert product: 3', 1, '2019-06-07 09:59:22'),
(1410, 'product', 'Insert product: 4', 1, '2019-06-07 10:06:25'),
(1411, 'product', 'Update product: 4', 1, '2019-06-07 10:16:28'),
(1412, 'product', 'Update product: 2', 1, '2019-06-07 10:16:42'),
(1413, 'product', 'Update product: 3', 1, '2019-06-07 10:17:48'),
(1414, 'category', 'Insert category: 110', 1, '2019-06-07 01:49:51'),
(1415, 'category', 'Insert category: 111', 1, '2019-06-07 01:50:21'),
(1416, 'category', 'Update category: 111', 1, '2019-06-07 01:51:01'),
(1417, 'category', 'Update category: 111', 1, '2019-06-07 02:02:41'),
(1418, 'category', 'Insert category: 112', 1, '2019-06-07 02:23:29'),
(1419, 'video', 'Update video: 7', 1, '2019-06-13 02:57:44'),
(1420, 'video', 'Update video: 4', 1, '2019-06-13 02:57:49'),
(1421, 'video', 'delete video: 4', 1, '2019-06-13 02:57:53'),
(1422, 'video', 'Update video: 6', 1, '2019-06-13 03:04:11'),
(1423, 'property', 'Insert property: 0', 1, '2019-06-13 04:37:31'),
(1424, 'video', 'Update video: 6', 1, '2019-06-13 04:51:59'),
(1425, 'video', 'Insert video: 8', 1, '2019-06-13 04:53:53'),
(1426, 'property', 'Update property: 112', 1, '2019-06-13 05:12:01'),
(1427, 'post', 'Thêm Banner có id là 8', 1, '2019-06-13 05:14:20'),
(1428, 'property', 'Insert property: 0', 1, '2019-06-15 09:12:07'),
(1429, 'product', 'Update product: 4', 1, '2019-06-15 09:12:49'),
(1430, 'product', 'Update product: 4', 1, '2019-06-15 09:16:56'),
(1431, 'product', 'Update product: 4', 1, '2019-06-15 09:18:20'),
(1432, 'agency', 'Update agency: 7', 1, '2019-06-15 04:35:21'),
(1433, 'agency', 'Update agency: 1', 1, '2019-06-15 04:35:33'),
(1434, 'agency', 'Update agency: 1', 1, '2019-06-15 04:36:23'),
(1435, 'faqs', 'Update faqs8', 1, '2019-06-20 02:56:55'),
(1436, 'page', 'Insert page: 0', 1, '2019-06-22 11:12:26'),
(1437, 'page', 'Update page: 13', 1, '2019-06-22 11:13:19'),
(1438, 'faqs', 'Update faqs8', 1, '2019-06-22 11:14:05'),
(1439, 'category', 'Update category: 112', 1, '2019-06-22 11:15:45'),
(1440, 'category', 'Update category: 110', 1, '2019-06-22 11:15:45'),
(1441, 'category', 'Update category: 111', 1, '2019-06-22 11:15:45'),
(1442, 'category', 'Insert category: 113', 1, '2019-06-22 11:16:10'),
(1443, 'category', 'Insert category: 114', 1, '2019-06-22 11:16:43'),
(1444, 'suggest_product', 'Update suggest_product: 4', 1, '2019-06-25 03:29:25'),
(1445, 'suggest_product', 'Update suggest_product: 2', 1, '2019-06-25 03:29:51'),
(1446, 'suggest_product', 'Insert suggest_product: 5', 1, '2019-06-25 03:30:35'),
(1447, 'newsletter', 'Update newsletter: 1', 1, '2019-06-25 04:02:21'),
(1448, 'account', 'Update account: 22', 1, '2019-06-25 05:31:38'),
(1449, 'account', 'Update account: 22', 1, '2019-06-25 05:34:14'),
(1450, 'account', 'Update account: 22', 1, '2019-06-25 05:51:49'),
(1451, 'account', 'Update account: 22', 1, '2019-06-25 05:52:00'),
(1452, 'property', 'Insert property: 0', 1, '2019-06-26 10:22:42'),
(1453, 'property', 'Insert property: 0', 1, '2019-06-26 10:23:40'),
(1454, 'property', 'Update property: 114', 1, '2019-06-26 10:47:10'),
(1455, 'property', 'Update property: 114', 1, '2019-06-26 10:49:48'),
(1456, 'category', 'Update category: 109', 1, '2019-06-26 11:01:46'),
(1457, 'category', 'Insert category: 1', 1, '2019-06-26 02:21:00'),
(1458, 'category', 'Insert category: 2', 1, '2019-06-26 02:22:49'),
(1459, 'category', 'Insert category: 3', 1, '2019-06-26 02:24:11'),
(1460, 'category', 'Insert category: 4', 1, '2019-06-26 02:25:19'),
(1461, 'page', 'Insert page: 0', 1, '2019-06-26 02:27:06'),
(1462, 'page', 'Insert page: 0', 1, '2019-06-26 02:27:39'),
(1463, 'page', 'Update page: 1', 1, '2019-06-26 02:28:38'),
(1464, 'category', 'Insert category: 5', 1, '2019-06-26 02:29:26'),
(1465, 'faqs', 'Insert faqs1', 1, '2019-06-26 02:31:25'),
(1466, 'faqs', 'Insert faqs2', 1, '2019-06-26 02:32:12'),
(1467, 'category', 'Insert category: 6', 1, '2019-06-26 02:32:38'),
(1468, 'category', 'Update category: 2', 1, '2019-06-26 02:41:38'),
(1469, 'property', 'Insert property: 0', 1, '2019-06-26 02:42:27'),
(1470, 'faqs', 'Insert faqs3', 1, '2019-06-26 02:42:48'),
(1471, 'post', 'Insert post: 1', 1, '2019-06-26 02:44:48'),
(1472, 'post', 'Update post: 1', 1, '2019-06-26 02:47:13'),
(1473, 'category', 'Update category: 1', 1, '2019-06-26 02:48:43'),
(1474, 'category', 'Update category: 1', 1, '2019-06-26 02:53:20'),
(1475, 'property', 'Insert property: 0', 1, '2019-06-26 02:55:48'),
(1476, 'post', 'Insert post: 2', 1, '2019-06-26 02:55:49'),
(1477, 'post', 'Update post: 2', 1, '2019-06-26 03:06:24'),
(1478, 'property', 'Update property: 1', 1, '2019-06-26 03:08:43'),
(1479, 'category', 'Update category: 6', 1, '2019-06-26 03:08:58'),
(1480, 'category', 'Update category: 5', 1, '2019-06-26 03:08:58'),
(1481, 'category', 'Insert category: 7', 1, '2019-06-26 03:11:04'),
(1482, 'category', 'Insert category: 8', 1, '2019-06-26 03:11:37'),
(1483, 'category', 'Insert category: 9', 1, '2019-06-26 03:12:08'),
(1484, 'category', 'Insert category: 10', 1, '2019-06-26 03:12:33'),
(1485, 'category', 'Insert category: 11', 1, '2019-06-26 03:13:05'),
(1486, 'category', 'Insert category: 12', 1, '2019-06-26 03:13:32'),
(1487, 'category', 'Insert category: 13', 1, '2019-06-26 03:13:59'),
(1488, 'category', 'Insert category: 14', 1, '2019-06-26 03:15:16'),
(1489, 'category', 'Insert category: 15', 1, '2019-06-26 03:15:55'),
(1490, 'category', 'Update category: 15', 1, '2019-06-26 03:16:17'),
(1491, 'category', 'Insert category: 16', 1, '2019-06-26 03:16:48'),
(1492, 'category', 'Insert category: 17', 1, '2019-06-26 03:17:20'),
(1493, 'category', 'Insert category: 18', 1, '2019-06-26 03:17:37'),
(1494, 'category', 'Insert category: 19', 1, '2019-06-26 03:17:50'),
(1495, 'category', 'Update category: 19', 1, '2019-06-26 03:17:59'),
(1496, 'category', 'Insert category: 20', 1, '2019-06-26 03:18:39'),
(1497, 'category', 'Update category: 17', 1, '2019-06-26 03:18:57'),
(1498, 'post', 'Insert post: 3', 1, '2019-06-26 03:19:28'),
(1499, 'post', 'Insert post: 4', 1, '2019-06-26 03:21:39'),
(1500, 'product', 'Insert product: 4', 1, '2019-06-26 03:22:40'),
(1501, 'post', 'Insert post: 5', 1, '2019-06-26 03:23:00'),
(1502, 'product', 'Insert product: 5', 1, '2019-06-26 03:24:05'),
(1503, 'post', 'Insert post: 6', 1, '2019-06-26 03:24:21'),
(1504, 'page', 'Insert page: 0', 1, '2019-06-26 03:26:28'),
(1505, 'page', 'Insert page: 0', 1, '2019-06-26 03:27:29'),
(1506, 'page', 'Update page: 3', 1, '2019-06-26 03:31:14'),
(1507, 'property', 'Insert property: 0', 1, '2019-06-26 04:10:56'),
(1508, 'post', 'Thêm Banner có id là 1', 1, '2019-06-26 04:11:18'),
(1509, 'post', 'Thêm Banner có id là 2', 1, '2019-06-26 04:11:33'),
(1510, 'agency', 'Insert agency: 1', 1, '2019-06-26 06:06:23'),
(1511, 'property', 'Insert property: 0', 1, '2019-06-26 06:14:15'),
(1512, 'category', 'Update category: 13', 1, '2019-06-27 09:10:55'),
(1513, 'category', 'Update category: 13', 1, '2019-06-27 09:13:22'),
(1514, 'product', 'Update product: 5', 1, '2019-06-27 09:15:56'),
(1515, 'suggest_product', 'Insert suggest_product: 1', 1, '2019-06-27 09:17:33'),
(1516, 'product', 'Update product: 5', 1, '2019-06-27 10:18:23'),
(1517, 'suggest_product', 'Update suggest_product: 1', 1, '2019-06-27 10:18:57'),
(1518, 'suggest_product', 'Insert suggest_product: 2', 1, '2019-06-27 10:43:14'),
(1519, 'page', 'Insert page: 0', 1, '2019-06-27 11:37:30'),
(1520, 'page', 'Update page: 5', 1, '2019-06-27 11:46:23'),
(1521, 'product', 'Insert product: 6', 1, '2019-06-27 01:29:40'),
(1522, 'suggest_product', 'Update suggest_product: 2', 1, '2019-06-27 01:31:10'),
(1523, 'product', 'Insert product: 7', 1, '2019-06-27 01:32:47'),
(1524, 'suggest_product', 'Update suggest_product: 2', 1, '2019-06-27 01:33:07'),
(1525, 'suggest_product', 'Update suggest_product: 1', 1, '2019-06-27 01:35:12'),
(1526, 'category', 'Update category: 7', 1, '2019-06-27 04:46:21'),
(1527, 'category', 'Update category: 8', 1, '2019-06-27 04:46:34'),
(1528, 'category', 'Update category: 9', 1, '2019-06-27 04:46:50'),
(1529, 'category', 'Update category: 10', 1, '2019-06-27 04:47:03'),
(1530, 'category', 'Update category: 10', 1, '2019-06-27 04:47:19'),
(1531, 'category', 'Update category: 12', 1, '2019-06-27 04:47:36'),
(1532, 'category', 'Update category: 13', 1, '2019-06-27 04:47:48'),
(1533, 'category', 'Update category: 7', 1, '2019-06-27 04:49:52'),
(1534, 'category', 'Update category: 11', 1, '2019-06-27 04:53:04'),
(1535, 'system_menu', 'Delete system_menu: 18', 1, '2019-06-27 05:02:57'),
(1536, 'system_menu', 'Delete system_menu: 18', 1, '2019-06-27 05:02:57'),
(1537, 'suggest_product', 'Update suggest_product: 2', 1, '2019-06-27 05:52:56'),
(1538, 'suggest_product', 'Update suggest_product: 2', 1, '2019-06-27 05:53:51'),
(1539, 'property', 'Insert property: 0', 1, '2019-07-02 08:46:58'),
(1540, 'post', 'Thêm Banner có id là 32', 1, '2019-07-02 08:47:40'),
(1541, 'property', 'Insert property: 0', 1, '2019-07-02 02:46:10'),
(1542, 'property', 'Update property: 6', 1, '2019-07-02 02:48:36'),
(1543, 'property', 'Insert property: 0', 1, '2019-07-02 02:50:00'),
(1544, 'property', 'Update property: 7', 1, '2019-07-02 02:50:10'),
(1545, 'property', 'Insert property: 0', 1, '2019-07-02 02:56:47'),
(1546, 'property', 'Insert property: 0', 1, '2019-07-02 02:58:08'),
(1547, 'property', 'Insert property: 0', 1, '2019-07-02 02:59:07'),
(1548, 'property', 'Insert property: 0', 1, '2019-07-02 03:00:06'),
(1549, 'property', 'Update property: 4', 1, '2019-07-02 03:00:50');
INSERT INTO `ap_log_action` (`id`, `action`, `note`, `uid`, `created_time`) VALUES
(1550, 'property', 'Update property: 6', 1, '2019-07-02 03:00:56'),
(1551, 'property', 'Update property: 7', 1, '2019-07-02 03:01:01'),
(1552, 'property', 'Insert property: 0', 1, '2019-07-02 03:10:18'),
(1553, 'property', 'Insert property: 0', 1, '2019-07-02 03:10:58'),
(1554, 'property', 'Insert property: 0', 1, '2019-07-02 03:11:32'),
(1555, 'faqs', 'Insert faqs4', 1, '2019-07-02 03:26:47'),
(1556, 'property', 'Update property: 4', 1, '2019-07-02 03:27:53'),
(1557, 'faqs', 'Update faqs1', 1, '2019-07-02 03:28:07'),
(1558, 'property', 'Insert property: 0', 1, '2019-07-02 03:28:16'),
(1559, 'property', 'Update property: 15', 1, '2019-07-02 03:28:24'),
(1560, 'faqs', 'Update faqs2', 1, '2019-07-02 03:28:52'),
(1561, 'faqs', 'Update faqs3', 1, '2019-07-02 03:30:34'),
(1562, 'faqs', 'Insert faqs5', 1, '2019-07-02 03:32:03'),
(1563, 'faqs', 'Insert faqs6', 1, '2019-07-02 03:34:50'),
(1564, 'faqs', 'Insert faqs7', 1, '2019-07-02 03:36:18'),
(1565, 'faqs', 'Insert faqs8', 1, '2019-07-02 03:37:02'),
(1566, 'faqs', 'Insert faqs9', 1, '2019-07-02 03:37:41'),
(1567, 'post', 'Update post: 5', 1, '2019-07-02 04:29:59'),
(1568, 'suggest_product', 'Update suggest_product: 2', 1, '2019-07-02 04:31:41'),
(1569, 'suggest_product', 'Update suggest_product: 2', 1, '2019-07-02 04:32:20'),
(1570, 'suggest_product', 'Update suggest_product: 2', 1, '2019-07-02 04:32:34'),
(1571, 'suggest_product', 'Update suggest_product: 2', 1, '2019-07-02 04:33:20'),
(1572, 'suggest_product', 'Update suggest_product: 2', 1, '2019-07-02 04:34:35'),
(1573, 'account', 'Update account: 24', 1, '2019-07-02 04:37:59'),
(1574, 'agency', 'Insert agency: 2', 1, '2019-07-02 05:27:58'),
(1575, 'faqs', 'Update faqs1', 1, '2019-07-02 05:52:25'),
(1576, 'banner', 'Sửa Banner có id là 32', 1, '2019-07-02 05:58:00'),
(1577, 'category', 'Update category: 13', 1, '2019-07-03 10:45:56'),
(1578, 'category', 'Update category: 12', 1, '2019-07-03 10:46:34'),
(1579, 'category', 'Update category: 11', 1, '2019-07-03 10:47:06'),
(1580, 'category', 'Update category: 10', 1, '2019-07-03 10:47:54'),
(1581, 'category', 'Update category: 10', 1, '2019-07-03 10:48:58'),
(1582, 'category', 'Update category: 9', 1, '2019-07-03 10:50:49'),
(1583, 'category', 'Update category: 9', 1, '2019-07-03 10:52:18'),
(1584, 'category', 'Update category: 7', 1, '2019-07-03 10:55:04'),
(1585, 'category', 'Update category: 18', 1, '2019-07-03 10:56:20'),
(1586, 'category', 'Update category: 8', 1, '2019-07-03 10:56:54'),
(1587, 'category', 'Update category: 16', 1, '2019-07-03 10:57:37'),
(1588, 'category', 'Update category: 15', 1, '2019-07-03 10:58:14'),
(1589, 'category', 'Update category: 14', 1, '2019-07-03 10:58:45'),
(1590, 'category', 'Update category: 20', 1, '2019-07-03 10:59:18'),
(1591, 'category', 'Update category: 19', 1, '2019-07-03 10:59:48'),
(1592, 'product', 'Insert product: 8', 1, '2019-07-03 11:04:13'),
(1593, 'category', 'Update category: 11', 1, '2019-07-03 11:08:15'),
(1594, 'category', 'Update category: 13', 1, '2019-07-03 11:08:25'),
(1595, 'category', 'Update category: 13', 1, '2019-07-03 11:08:43'),
(1596, 'category', 'Update category: 11', 1, '2019-07-03 11:09:28'),
(1597, 'product', 'Insert product: 9', 1, '2019-07-03 11:22:28'),
(1598, 'product', 'Insert product: 10', 1, '2019-07-03 11:24:14'),
(1599, 'product', 'Insert product: 11', 1, '2019-07-03 11:27:19'),
(1600, 'property', 'Update property: 1', 1, '2019-07-03 01:54:42'),
(1601, 'property', 'Insert property: 0', 1, '2019-07-03 01:55:01'),
(1602, 'post', 'Update post: 3', 1, '2019-07-03 02:57:32'),
(1603, 'suggest_product', 'Update suggest_product: 1', 1, '2019-07-03 03:22:28'),
(1604, 'product', 'Update product: 5', 1, '2019-07-03 03:23:04'),
(1605, 'suggest_product', 'Update suggest_product: 2', 1, '2019-07-03 03:24:40'),
(1606, 'product', 'Insert product: 12', 1, '2019-07-03 03:49:10'),
(1607, 'product', 'Insert product: 13', 1, '2019-07-03 03:51:11'),
(1608, 'product', 'Update product: 9', 1, '2019-07-03 03:51:45'),
(1609, 'product', 'Update product: 9', 1, '2019-07-03 03:52:37'),
(1610, 'post', 'Update post: 6', 1, '2019-07-04 10:57:37'),
(1611, 'property', 'Update property: 2', 1, '2019-07-04 04:21:50'),
(1612, 'property', 'Update property: 2', 1, '2019-07-04 04:22:51'),
(1613, 'banner', 'Sửa Banner có id là 32', 22, '2019-07-08 01:52:29'),
(1614, 'banner', 'Sửa Banner có id là 10', 22, '2019-07-08 02:03:38'),
(1615, 'page', 'Update page: 3', 22, '2019-07-08 03:14:37'),
(1616, 'category', 'Update category: 10', 1, '2019-07-09 03:59:27'),
(1617, 'category', 'Insert category: 21', 1, '2019-07-09 04:01:49'),
(1618, 'category', 'Update category: 21', 1, '2019-07-09 04:08:19'),
(1619, 'category', 'Update category: 11', 1, '2019-07-09 04:08:43'),
(1620, 'category', 'Update category: 12', 1, '2019-07-09 04:08:49'),
(1621, 'category', 'Update category: 11', 1, '2019-07-09 04:10:00'),
(1622, 'category', 'Update category: 8', 1, '2019-07-09 04:10:13'),
(1623, 'category', 'Update category: 12', 1, '2019-07-09 04:10:20'),
(1624, 'suggest_product', 'Update suggest_product: 2', 1, '2019-07-09 04:10:26'),
(1625, 'suggest_product', 'Update suggest_product: 2', 1, '2019-07-09 04:10:37'),
(1626, 'category', 'Update category: 21', 1, '2019-07-09 04:11:20'),
(1627, 'category', 'Update category: 13', 1, '2019-07-09 04:11:40'),
(1628, 'category', 'Update category: 10', 1, '2019-07-09 04:11:53'),
(1629, 'account', 'delete account: 26', 1, '2019-07-09 04:36:44'),
(1630, 'account', 'delete account: 25', 1, '2019-07-09 04:36:49'),
(1631, 'account', 'Update account: 24', 1, '2019-07-09 04:37:33'),
(1632, 'agency', 'Insert agency: 3', 1, '2019-07-09 05:46:54'),
(1633, 'agency', 'Update agency: 3', 1, '2019-07-09 05:50:01'),
(1634, 'agency', 'Insert agency: 4', 1, '2019-07-09 05:54:26'),
(1635, 'category', 'Insert category: 22', 1, '2019-07-09 07:11:21'),
(1636, 'category', 'Update category: 22', 1, '2019-07-09 07:11:48'),
(1637, 'post', 'Insert post: 7', 1, '2019-07-09 07:15:15'),
(1638, 'post', 'Update post: 2', 1, '2019-07-09 07:24:05'),
(1639, 'post', 'Update post: 4', 1, '2019-07-09 07:39:40'),
(1640, 'category', 'Insert category: 23', 1, '2019-07-09 07:43:41'),
(1641, 'post', 'Insert post: 8', 1, '2019-07-09 07:46:03'),
(1642, 'category', 'Update category: 13', 1, '2019-07-09 07:53:00'),
(1643, 'post', 'Update post: 7', 1, '2019-07-10 08:48:56'),
(1644, 'post', 'Update post: 7', 1, '2019-07-10 08:56:20'),
(1645, 'post', 'Update post: 7', 1, '2019-07-10 09:08:01'),
(1646, 'product', 'Insert product: 14', 1, '2019-07-10 09:17:01'),
(1647, 'post', 'Update post: 2', 1, '2019-07-10 09:22:43'),
(1648, 'account', 'Update account: 22', 1, '2019-07-10 09:29:31'),
(1649, 'product', 'Insert product: 15', 1, '2019-07-10 09:36:44'),
(1650, 'product', 'Insert product: 16', 1, '2019-07-10 09:40:52'),
(1651, 'product', 'Insert product: 17', 1, '2019-07-10 09:42:39'),
(1652, 'product', 'Insert product: 18', 1, '2019-07-10 09:45:02'),
(1653, 'product', 'Insert product: 19', 1, '2019-07-10 09:47:10'),
(1654, 'product', 'Insert product: 20', 1, '2019-07-10 09:48:42'),
(1655, 'product', 'Insert product: 21', 1, '2019-07-10 09:50:36'),
(1656, 'product', 'Insert product: 22', 1, '2019-07-10 09:54:17'),
(1657, 'product', 'Insert product: 23', 1, '2019-07-10 09:56:20'),
(1658, 'product', 'Insert product: 24', 1, '2019-07-10 09:58:42'),
(1659, 'product', 'Insert product: 25', 1, '2019-07-10 10:00:41'),
(1660, 'product', 'Insert product: 26', 1, '2019-07-10 10:04:29'),
(1661, 'product', 'Insert product: 27', 1, '2019-07-10 10:06:10'),
(1662, 'product', 'Update product: 27', 1, '2019-07-10 11:54:51'),
(1663, 'post', 'Thêm Banner có id là 33', 22, '2019-07-10 02:23:15'),
(1664, 'post', 'Thêm Banner có id là 34', 22, '2019-07-10 02:28:01'),
(1665, 'post', 'Thêm Banner có id là 35', 22, '2019-07-10 02:29:18'),
(1666, 'banner', 'Sửa Banner có id là 34', 22, '2019-07-10 02:30:28'),
(1667, 'post', 'Thêm Banner có id là 36', 22, '2019-07-10 02:33:30'),
(1668, 'property', 'Insert property: 0', 22, '2019-07-10 03:10:19'),
(1669, 'banner', 'Sửa Banner có id là 26', 1, '2019-07-10 06:01:58'),
(1670, 'banner', 'Sửa Banner có id là 24', 1, '2019-07-10 06:02:08'),
(1671, 'post', 'Thêm Banner có id là 47', 22, '2019-07-10 06:08:34'),
(1672, 'product', 'Update product: 27', 1, '2019-07-11 10:05:10'),
(1673, 'product', 'Update product: 26', 1, '2019-07-11 10:05:19'),
(1674, 'product', 'Update product: 25', 1, '2019-07-11 10:05:23'),
(1675, 'product', 'Update product: 24', 1, '2019-07-11 10:05:29'),
(1676, 'product', 'Update product: 23', 1, '2019-07-11 10:05:36'),
(1677, 'product', 'Update product: 22', 1, '2019-07-11 10:05:41'),
(1678, 'product', 'Update product: 21', 1, '2019-07-11 10:05:45'),
(1679, 'product', 'Update product: 19', 1, '2019-07-11 10:05:50'),
(1680, 'product', 'Update product: 20', 1, '2019-07-11 10:05:55'),
(1681, 'product', 'Update product: 18', 1, '2019-07-11 10:05:59'),
(1682, 'product', 'Update product: 14', 1, '2019-07-11 10:06:18'),
(1683, 'property', 'Update property: 2', 1, '2019-07-11 10:41:53'),
(1684, 'property', 'Insert property: 0', 1, '2019-07-11 11:04:32'),
(1685, 'property', 'Insert property: 0', 1, '2019-07-11 02:45:43'),
(1686, 'post', 'Insert post: 9', 1, '2019-07-11 02:46:40'),
(1687, 'property', 'Insert property: 0', 1, '2019-07-11 02:46:45'),
(1688, 'property', 'Insert property: 0', 1, '2019-07-11 02:49:00'),
(1689, 'category', 'Update category: 23', 1, '2019-07-11 02:51:17'),
(1690, 'post', 'Insert post: 10', 1, '2019-07-11 02:54:54'),
(1691, 'post', 'Insert post: 11', 1, '2019-07-11 02:57:16'),
(1692, 'property', 'Update property: 4', 1, '2019-07-11 03:26:22'),
(1693, 'property', 'Update property: 6', 1, '2019-07-11 03:27:29'),
(1694, 'property', 'Update property: 15', 1, '2019-07-11 03:28:25'),
(1695, 'property', 'Update property: 16', 1, '2019-07-11 03:29:15'),
(1696, 'property', 'Update property: 17', 1, '2019-07-11 03:30:21'),
(1697, 'property', 'Insert property: 0', 1, '2019-07-11 03:31:34'),
(1698, 'property', 'Insert property: 0', 1, '2019-07-11 03:32:41'),
(1699, 'property', 'Insert property: 0', 1, '2019-07-11 03:33:25'),
(1700, 'property', 'Insert property: 0', 1, '2019-07-11 03:34:02'),
(1701, 'property', 'Insert property: 0', 1, '2019-07-11 03:35:44'),
(1702, 'post', 'Update post: 11', 1, '2019-07-11 04:03:28'),
(1703, 'post', 'Update post: 3', 1, '2019-07-11 04:08:41'),
(1704, 'category', 'Update category: 11', 1, '2019-07-11 04:51:13'),
(1705, 'banner', 'Sửa Banner có id là 18', 1, '2019-07-11 04:55:08'),
(1706, 'banner', 'Sửa Banner có id là 17', 1, '2019-07-11 04:55:14'),
(1707, 'banner', 'Sửa Banner có id là 16', 1, '2019-07-11 04:55:19'),
(1708, 'banner', 'Sửa Banner có id là 15', 1, '2019-07-11 04:55:24'),
(1709, 'property', 'Insert property: 0', 1, '2019-07-11 05:01:55'),
(1710, 'property', 'Update property: 23', 1, '2019-07-11 05:01:58'),
(1711, 'users', 'Update users: 59', 1, '2019-07-11 05:20:07'),
(1712, 'suggest_product', 'Update suggest_product: 1', 1, '2019-07-11 05:37:52'),
(1713, 'suggest_product', 'Insert suggest_product: 3', 1, '2019-07-11 05:41:45'),
(1714, 'product', 'Insert product: 28', 1, '2019-07-11 05:59:04'),
(1715, 'post', 'Update post: 3', 1, '2019-07-11 05:59:53'),
(1716, 'product', 'Insert product: 29', 1, '2019-07-11 06:01:02'),
(1717, 'product', 'Insert product: 30', 1, '2019-07-11 06:02:50'),
(1718, 'post', 'Update post: 3', 1, '2019-07-11 06:03:30'),
(1719, 'product', 'Insert product: 31', 1, '2019-07-11 06:04:56'),
(1720, 'product', 'Insert product: 32', 1, '2019-07-11 06:06:48'),
(1721, 'product', 'Insert product: 33', 1, '2019-07-11 06:17:54'),
(1722, 'product', 'Insert product: 34', 1, '2019-07-11 06:19:24'),
(1723, 'product', 'Insert product: 35', 1, '2019-07-11 06:22:27'),
(1724, 'product', 'Insert product: 36', 1, '2019-07-11 06:23:59'),
(1725, 'suggest_product', 'Update suggest_product: 1', 1, '2019-07-11 06:24:45'),
(1726, 'banner', 'Sửa Banner có id là 18', 1, '2019-07-11 06:38:19'),
(1727, 'banner', 'Sửa Banner có id là 15', 1, '2019-07-11 06:38:39'),
(1728, 'suggest_product', 'Update suggest_product: 2', 1, '2019-07-11 06:38:54'),
(1729, 'banner', 'Sửa Banner có id là 16', 1, '2019-07-11 06:38:58'),
(1730, 'banner', 'Sửa Banner có id là 17', 1, '2019-07-11 06:39:08'),
(1731, 'banner', 'Sửa Banner có id là 18', 1, '2019-07-11 06:39:16'),
(1732, 'banner', 'Sửa Banner có id là 15', 1, '2019-07-11 06:39:24'),
(1733, 'banner', 'Sửa Banner có id là 16', 1, '2019-07-11 06:39:31'),
(1734, 'banner', 'Sửa Banner có id là 17', 1, '2019-07-11 06:39:37'),
(1735, 'banner', 'Sửa Banner có id là 18', 1, '2019-07-11 06:39:42'),
(1736, 'suggest_product', 'Insert suggest_product: 4', 1, '2019-07-11 06:42:45'),
(1737, 'product', 'Update product: 13', 1, '2019-07-11 06:43:15'),
(1738, 'banner', 'Sửa Banner có id là 37', 1, '2019-07-11 07:21:39'),
(1739, 'banner', 'Sửa Banner có id là 37', 1, '2019-07-11 07:22:23'),
(1740, 'property', 'Update property: 3', 1, '2019-07-11 07:34:49'),
(1741, 'property', 'Update property: 7', 1, '2019-07-11 07:46:54'),
(1742, 'property', 'Update property: 8', 1, '2019-07-11 07:49:28'),
(1743, 'users', 'Update users: 59', 1, '2019-07-12 07:15:34'),
(1744, 'category', 'Update category: 22', 1, '2019-07-12 07:25:38'),
(1745, 'product', 'Update product: 36', 1, '2019-07-12 09:26:37'),
(1746, 'property', 'Update property: 14', 1, '2019-07-12 11:12:56'),
(1747, 'suggest_product', 'Update suggest_product: 4', 1, '2019-07-12 11:17:55'),
(1748, 'account', 'delete account: 19', 1, '2019-07-12 11:58:42'),
(1749, 'property', 'Update property: 21', 1, '2019-07-12 01:41:55'),
(1750, 'property', 'Update property: 20', 1, '2019-07-12 01:42:04'),
(1751, 'property', 'Update property: 19', 1, '2019-07-12 01:42:13'),
(1752, 'property', 'Update property: 18', 1, '2019-07-12 01:42:22'),
(1753, 'property', 'Update property: 17', 1, '2019-07-12 01:42:31'),
(1754, 'property', 'Update property: 16', 1, '2019-07-12 01:42:42'),
(1755, 'property', 'Update property: 15', 1, '2019-07-12 01:42:52'),
(1756, 'property', 'Update property: 6', 1, '2019-07-12 01:43:03'),
(1757, 'property', 'Update property: 4', 1, '2019-07-12 01:43:12'),
(1758, 'suggest_product', 'Update suggest_product: 3', 1, '2019-07-12 02:01:14'),
(1759, 'page', 'Update page: 5', 1, '2019-07-12 02:08:56'),
(1760, 'system_menu', 'Update system_menu: 27', 1, '2019-07-12 02:13:33'),
(1761, 'suggest_product', 'Update suggest_product: 1', 1, '2019-07-12 02:19:40'),
(1762, 'suggest_product', 'Update suggest_product: 1', 1, '2019-07-12 02:20:07'),
(1763, 'suggest_product', 'Update suggest_product: 1', 1, '2019-07-12 02:20:31'),
(1764, 'suggest_product', 'Update suggest_product: 1', 1, '2019-07-12 02:20:39'),
(1765, 'page', 'Update page: 4', 1, '2019-07-12 02:40:20'),
(1766, 'post', 'Update post: 2', 1, '2019-07-12 03:17:49'),
(1767, 'property', 'Update property: 22', 1, '2019-07-12 03:37:57'),
(1768, 'property', 'Update property: 20', 1, '2019-07-12 03:38:06'),
(1769, 'property', 'Update property: 21', 1, '2019-07-12 03:38:13'),
(1770, 'property', 'Update property: 17', 1, '2019-07-12 04:20:32'),
(1771, 'property', 'Update property: 15', 1, '2019-07-12 04:21:08'),
(1772, 'property', 'Update property: 16', 1, '2019-07-12 04:21:24'),
(1773, 'property', 'Update property: 18', 1, '2019-07-12 04:22:02'),
(1774, 'category', 'Update category: 21', 1, '2019-07-12 06:51:46'),
(1775, 'category', 'Update category: 21', 1, '2019-07-12 07:18:49'),
(1776, 'property', 'Update property: 1', 1, '2019-07-12 07:24:44'),
(1777, 'system_menu', 'Delete system_menu: 22', 1, '2019-07-12 10:35:16'),
(1778, 'account', 'Update account: 17', 1, '2019-07-12 11:39:24'),
(1779, 'account', 'Update account: 17', 1, '2019-07-12 11:39:46'),
(1780, 'account', 'Update account: 18', 1, '2019-07-13 12:35:24'),
(1781, 'property', 'Insert property: 0', 1, '2019-07-13 09:06:07'),
(1782, 'post', 'Thêm Banner có id là 48', 1, '2019-07-13 09:06:46'),
(1783, 'post', 'Thêm Banner có id là 49', 1, '2019-07-13 09:07:01'),
(1784, 'category', 'Update category: 21', 1, '2019-07-13 10:03:49'),
(1785, 'account', 'Update account: 22', 1, '2019-07-13 11:03:05'),
(1786, 'property', 'Insert property: 0', 1, '2019-07-13 11:05:35'),
(1787, 'property', 'Insert property: 0', 1, '2019-07-13 11:07:12'),
(1788, 'property', 'Insert property: 0', 1, '2019-07-13 11:08:45'),
(1789, 'property', 'Insert property: 0', 1, '2019-07-13 11:09:18'),
(1790, 'post', 'Update post: 1', 1, '2019-07-13 02:23:04'),
(1791, 'property', 'Update property: 1', 1, '2019-07-13 02:28:31'),
(1792, 'property', 'Insert property: 0', 1, '2019-07-13 02:29:27'),
(1793, 'product', 'Update product: 27', 1, '2019-07-13 02:43:18'),
(1794, 'property', 'Update property: 1', 1, '2019-07-13 03:58:35'),
(1795, 'agency', 'Insert agency: 5', 1, '2019-07-16 09:38:07'),
(1796, 'agency', 'Update agency: 5', 1, '2019-07-16 09:40:36'),
(1797, 'category', 'Insert category: 24', 1, '2019-07-23 10:46:52'),
(1798, 'category', 'Update category: 24', 1, '2019-07-29 11:29:10'),
(1799, 'category', 'Insert category: 25', 1, '2019-07-29 11:31:58'),
(1800, 'system_menu', 'Delete system_menu: 13', 1, '2019-08-10 02:23:12'),
(1801, 'product', 'Update product: 9', 1, '2019-08-14 03:07:52'),
(1802, 'product', 'Update product: 9', 1, '2019-08-14 03:08:08'),
(1803, 'product', 'Update product: 34', 1, '2019-08-14 03:17:39'),
(1804, 'category', 'Update category: 21', 1, '2019-08-15 10:01:16'),
(1805, 'category', 'Update category: 21', 1, '2019-08-15 10:04:41'),
(1806, 'category', 'Update category: 13', 1, '2019-08-15 10:04:53'),
(1807, 'category', 'Update category: 21', 1, '2019-08-15 10:38:12'),
(1808, 'category', 'Update category: 13', 1, '2019-08-15 10:38:20'),
(1809, 'category', 'Update category: 12', 1, '2019-08-15 10:38:25'),
(1810, 'category', 'Update category: 11', 1, '2019-08-15 10:38:32'),
(1811, 'category', 'Update category: 10', 1, '2019-08-15 10:38:40'),
(1812, 'category', 'Update category: 8', 1, '2019-08-15 10:38:48'),
(1813, 'product', 'Update product: 9', 1, '2019-08-16 08:49:44'),
(1814, 'product', 'Update product: 9', 1, '2019-08-16 08:57:56'),
(1815, 'product', 'Update product: 35', 1, '2019-08-16 09:13:17'),
(1816, 'product', 'Update product: 35', 1, '2019-08-16 09:13:23'),
(1817, 'product', 'Update product: 35', 1, '2019-08-16 09:23:01'),
(1818, 'product', 'Update product: 35', 1, '2019-08-16 09:26:14'),
(1819, 'product', 'Update product: 35', 1, '2019-08-16 09:26:21'),
(1820, 'page', 'Insert page: 0', 1, '2019-08-23 01:57:12'),
(1821, 'page', 'Insert page: 0', 1, '2019-08-23 01:57:39'),
(1822, 'page', 'Insert page: 0', 1, '2019-08-23 01:57:57'),
(1823, 'page', 'Update page: 2', 1, '2019-08-23 03:34:06'),
(1824, 'page', 'Update page: 2', 1, '2019-08-23 03:37:29'),
(1825, 'page', 'Update page: 2', 1, '2019-08-23 03:55:04'),
(1826, 'category', 'Insert category: 1', 1, '2019-08-23 04:05:53'),
(1827, 'post', 'Insert post: 1', 1, '2019-08-23 04:06:33'),
(1828, 'post', 'Insert post: 2', 1, '2019-08-23 04:07:00'),
(1829, 'post', 'Insert post: 3', 1, '2019-08-23 04:07:28'),
(1830, 'post', 'Insert post: 4', 1, '2019-08-23 04:07:54'),
(1831, 'category', 'Insert category: 2', 1, '2019-08-23 04:24:32'),
(1832, 'category', 'Update category: 2', 1, '2019-08-23 04:46:27'),
(1833, 'post', 'Insert post: 5', 1, '2019-08-23 04:58:02'),
(1834, 'post', 'Insert post: 6', 1, '2019-08-23 04:58:36'),
(1835, 'post', 'Update post: 3', 1, '2019-08-23 04:58:42'),
(1836, 'post', 'Update post: 4', 1, '2019-08-23 04:58:46'),
(1837, 'post', 'Update post: 5', 1, '2019-08-23 04:58:53'),
(1838, 'post', 'Update post: 6', 1, '2019-08-23 04:58:57'),
(1839, 'post', 'Update post: 1', 1, '2019-08-23 04:59:07'),
(1840, 'post', 'Update post: 2', 1, '2019-08-23 04:59:10'),
(1841, 'post', 'Update post: 3', 1, '2019-08-23 04:59:13'),
(1842, 'page', 'Insert page: 0', 1, '2019-08-23 07:25:11'),
(1843, 'property', 'Insert property: 0', 1, '2019-08-23 07:32:15'),
(1844, 'post', 'Thêm Banner có id là 1', 1, '2019-08-23 07:32:41'),
(1845, 'post', 'Thêm Banner có id là 2', 1, '2019-08-23 07:32:54'),
(1846, 'system_menu', 'Delete system_menu: 5', 1, '2019-08-23 07:33:51'),
(1847, 'system_menu', 'Delete system_menu: 9', 1, '2019-08-23 07:34:00'),
(1848, 'system_menu', 'Delete system_menu: 31', 1, '2019-08-23 07:34:05'),
(1849, 'system_menu', 'Delete system_menu: 14', 1, '2019-08-23 07:34:12'),
(1850, 'system_menu', 'Delete system_menu: 27', 1, '2019-08-23 07:34:16'),
(1851, 'system_menu', 'Delete system_menu: 28', 1, '2019-08-23 07:34:21'),
(1852, 'system_menu', 'Delete system_menu: 29', 1, '2019-08-23 07:34:24'),
(1853, 'system_menu', 'Delete system_menu: 12', 1, '2019-08-23 07:34:39'),
(1854, 'page', 'Update page: 4', 1, '2019-08-23 07:41:47'),
(1855, 'page', 'Update page: 3', 1, '2019-08-23 07:45:57'),
(1856, 'page', 'Update page: 1', 1, '2019-08-23 07:46:22'),
(1857, 'page', 'Update page: 3', 1, '2019-08-23 07:47:19'),
(1858, 'category', 'Insert category: 3', 1, '2019-08-23 07:51:20'),
(1859, 'post', 'Update post: 6', 1, '2019-08-23 07:51:29'),
(1860, 'post', 'Update post: 5', 1, '2019-08-23 07:51:34'),
(1861, 'post', 'Update post: 4', 1, '2019-08-23 07:51:37'),
(1862, 'post', 'Update post: 3', 1, '2019-08-23 07:51:42'),
(1863, 'post', 'Update post: 2', 1, '2019-08-23 07:51:45'),
(1864, 'post', 'Update post: 1', 1, '2019-08-23 07:51:49'),
(1865, 'category', 'Insert category: 4', 1, '2019-08-23 07:59:16'),
(1866, 'category', 'Insert category: 5', 1, '2019-08-23 07:59:40'),
(1867, 'category', 'Insert category: 6', 1, '2019-08-23 08:00:04'),
(1868, 'category', 'Insert category: 7', 1, '2019-08-23 08:00:23'),
(1869, 'category', 'Insert category: 8', 1, '2019-08-23 08:00:50'),
(1870, 'category', 'Insert category: 9', 1, '2019-08-23 08:01:06'),
(1871, 'category', 'Insert category: 10', 1, '2019-08-23 08:01:55'),
(1872, 'product', 'Insert product: 1', 1, '2019-08-23 08:04:05'),
(1873, 'product', 'Insert product: 2', 1, '2019-08-23 08:06:06'),
(1874, 'product', 'Insert product: 3', 1, '2019-08-23 08:06:40'),
(1875, 'product', 'Insert product: 4', 1, '2019-08-23 08:07:12'),
(1876, 'product', 'Insert product: 5', 1, '2019-08-23 08:07:48'),
(1877, 'product', 'Insert product: 6', 1, '2019-08-23 08:08:20'),
(1878, 'product', 'Insert product: 7', 1, '2019-08-23 08:08:51'),
(1879, 'property', 'Insert property: 0', 1, '2019-08-23 08:38:00'),
(1880, 'post', 'Thêm Banner có id là 3', 1, '2019-08-23 08:38:21'),
(1881, 'category', 'Update category: 10', 1, '2019-08-23 08:42:18'),
(1882, 'category', 'Update category: 5', 1, '2019-08-23 08:42:21'),
(1883, 'category', 'Update category: 5', 1, '2019-08-23 08:42:26'),
(1884, 'category', 'Update category: 9', 1, '2019-08-23 08:42:32'),
(1885, 'category', 'Update category: 8', 1, '2019-08-23 08:42:39'),
(1886, 'category', 'Update category: 4', 1, '2019-08-23 08:42:46'),
(1887, 'category', 'Update category: 7', 1, '2019-08-23 08:42:51'),
(1888, 'category', 'Update category: 6', 1, '2019-08-23 08:42:56'),
(1889, 'page', 'Insert page: 0', 1, '2019-08-23 09:28:03'),
(1890, 'category', 'Update category: 10', 1, '2019-08-23 09:57:33'),
(1891, 'category', 'Update category: 5', 1, '2019-08-23 09:57:47'),
(1892, 'category', 'Update category: 9', 1, '2019-08-23 09:57:51'),
(1893, 'category', 'Update category: 4', 1, '2019-08-23 09:57:56'),
(1894, 'category', 'Update category: 7', 1, '2019-08-23 09:57:59'),
(1895, 'category', 'Update category: 6', 1, '2019-08-23 09:58:05'),
(1896, 'product', 'Update product: 7', 1, '2019-08-23 11:04:53'),
(1897, 'product', 'Update product: 7', 1, '2019-08-23 11:06:00'),
(1898, 'product', 'Update product: 6', 1, '2019-08-23 11:07:19'),
(1899, 'product', 'Update product: 5', 1, '2019-08-23 11:07:24'),
(1900, 'product', 'Update product: 4', 1, '2019-08-23 11:07:29'),
(1901, 'product', 'Update product: 3', 1, '2019-08-23 11:07:34'),
(1902, 'product', 'Update product: 1', 1, '2019-08-23 11:07:40'),
(1903, 'product', 'Update product: 2', 1, '2019-08-23 11:07:44'),
(1904, 'product', 'Update product: 7', 1, '2019-08-26 11:48:06'),
(1905, 'product', 'Insert product: 8', 1, '2019-08-26 07:32:46'),
(1906, 'category', 'Update category: 4', 1, '2019-08-27 05:45:36'),
(1907, 'category', 'Update category: 5', 1, '2019-08-27 05:58:45'),
(1908, 'category', 'Update category: 5', 1, '2019-08-27 06:01:10'),
(1909, 'post', 'Update post: 6', 1, '2019-08-27 06:11:02'),
(1910, 'page', 'Update page: 5', 1, '2019-08-27 06:18:33'),
(1911, 'category', 'Update category: 10', 1, '2019-08-27 06:25:17'),
(1912, 'product', 'Update product: 8', 1, '2019-08-28 08:16:30'),
(1913, 'product', 'Update product: 8', 1, '2019-08-28 08:17:57'),
(1914, 'product', 'Update product: 8', 1, '2019-08-28 08:49:29'),
(1915, 'product', 'Update product: 8', 1, '2019-08-28 10:50:22');

-- --------------------------------------------------------

--
-- Table structure for table `ap_menus`
--

CREATE TABLE `ap_menus` (
  `id` int(4) NOT NULL,
  `title` varchar(75) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#',
  `parent_id` int(2) NOT NULL DEFAULT '0',
  `order` tinyint(2) DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` smallint(4) NOT NULL,
  `language_code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_menus`
--

INSERT INTO `ap_menus` (`id`, `title`, `icon`, `link`, `parent_id`, `order`, `class`, `location_id`, `language_code`, `type`) VALUES
(4, 'Trang chủ', '', '/', 0, 1, '', 1, 'vi', 'other'),
(5, 'Giới thiệu', '', '#', 0, 2, '', 1, 'vi', 'other'),
(6, 'Tổng quan', '', 'tong-quan.html', 5, 1, '', 1, 'vi', NULL),
(7, 'Về chúng tôi', '', 've-chung-toi.html', 5, 2, '', 1, 'vi', NULL),
(8, 'Liên hệ', '', 'lien-he.html', 0, 3, '', 1, 'vi', 'page'),
(19, 'Trang chủ', '', '/', 0, 1, '', 1, 'en', 'other'),
(20, 'Giới thiệu', '', '#', 0, 2, '', 1, 'en', 'other'),
(21, 'Tổng quan', '', 'tong-quan.html', 20, 1, '', 1, 'en', NULL),
(22, 'Về chúng tôi', '', 've-chung-toi.html', 20, 2, '', 1, 'en', NULL),
(23, 'Liên hệ', '', 'lien-he.html', 0, 3, '', 1, 'en', 'page');

-- --------------------------------------------------------

--
-- Table structure for table `ap_newsletter`
--

CREATE TABLE `ap_newsletter` (
  `id` int(11) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fullname` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_newsletter`
--

INSERT INTO `ap_newsletter` (`id`, `created_time`, `fullname`, `phone`) VALUES
(1, '2019-08-23 15:20:41', 'aaaaa', '0968848712'),
(20, '2019-08-23 15:23:55', 'aaaaaaaaa', '0968848712'),
(21, '2019-08-23 15:24:02', 'â', '0971116593'),
(22, '2019-08-25 17:53:08', 'kexno', '0974301136'),
(23, '2019-08-27 15:26:26', 'Kexno Studio', '0974301136'),
(24, '2019-08-28 14:32:06', 'Kexno Studio', '0974301136'),
(25, '2019-08-28 14:32:21', 'kadsdinjksdnji', '0974301136');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order`
--

CREATE TABLE `ap_order` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `is_status` int(11) NOT NULL DEFAULT '1',
  `full_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `ward_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `customer_id` int(11) DEFAULT NULL,
  `addredit` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order`
--

INSERT INTO `ap_order` (`id`, `code`, `is_status`, `full_name`, `address`, `phone`, `email`, `city_id`, `district_id`, `ward_id`, `note`, `created_time`, `updated_time`, `customer_id`, `addredit`) VALUES
(1, '', 1, 'Mai Huynh Nhu', 'aa', '0968848712', '', 0, 0, 8, 'aaaa', '2019-08-28 13:11:15', '2019-08-28 13:11:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_items`
--

CREATE TABLE `ap_order_items` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(15,0) NOT NULL,
  `is_status` int(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_page`
--

CREATE TABLE `ap_page` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `style` varchar(20) DEFAULT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `is_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '2' COMMENT 'trang thai',
  `file` varchar(255) DEFAULT NULL,
  `time_thanhlap` datetime NOT NULL,
  `displayed_time` datetime NOT NULL COMMENT 'ngay publish',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngay tao',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngay sua',
  `album` text,
  `block` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_page`
--

INSERT INTO `ap_page` (`id`, `category_id`, `style`, `thumbnail`, `banner`, `is_status`, `file`, `time_thanhlap`, `displayed_time`, `created_time`, `updated_time`, `album`, `block`) VALUES
(1, 0, 'contact', '01.jpg', '', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-08-23 13:57:12', '2019-08-23 19:46:22', NULL, NULL),
(2, 0, 'overview', '01.jpg', '02.jpg', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-08-23 13:57:39', '2019-08-23 15:34:06', NULL, NULL),
(3, 0, 'about', '01.jpg', '', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-08-23 13:57:57', '2019-08-23 19:47:19', NULL, NULL),
(4, 0, 'overview_product', '', '', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-08-23 19:25:10', '2019-08-23 19:25:10', NULL, NULL),
(5, 0, '', '', '', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-08-23 21:28:03', '2019-08-23 21:28:03', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_page_translations`
--

CREATE TABLE `ap_page_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(5) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `content` longtext,
  `file_timeline` varchar(255) DEFAULT NULL,
  `content_more` longtext,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(170) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_page_translations`
--

INSERT INTO `ap_page_translations` (`id`, `language_code`, `slug`, `title`, `description`, `content`, `file_timeline`, `content_more`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 'vi', 'lien-he', 'Liên hệ', NULL, '<p>Li&ecirc;n hệ</p>', NULL, '', 'Liên hệ', 'Liên hệ ', 'Liên hệ '),
(1, 'en', 'lien-he', 'Liên hệ', NULL, '<p>Li&ecirc;n hệ</p>', NULL, '', 'Liên hệ', 'Liên hệ ', 'Liên hệ '),
(2, 'vi', 'tong-quan', 'Tổng quan', NULL, '<p><span>C&ocirc;ng ty Tr&aacute;ch nhiệm Hữu hạng Lụa Tơ Tằm Phương Mai l&agrave; một trong những doanh nghiệp dệt may - cung cấp sản phẩm v&agrave; dịch vụ thời trang h&agrave;ng đầu Việt Nam, chuy&ecirc;n sản xuất vải dệt, vải in b&ocirc;ng&hellip; từ sợi tơ tằm thi&ecirc;n nhi&ecirc;n 100% với c&ocirc;ng nghệ in 3D độc quyền.&nbsp;</span><br><br><span>Được th&agrave;nh lập từ năm 2016 đến nay, Lụa Tơ Tằm Phương Mai đ&atilde; dần trở th&agrave;nh một thương hiệu mua sắm tin cậy của người ti&ecirc;u d&ugrave;ng trong nước lẫn c&aacute;c doanh nghiệp đối t&aacute;c, đồng thời l&agrave; một trong những c&ocirc;ng ty dệt may uy t&iacute;n tại Việt Nam chuy&ecirc;n cung cấp c&aacute;c sản phẩm từ lụa tơ tằm thi&ecirc;n nhi&ecirc;n c&oacute; xuất xứ tại Bảo Lộc.&nbsp;</span><br><br><span>Để tiếp cận gần hơn với kh&aacute;ch h&agrave;ng, Lụa Tơ Tằm Phương Mai đ&atilde; c&oacute; một bước đi đầy chiến lược với k&ecirc;nh chăm s&oacute;c kh&aacute;ch h&agrave;ng 24/7, hệ thống mua h&agrave;ng trực tuyến n&acirc;ng cao trải nghiệm, đồng thời tăng cường khai trương hệ thống showroom gi&uacute;p qu&aacute; tr&igrave;nh mua sắm th&ecirc;m thuận tiện cho kh&aacute;ch h&agrave;ng tại khắp c&aacute;c tỉnh th&agrave;nh tr&ecirc;n cả nước. Đ&acirc;y lu&ocirc;n l&agrave; động lực cho Lụa Tơ Tằm Phương Mai kh&ocirc;ng ngừng cải tiến v&agrave; ph&aacute;t triển thương hiệu kh&ocirc;ng chỉ ở thị trường nội địa m&agrave; mở rộng sang cả thị trường quốc tế.</span></p>', NULL, '<p><span>Được định vị trở th&agrave;nh &ldquo;Nh&agrave; cung cấp lụa tơ tằm uy t&iacute;n v&agrave; dịch vụ thời trang h&agrave;ng đầu Việt Nam&rdquo; c&ugrave;ng sứ mệnh &ldquo;T&ocirc;n vinh n&eacute;t đẹp truyền thống người phụ nữ Việt&rdquo;, Lụa Tơ Tằm Phương Mai kh&ocirc;ng ngừng đầu tư, ph&aacute;t triển v&agrave; ứng dụng c&aacute;c c&ocirc;ng nghệ ti&ecirc;n tiến nhất kết hợp c&ugrave;ng phương ph&aacute;p truyền thống v&agrave;o sản xuất. C&oacute; thể n&oacute;i, ch&iacute;nh sự kết hợp h&agrave;i ho&agrave; giữa n&eacute;t cổ xưa v&agrave; hiện đại trong quy tr&igrave;nh chế t&aacute;c ngay từ những ng&agrave;y đầu th&agrave;nh lập đ&atilde; g&oacute;p phần l&agrave;m n&ecirc;n th&agrave;nh c&ocirc;ng của thương hiệu.&nbsp;</span><br><br><span>Hoạt động với t&ocirc;n chỉ &ldquo;Lu&ocirc;n đặt uy t&iacute;n v&agrave; chất lượng l&ecirc;n h&agrave;ng đầu&rdquo;, tất cả những sản phẩm của Phương Mai lu&ocirc;n được kiểm so&aacute;t v&agrave; gi&aacute;m định chất lượng chặt chẽ. Mỗi giai đoạn &ndash; một định hướng kh&aacute;c nhau, trong suốt 3 năm qua, Lụa Tơ Tằm Phương Mai lu&ocirc;n tập trung đi s&acirc;u v&agrave;o thiết kế v&agrave; s&aacute;ng tạo c&aacute;c mẫu hoa văn phong ph&uacute; tr&ecirc;n chất liệu lụa. B&ecirc;n cạnh những d&ograve;ng sản phẩm hiện c&oacute;, Lụa Tơ Tằm Phương Mai tiếp tục đẩy mạnh nh&oacute;m sản phẩm d&agrave;nh cho doanh nh&acirc;n v&agrave; giới thượng lưu, nhằm mở rộng thị phần c&ugrave;ng ph&acirc;n kh&uacute;c gi&aacute; ph&ugrave; hợp với nhu cầu của mọi đối tượng kh&aacute;ch h&agrave;ng.&nbsp;</span></p>', 'Tổng quan', 'Tổng quan', 'Tổng quan'),
(2, 'en', 'tong-quan', 'Tổng quan', NULL, '<p>Tổng quan</p>', NULL, '', 'Tổng quan', 'Tổng quan', 'Tổng quan'),
(3, 'vi', 've-chung-toi', 'Về chúng tôi', NULL, '<h4 class=\"col-12\">Phương Mai - Thương hiệu đ&atilde; được ai đ&oacute; tin d&ugrave;ng</h4>', NULL, '<p class=\"col-12\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<div class=\"col-auto\"></div>', 'Về chúng tôi', 'Phương Mai - Thương hiệu đã được ai đó tin dùng', 'Về chúng tôi '),
(3, 'en', 've-chung-toi', 'Về chúng tôi', NULL, '<p>Về ch&uacute;ng t&ocirc;i</p>', NULL, '', 'Về chúng tôi', 'Về chúng tôi ', 'Về chúng tôi '),
(4, 'vi', 'tong-quan-san-pham', 'Tổng quan sản phẩm', NULL, '<h3 class=\"col-12\">Lụa Tơ Tằm, đẳng cấp của loại vải hữu cơ duy nhất tr&ecirc;n đời</h3>', NULL, '<p class=\"col-12\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n<p class=\"col-12\">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p class=\"col-12\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>', 'Tổng quan sản phẩm', 'Tổng quan sản phẩm', 'Tổng quan sản phẩm'),
(4, 'en', 'tong-quan-san-pham', 'Tổng quan sản phẩm', NULL, '<p>Tổng quan sản phẩm</p>', NULL, '<p>Tổng quan sản phẩm</p>', 'Tổng quan sản phẩm', 'Tổng quan sản phẩm', 'Tổng quan sản phẩm'),
(5, 'vi', 'review-trang-chu', 'Review Trang chủ', NULL, '<h1>PHƯƠNG MAI SILK - N&Acirc;NG TẦM GI&Aacute; TRỊ TRUYỀN THỐNG VIỆT</h1>', NULL, '<p>Lụa tơ tằm được mệnh danh l&agrave; loại sản phẩm cao cấp d&agrave;nh cho tầng lớp thượng lưu. Tuy nhi&ecirc;n, &iacute;t ai biết được sự k&igrave; c&ocirc;ng của những nghệ nh&acirc;n g&oacute;p phần tạo n&ecirc;n một n&eacute;t văn h&oacute;a l&agrave;ng nghề truyền thống tơ tằm Việt Nam.</p>\r\n<p>H&atilde;y c&ugrave;ng tr&acirc;n qu&yacute; c&ocirc;ng lao động từng nghệ nh&acirc;n v&agrave; t&ocirc;n trọng họ bằng c&aacute;ch kinh doanh lụa nguy&ecirc;n bản 100% kh&ocirc;ng pha trộn.</p>\r\n<div class=\"text_exposed_show\">\r\n<p>Phương Mai cam kết từng thước vải lụa tơ tằm đến kh&aacute;ch h&agrave;ng đều thật v&agrave; chất lượng cao nhất.</p>\r\n</div>', 'Review Trang chủ', 'Review Trang chủ', 'Review Trang chủ'),
(5, 'en', 'review-trang-chu', 'Review Trang chủ', NULL, '<h1 class=\"col-12\">&Aacute;o D&agrave;i Tơ Tằm - N&eacute;t Đẹp Việt Nam</h1>', NULL, '<p><b class=\"col-12\">Từ h&agrave;ng trăm năm nay, chiếc &aacute;o d&agrave;i lụa đ&atilde; được xem l&agrave; \"quốc hồn quốc tu&yacute;\" của d&acirc;n tộc Việt Nam. Vẻ đẹp mềm mại, dịu d&agrave;ng v&agrave; k&iacute;n đ&aacute;o đặc trưng của chiếc &aacute;o d&agrave;i được thể hiện qua phần cổ &aacute;o cao, bờ vai bo tr&ograve;n nhẹ nh&agrave;ng v&agrave; nổi bật với hai t&agrave; &aacute;o thướt tha đầy nữ t&iacute;nh.</b></p>\r\n<p class=\"col-12\">Theo d&ograve;ng chảy lịch sử, thiết kế của &aacute;o d&agrave;i đ&atilde; c&oacute; nhiều cải tiến để ph&ugrave; hợp với từng giai đoạn của x&atilde; hội. Nhưng d&ugrave; c&oacute; thay đổi thế n&agrave;o đi nữa, &aacute;o d&agrave;i vẫn lu&ocirc;n l&agrave; biểu tượng của vẻ đẹp, văn h&oacute;a v&agrave; l&agrave; niềm tự h&agrave;o của h&agrave;ng triệu người con đất Việt.</p>\r\n<p class=\"col-12\">Nếu với ph&aacute;i nữ, chiếc &aacute;o d&agrave;i lụa l&agrave; trang phục t&ocirc;n vinh n&eacute;t duy&ecirc;n thầm k&iacute;n, thanh lịch, th&igrave; &aacute;o d&agrave;i nam lại l&agrave; trang phục mang n&eacute;t trang trọng tạo n&ecirc;n t&acirc;m hồn, cốt c&aacute;ch của người đ&agrave;n &ocirc;ng đất Việt. Thế nhưng, &iacute;t ai biết rằng để c&oacute; được vị thế như ng&agrave;y h&ocirc;m nay, &aacute;o d&agrave;i đ&atilde; phải trải qua nhiều biến cố thăng trầm.</p>\r\n<p class=\"col-12\">C&oacute; thể thấy rằng tại Việt Nam chiếc &aacute;o d&agrave;i lụa l&agrave; trang phục lu&ocirc;n xuất hiện trong nhiều sự kiện kh&aacute;c nhau, từ những dịp trang trọng cho đến lễ cưới, hoặc những ng&agrave;y lễ tết. C&oacute; thể n&oacute;i rằng truyền thống &aacute;o d&agrave;i đ&atilde; g&oacute;p phần quảng b&aacute; h&igrave;nh ảnh Việt Nam ra thế giới. Hẳn sẽ rất tuyệt vời nếu bạn sở hữu một bộ &aacute;o d&agrave;i.</p>\r\n<p class=\"col-12\">Thị trường thời trang hiện nay rất dễ d&agrave;ng để bạn chọn cho m&igrave;nh một loại vải ph&ugrave; hợp để may &aacute;o d&agrave;i. Tuy nhi&ecirc;n, để c&oacute; thể t&ocirc;n vinh n&eacute;t đẹp duy&ecirc;n d&aacute;ng của người mặc th&igrave; chất liệu lụa tơ tằm thi&ecirc;n nhi&ecirc;n ch&iacute;nh l&agrave; lựa chọn v&ocirc; c&ugrave;ng ho&agrave;n hảo. Với đặc t&iacute;nh mềm mại, nhẹ nh&agrave;ng nhưng bền chặt, chiếc &aacute;o d&agrave;i được may từ vải lụa tơ tằm sẽ kh&eacute;o l&eacute;o tạo cảm gi&aacute;c thoải m&aacute;i, l&agrave;m nổi bật h&igrave;nh thể cho người mặc,.</p>\r\n<p class=\"col-12\">Cuộc sống hiện đại khiến nhu cầu v&agrave; phong c&aacute;ch thời trang của mọi người ng&agrave;y c&agrave;ng thay đổi, nhưng &aacute;o d&agrave;i lụa vẫn lu&ocirc;n l&agrave; trang phục đặc trưng của người phụ nữ Việt Nam m&agrave; kh&ocirc;ng một loại trang phục n&agrave;o c&oacute; thể thay thế được. H&igrave;nh ảnh người con g&aacute;i duy&ecirc;n d&aacute;ng với t&agrave; &aacute;o d&agrave;i thướt tha sẽ lu&ocirc;n để lại ấn tượng s&acirc;u sắc trong l&ograve;ng những du kh&aacute;ch nước ngo&agrave;i, v&agrave; đ&oacute; cũng ch&iacute;nh l&agrave; n&eacute;t đẹp văn h&oacute;a truyền thống đ&aacute;ng ngưỡng mộ của người phụ nữ Việt Nam.</p>', 'Review Trang chủ', 'Review Trang chủ', 'Review Trang chủ');

-- --------------------------------------------------------

--
-- Table structure for table `ap_post`
--

CREATE TABLE `ap_post` (
  `id` int(11) NOT NULL,
  `id_crawler` int(11) DEFAULT NULL,
  `category_product` varchar(255) DEFAULT 'NULL' COMMENT 'List ID cate product',
  `thumbnail` varchar(255) NOT NULL,
  `album` text,
  `url_video` varchar(255) DEFAULT NULL,
  `is_status` tinyint(2) UNSIGNED NOT NULL DEFAULT '2' COMMENT 'trang thai',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `displayed_time` date NOT NULL COMMENT 'ngay publish',
  `program` tinyint(1) NOT NULL DEFAULT '0',
  `number` varchar(5) DEFAULT '1',
  `viewed` bigint(20) NOT NULL DEFAULT '0',
  `type` varchar(100) DEFAULT NULL COMMENT 'Partime/fulltime',
  `type_career` int(5) DEFAULT NULL COMMENT 'Loại hình làm việc',
  `level` varchar(50) DEFAULT NULL COMMENT 'Trình độ',
  `address` varchar(255) DEFAULT NULL COMMENT 'Địa điểm làm việc',
  `address_career` varchar(255) DEFAULT NULL COMMENT 'Chi nhánh',
  `expiration_time` date NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngay tao',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngay sua',
  `files` varchar(255) DEFAULT NULL,
  `salary` varchar(50) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_post`
--

INSERT INTO `ap_post` (`id`, `id_crawler`, `category_product`, `thumbnail`, `album`, `url_video`, `is_status`, `is_featured`, `displayed_time`, `program`, `number`, `viewed`, `type`, `type_career`, `level`, `address`, `address_career`, `expiration_time`, `created_time`, `updated_time`, `files`, `salary`, `time`) VALUES
(1, NULL, 'NULL', '01.jpg', NULL, NULL, 1, 0, '0000-00-00', 0, '1', 0, NULL, NULL, NULL, '', NULL, '0000-00-00', '2019-08-23 16:06:33', '2019-08-23 16:06:33', NULL, NULL, NULL),
(2, NULL, 'NULL', '02.jpg', NULL, NULL, 1, 0, '0000-00-00', 0, '1', 0, NULL, NULL, NULL, '', NULL, '0000-00-00', '2019-08-23 16:06:59', '2019-08-23 16:06:59', NULL, NULL, NULL),
(3, NULL, 'NULL', 'home.jpg', NULL, NULL, 1, 0, '0000-00-00', 0, '1', 0, NULL, NULL, NULL, '', NULL, '0000-00-00', '2019-08-23 16:07:28', '2019-08-23 16:07:28', NULL, NULL, NULL),
(4, NULL, 'NULL', '02.jpg', NULL, NULL, 1, 0, '0000-00-00', 0, '1', 0, NULL, NULL, NULL, '', NULL, '0000-00-00', '2019-08-23 16:07:54', '2019-08-23 16:07:54', NULL, NULL, NULL),
(5, NULL, 'NULL', '01.jpg', NULL, NULL, 1, 0, '0000-00-00', 0, '1', 0, NULL, NULL, NULL, '', NULL, '0000-00-00', '2019-08-23 16:58:02', '2019-08-23 16:58:02', NULL, NULL, NULL),
(6, NULL, 'NULL', 'phuong_mai_silk.jpg', NULL, NULL, 1, 0, '0000-00-00', 0, '1', 0, NULL, NULL, NULL, '', NULL, '0000-00-00', '2019-08-23 16:58:35', '2019-08-27 11:11:02', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_post_category`
--

CREATE TABLE `ap_post_category` (
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_post_category`
--

INSERT INTO `ap_post_category` (`post_id`, `category_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ap_post_translations`
--

CREATE TABLE `ap_post_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(5) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `content` longtext,
  `content_more` text,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(170) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_post_translations`
--

INSERT INTO `ap_post_translations` (`id`, `language_code`, `slug`, `title`, `description`, `content`, `content_more`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 'vi', 'tieu-de-bai-viet', 'TIều đề bài viết', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', '<p>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</p>', NULL, 'TIều đề bài viết', 'TIều đề bài viết', 'TIều đề bài viết'),
(1, 'en', 'tieu-de-bai-viet', 'TIều đề bài viết', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', '<p>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</p>', NULL, 'TIều đề bài viết', 'TIều đề bài viết', 'TIều đề bài viết'),
(2, 'vi', 'tieu-de-bai-vietes', 'Tiêu đề bài vietes', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', '<p>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</p>', NULL, 'Tiêu đề bài vietes', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', 'Tiêu đề bài vietes'),
(2, 'en', 'tieu-de-bai-vietes', 'Tiêu đề bài vietes', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', '<p>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</p>', NULL, 'Tiêu đề bài vietes', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', 'Tiêu đề bài vietes'),
(3, 'vi', 'tieu-de-bai-vietes', 'Tiêu đề bài vietes', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', '<p><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span></p>', NULL, 'Tiêu đề bài vietes', 'Tiêu đề bài vietes', 'Tiêu đề bài vietes'),
(3, 'en', 'tieu-de-bai-vietes', 'Tiêu đề bài vietes', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', '<p><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span></p>', NULL, 'Tiêu đề bài vietes', 'Tiêu đề bài vietes', 'Tiêu đề bài vietes'),
(4, 'vi', 'tieu-de-bai-viet', 'Tiêu đề bài viết', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', '<p><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span></p>', NULL, 'Tiêu đề bài viết', 'Tiêu đề bài viết', 'Tiêu đề bài viết'),
(4, 'en', 'tieu-de-bai-viet', 'Tiêu đề bài viết', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', '<p><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span></p>', NULL, 'Tiêu đề bài viết', 'Tiêu đề bài viết', 'Tiêu đề bài viết'),
(5, 'vi', 'tieu-de-1', 'Tiêu đề 1', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', '<p><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span><span>Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn. Tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch, tr&iacute;ch đoạn tr&iacute;ch đoạn tr&iacute;ch tr&iacute;ch đoạn.</span></p>', NULL, 'Tiêu đề 1', 'Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn. Trích đoạn trích đoạn trích đoạn trích, trích đoạn trích đoạn trích trích đoạn.', 'Tiêu đề 1'),
(5, 'en', 'tieu-de-1', 'Tiêu đề 1', 'Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1Tiêu đề 1', '<p>Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1Ti&ecirc;u đề 1</p>', NULL, 'Tiêu đề 1', 'Tiêu đề 1', 'Tiêu đề 1'),
(6, 'vi', 'Lụa Phương Mai đưa sen lên áo dài tại Festival Nghề truyền thống Huế 2019', 'Vnexpress: Lụa Phương Mai đưa sen lên áo dài tại Festival Nghề truyền thống Huế 2019', 'Lần đầu đến với Lễ hội Áo dài trong khuôn khổ Festival Nghề truyền thống Huế 2019 (từ 26/4 đến 2/5), Phương Mai Silk giới thiệu bộ sưu tập Người đẹp và sen mang vẻ dịu dàng, nền nã.', '<p>Tại đ&ecirc;m diễn c&oacute; chủ đề &Aacute;o d&agrave;i tr&ecirc;n con đường di sản, đại diện đến từ thủ phủ của tơ tằm Việt Nam - v&ugrave;ng đất Bảo Lộc (L&acirc;m Đồng) - giới thiệu đến c&ocirc;ng ch&uacute;ng những mẫu &aacute;o d&agrave;i lụa thuộc h&agrave;ng &ldquo;thượng phẩm&rdquo;. Lụa ở đ&acirc;y được người dệt đến từ c&aacute;c l&agrave;ng nghề chăm ch&uacute;t cần thận từng chi tiết nhỏ từ l&uacute;c chăn tằm cho tới ươm tơ dệt vải.</p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-1-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-1-ngoisao.vn-w580-h870 18\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-2-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-2-ngoisao.vn-w580-h870 17\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-3-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-3-ngoisao.vn-w580-h870 16\"></p>\r\n<p>Đ&acirc;y l&agrave; lần đầu ti&ecirc;n Phương Mai kết hợp c&ugrave;ng c&aacute;c nh&agrave; thiết kế người Huế (Viết Bảo, Xu&acirc;n Hảo) đưa c&ocirc;ng nghệ in 3D l&ecirc;n tơ tằm Việt Nam nguy&ecirc;n chất 100% để tạo n&ecirc;n 40 mẫu &aacute;o d&agrave;i được chăm ch&uacute;t kỹ từng đường n&eacute;t.</p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-4-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-4-ngoisao.vn-w580-h870 15\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-5-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-5-ngoisao.vn-w580-h870 14\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-6-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-6-ngoisao.vn-w580-h870 13\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-9-ngoisao.vn-w580-h387.jpeg\" title=\"phuong-mai-silk-194-9-ngoisao.vn-w580-h387 10\"></p>\r\n<p>NTK Viết Bảo chia sẻ: &ldquo;<em>Ch&uacute;ng t&ocirc;i thống nhất c&ugrave;ng đem đến Festival nghề truyền thống năm nay bộ sưu tập chủ đề hoa sen. Người ta vẫn n&oacute;i &ldquo;người đẹp v&igrave; lụa&rdquo;, th&igrave; ở đ&acirc;y, với cảm hứng từ sen hồ Tịnh T&acirc;m ở Huế, c&aacute;c c&ocirc; g&aacute;i đẹp hơn v&igrave; lụa v&agrave;... v&igrave; sen</em>&rdquo;.</p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-7-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-7-ngoisao.vn-w580-h870 12\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-8-ngoisao.vn-w580-h387.jpeg\" title=\"phuong-mai-silk-194-8-ngoisao.vn-w580-h387 11\"></p>\r\n<p>Ngo&agrave;i kỹ thuật th&ecirc;u truyền thống, NTK gốc Huế cho biết, anh c&ugrave;ng Phương Mai ch&uacute; trọng khai th&aacute;c c&ocirc;ng nghệ in tr&ecirc;n tơ tằm ti&ecirc;n tiến để chuyển tải vẻ đẹp của t&agrave; &aacute;o d&agrave;i truyền thống.</p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-10-ngoisao.vn-w580-h387.jpeg\" title=\"phuong-mai-silk-194-10-ngoisao.vn-w580-h387 9\"><ins class=\"982a9496\" data-key=\"ca6a8dc0cc8e17880e8cbec473e69d55\" id=\"982a9496-ca6a8dc0cc8e17880e8cbec473e69d55-3-7521\"><ins id=\"982a9496-ca6a8dc0cc8e17880e8cbec473e69d55-3-7521-1\"></ins></ins></p>\r\n<div id=\"yomedia-passback-cf8ba14f70e6410eb7ddc777ae65e5b5-1566903779921\">\r\n<div id=\"bs__1566903783924\" class=\"ad__scope\" data-placement=\"1161\" data-width=\"0\">\r\n<div class=\"ad__slot\">\r\n<div class=\"ad__box\"></div>\r\n</div>\r\n</div>\r\n</div>\r\n<p></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-11-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-11-ngoisao.vn-w580-h870 8\"></p>\r\n<p>&ldquo;<em>Chất liệu được ch&uacute;ng t&ocirc;i sử dụng kh&ocirc;ng g&acirc;y hại cho da. Qua bộ sưu tập, ch&uacute;ng t&ocirc;i muốn t&ocirc;n vinh chất liệu lụa truyền thống của l&agrave;ng nghề Việt Nam, x&oacute;a bỏ những chất liệu kh&ocirc;ng nguồn gốc, vải giả tơ lụa đang tr&agrave; trộn tr&ecirc;n thị trường, đ&aacute;nh lừa người ti&ecirc;u dụng</em>&rdquo;, đại diện Phương Mai Silk khẳng định.</p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-12-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-12-ngoisao.vn-w580-h870 7\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-13-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-13-ngoisao.vn-w580-h870 6\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-15-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-15-ngoisao.vn-w580-h870 4\"></p>\r\n<p>Với chủ đề Tinh hoa nghề Việt, Festival Nghề truyền thống Huế 2019 sẽ giới thiệu đến du kh&aacute;ch c&aacute;c sản phẩm độc đ&aacute;o của nhiều nghề, l&agrave;ng nghề truyền thống đặc trưng của Huế v&agrave; nhiều địa phương trong cả nước. Mục ti&ecirc;u của sự kiện quy m&ocirc; n&agrave;y l&agrave; vừa kh&ocirc;i phục, bảo tồn l&agrave;ng nghề; vừa x&acirc;y dựng v&agrave; ph&aacute;t triển c&aacute;c tour, tuyến du lịch gắn với l&agrave;ng nghề truyền thống.</p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-14-ngoisao.vn-w580-h387.jpeg\" title=\"phuong-mai-silk-194-14-ngoisao.vn-w580-h387 5\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-16-ngoisao.vn-w580-h387.jpeg\" title=\"phuong-mai-silk-194-16-ngoisao.vn-w580-h387 3\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-18-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-18-ngoisao.vn-w580-h870 1\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-17-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-17-ngoisao.vn-w580-h870 2\"></p>\r\n<p class=\"photo\"><img alt=\"Lụa Phương Mai, Festival Nghệ truyền thống Huế 2019, Phương Mai Silk\" src=\"https://media.ngoisao.vn/resize_580/news/2019/04/19/phuong-mai-silk-194-19-ngoisao.vn-w580-h870.jpeg\" title=\"phuong-mai-silk-194-19-ngoisao.vn-w580-h870 0\"></p>\r\n<p class=\"photo\">Tr&iacute;ch dẫn:</p>\r\n<p class=\"photo\"><a href=\"https://ngoisao.vn/dep/thoi-trang/ngam-sen-xu-hue-tren-ao-dai-lua-truyen-thong-262639.htm\">https://ngoisao.vn/dep/thoi-trang/ngam-sen-xu-hue-tren-ao-dai-lua-truyen-thong-262639.htm</a></p>', NULL, 'Tin tức 2Lụa Phương Mai đưa sen lên áo dài tại Festival', 'Lần đầu đến với Lễ hội Áo dài trong khuôn khổ Festival Nghề truyền thống Huế 2019 (từ 26/4 đến 2/5).', 'Phương Mai Silk'),
(6, 'en', 'tin-tuc-2', 'Tin tức 2', 'Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2', '<p>Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2Tin tức 2</p>', NULL, 'Tin tức 2Tin tức 2Tin tức 2Tin tức 2', 'Tin tức 2Tin tức 2Tin tức 2Tin tức 2', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_product`
--

CREATE TABLE `ap_product` (
  `id` int(11) NOT NULL,
  `quantity` int(4) DEFAULT '0',
  `banner` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `album` text,
  `viewed` int(5) NOT NULL DEFAULT '0',
  `model` varchar(20) DEFAULT NULL,
  `price` decimal(15,0) DEFAULT NULL,
  `price_sale` decimal(15,0) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) DEFAULT '0',
  `is_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `features` text,
  `space` text,
  `project` text,
  `is_qty` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:còn hàng, 0 hết hàng',
  `sale_up` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `best_seller` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_product`
--

INSERT INTO `ap_product` (`id`, `quantity`, `banner`, `image`, `thumbnail`, `album`, `viewed`, `model`, `price`, `price_sale`, `is_featured`, `is_status`, `created_time`, `updated_time`, `features`, `space`, `project`, `is_qty`, `sale_up`, `unit`, `best_seller`) VALUES
(1, 0, NULL, NULL, '01.jpg', '[\"product_(1)z.jpg\",\"product_(2).jpg\",\"product_(2)z.jpg\"]', 0, '', '100000', '100000', 0, 1, '2019-08-23 20:04:04', '2019-08-26 04:50:04', NULL, NULL, NULL, 1, 0, 0, 0),
(2, 0, NULL, NULL, '02.jpg', '[\"product_(1)z.jpg\",\"product_(2).jpg\",\"product_(2)z.jpg\"]', 0, '', '111111', '0', 0, 1, '2019-08-23 20:06:05', '2019-08-26 04:50:02', NULL, NULL, NULL, 1, 0, 0, 0),
(3, 0, NULL, NULL, 'product_(2).jpg', '[\"product_(1)z.jpg\",\"product_(2).jpg\",\"product_(2)z.jpg\"]', 0, '', '11111', '0', 1, 1, '2019-08-23 20:06:39', '2019-08-26 04:50:00', NULL, NULL, NULL, 1, 0, 0, 0),
(4, 0, NULL, NULL, 'product_(2).jpg', '[\"product_(1)z.jpg\",\"product_(2).jpg\",\"product_(2)z.jpg\"]', 0, '', '55555555', '0', 1, 1, '2019-08-23 20:07:12', '2019-08-26 04:49:59', NULL, NULL, NULL, 1, 0, 0, 0),
(5, 0, NULL, NULL, 'product_(1).jpg', '[\"product_(1)z.jpg\",\"product_(2).jpg\",\"product_(2)z.jpg\"]', 0, '', '55555555', '0', 1, 1, '2019-08-23 20:07:47', '2019-08-26 04:49:57', NULL, NULL, NULL, 1, 0, 0, 0),
(6, 0, NULL, NULL, 'product_(1).jpg', '[\"product_(1)z.jpg\",\"product_(2).jpg\",\"product_(2)z.jpg\"]', 0, '', '7777777', '0', 1, 1, '2019-08-23 20:08:20', '2019-08-26 04:49:55', NULL, NULL, NULL, 1, 0, 0, 0),
(7, 0, NULL, NULL, 'product_(1).jpg', '[\"product_(1)z.jpg\",\"product_(2).jpg\",\"product_(2)z.jpg\"]', 0, '', '333333', '0', 1, 1, '2019-08-23 20:08:51', '2019-08-26 04:48:06', NULL, NULL, NULL, 1, 0, 0, 0),
(8, 0, NULL, NULL, 'product_(2).jpg', '[\"product_(2)z.jpg\",\"product_(4).jpg\",\"product_(4)z.jpg\",\"phuong_mai_silk_khan_choang_co_1x1_01_(1).png\"]', 0, NULL, '2222', '0', 0, 1, '2019-08-26 12:32:46', '2019-08-28 15:50:22', NULL, NULL, NULL, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_category`
--

CREATE TABLE `ap_product_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_product_category`
--

INSERT INTO `ap_product_category` (`product_id`, `category_id`) VALUES
(1, 10),
(2, 9),
(3, 7),
(4, 8),
(5, 5),
(6, 6),
(7, 7),
(8, 6);

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_property`
--

CREATE TABLE `ap_product_property` (
  `product_id` int(11) UNSIGNED NOT NULL,
  `property_id` int(11) UNSIGNED NOT NULL,
  `type` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_translations`
--

CREATE TABLE `ap_product_translations` (
  `id` int(11) NOT NULL,
  `language_code` char(2) NOT NULL,
  `packing` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `description` text,
  `content_info` longtext NOT NULL,
  `content_benefit` longtext,
  `content_use` longtext,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_product_translations`
--

INSERT INTO `ap_product_translations` (`id`, `language_code`, `packing`, `title`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `description`, `content_info`, `content_benefit`, `content_use`, `content`) VALUES
(1, 'en', NULL, 'sản phẩm 1', 'san-pham-1', 'sản phẩm 1', 'Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1', 'sản phẩm 1', 'Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1', '', NULL, NULL, '<p>Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1</p>'),
(1, 'vi', NULL, 'Sản phẩm 1', 'san-pham-1', 'Sản phẩm 1', 'Sản phẩm 1', 'Sản phẩm 1', 'Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1', '', NULL, NULL, '<p>Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1</p>'),
(2, 'en', NULL, 'Sản phẩm 2', 'san-pham-2', 'Sản phẩm 2', 'Sản phẩm 2', 'Sản phẩm 2', 'Sản phẩm 2Sản phẩm 2Sản phẩm 2Sản phẩm 2Sản phẩm 2Sản phẩm 2', '', NULL, NULL, '<p>Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1</p>'),
(2, 'vi', NULL, 'Sản phẩm 2', 'san-pham-2', 'Sản phẩm 2', 'Sản phẩm 2', 'Sản phẩm 2', 'Sản phẩm 2', '', NULL, NULL, '<p>Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1Sản phẩm 1</p>'),
(3, 'en', NULL, 'Sản phẩm 3', 'san-pham-3', 'Sản phẩm 3', 'Sản phẩm 3', 'Sản phẩm 3', 'Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3', '', NULL, NULL, '<p>Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3</p>'),
(3, 'vi', NULL, 'Sản phẩm 3', 'san-pham-3', 'Sản phẩm 3', 'Sản phẩm 3', 'Sản phẩm 3', 'Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3', '', NULL, NULL, '<p>Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3Sản phẩm 3</p>'),
(4, 'en', NULL, 'Sản phẩm 4', 'san-pham-4', 'Sản phẩm 4', 'Sản phẩm 4', 'Sản phẩm 4', 'Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4', '', NULL, NULL, '<p>Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4</p>'),
(4, 'vi', NULL, 'Sản phẩm 4', 'san-pham-4', 'Sản phẩm 4', 'Sản phẩm 4', 'Sản phẩm 4', 'Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4', '', NULL, NULL, ''),
(5, 'en', NULL, 'Sản phẩm 5', 'san-pham-5', 'Sản phẩm 5', 'Sản phẩm 5', 'Sản phẩm 5', 'Sản phẩm 5Sản phẩm 5Sản phẩm 5Sản phẩm 5Sản phẩm 5Sản phẩm 5', '', NULL, NULL, '<p>Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4</p>'),
(5, 'vi', NULL, 'Sản phẩm 5', 'san-pham-5', 'Sản phẩm 5', 'Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 5', 'Sản phẩm 5', 'Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4', '', NULL, NULL, '<p>Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4Sản phẩm 4</p>'),
(6, 'en', NULL, 'Sản phẩm 6', 'san-pham-6', 'Sản phẩm 6', 'Sản phẩm 6', 'Sản phẩm 6', 'Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6', '', NULL, NULL, '<p>Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6</p>'),
(6, 'vi', NULL, 'Sản phẩm 6', 'san-pham-6', 'Sản phẩm 6', 'Sản phẩm 6', 'Sản phẩm 6', 'Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6', '', NULL, NULL, '<p>Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6Sản phẩm 6</p>'),
(7, 'en', NULL, 'Sản phẩm 7', 'san-pham-7', 'Sản phẩm 7', 'Sản phẩm 7', 'Sản phẩm 7', 'Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7', '', NULL, NULL, '<p>Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7</p>'),
(7, 'vi', NULL, 'Sản phẩm 7', 'san-pham-7', 'Sản phẩm 7', 'Sản phẩm 7', 'Sản phẩm 7', 'Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7', '', NULL, NULL, '<p>Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7Sản phẩm 7</p>'),
(8, 'en', NULL, 'Test nhé', 'test-nhe', 'Test nhé', 'Test nhé', 'Test nhé', 'Test nhé', '', NULL, NULL, '<p>Test nh&eacute;</p>'),
(8, 'vi', NULL, 'Test nhé', 'test-nhe', 'Test nhé', 'Test nhé', 'Test nhé', 'Test nhé', '', NULL, NULL, '<ul>\r\n<li>Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute;</li>\r\n<li>Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute; Test nh&eacute;</li>\r\n</ul>');

-- --------------------------------------------------------

--
-- Table structure for table `ap_property`
--

CREATE TABLE `ap_property` (
  `id` int(11) NOT NULL,
  `type` varchar(30) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `album` varchar(255) DEFAULT NULL,
  `color_code` varchar(255) DEFAULT NULL,
  `color_hex` varchar(255) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `is_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '2' COMMENT 'trang thai',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngay tao',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngay sua',
  `check_video` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_property`
--

INSERT INTO `ap_property` (`id`, `type`, `category_id`, `thumbnail`, `order`, `album`, `color_code`, `color_hex`, `class`, `is_status`, `created_time`, `updated_time`, `check_video`) VALUES
(1, 'banner', NULL, NULL, 0, NULL, NULL, '', NULL, 1, '2019-08-23 19:32:14', '2019-08-23 19:32:14', NULL),
(2, 'banner', NULL, NULL, 0, NULL, NULL, '', NULL, 1, '2019-08-23 20:38:00', '2019-08-23 20:38:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_property_translations`
--

CREATE TABLE `ap_property_translations` (
  `id` int(11) NOT NULL,
  `language_code` varchar(5) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `time` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `content` longtext,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(170) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_property_translations`
--

INSERT INTO `ap_property_translations` (`id`, `language_code`, `slug`, `title`, `time`, `description`, `content`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 'en', NULL, 'Banner trang danh mục sản phẩm', NULL, 'Banner trang danh mục sản phẩm', NULL, NULL, NULL, NULL),
(1, 'vi', NULL, 'Banner trang danh mục sản phẩm', NULL, 'Banner trang danh mục sản phẩm', NULL, NULL, NULL, NULL),
(2, 'en', NULL, 'Banner trang chủ', NULL, 'Banner trang chủ', NULL, NULL, NULL, NULL),
(2, 'vi', NULL, 'Banner trang chủ', NULL, 'Banner trang chủ', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_suggest_product`
--

CREATE TABLE `ap_suggest_product` (
  `id` int(11) NOT NULL,
  `displayed_time` date NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `data` text,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_system_menu`
--

CREATE TABLE `ap_system_menu` (
  `id` int(10) NOT NULL,
  `text` varchar(75) NOT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `href` varchar(255) NOT NULL DEFAULT '#',
  `controller` varchar(30) DEFAULT NULL,
  `parent_id` int(4) NOT NULL DEFAULT '0',
  `class` varchar(255) DEFAULT NULL,
  `order` int(4) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_system_menu`
--

INSERT INTO `ap_system_menu` (`id`, `text`, `icon`, `href`, `controller`, `parent_id`, `class`, `order`, `target`) VALUES
(1, 'Bảng điều khiển', 'fa fa-dashboard', '#', NULL, 0, '', 30, NULL),
(2, 'Quản lý thành viên', 'fa fa-users', '#', NULL, 0, 'treeview', 0, '_self'),
(3, 'Danh sách nhóm', 'fa fa-users', 'groups', 'groups', 2, '', 0, '_self'),
(4, 'Danh sách thành viên', 'fa fa-user', 'users', 'users', 2, '', 0, '_self'),
(6, 'Quản trị nội dung', 'fa fa-newspaper-o', '#', NULL, 0, 'treeview', 0, '_self'),
(7, 'Danh mục tin tức', 'fa fa-list-ol', 'category/post', 'category', 6, '', 0, '_self'),
(8, 'Danh sách tin tức', 'fa fa-file-pdf-o', 'post', 'post', 6, '', 0, '_self'),
(10, 'Quản lý liên hệ', 'fa fa-align-justify', 'contact', 'contact', 6, '', 0, '_self'),
(11, 'Đa phương tiện', 'fa fa-picture-o', '#', NULL, 0, 'treeview', 0, '_self'),
(15, 'Quản lý sản phẩm', 'fa fa-product-hunt', '#', NULL, 0, 'treeview', 0, '_self'),
(17, 'Quản lý page', 'fa fa-file-o', 'page', 'page', 0, '', 0, '_self'),
(19, 'Quản lý media', 'fa fa-file-picture-o', 'media', 'media', 11, '', 0, '_self'),
(20, 'Vị trí banner', 'fa fa-align-left', 'property/banner', 'property', 11, '', 0, '_self'),
(21, 'Danh sách banner', 'fa fa-file-text-o', 'banner', 'banner', 11, '', 0, '_self'),
(23, 'Nhà cung cấp', 'fa fa-bank', 'property/brand', 'property', 14, '', 0, '_self'),
(24, 'Đơn vị tính', 'fa fa-balance-scale', 'property/unit', 'property', 14, '', 0, '_self'),
(25, 'Danh mục sản phẩm', 'fa fa-list', 'category/product', 'category', 15, '', 0, '_self'),
(26, 'Danh sách sản phẩm', 'fa fa-product-hunt', 'product', 'product', 15, '', 0, '_self'),
(30, 'Quản lý newsletter', 'fa fa-envelope-o', 'newsletter', 'newsletter', 6, '', 0, '_self');

-- --------------------------------------------------------

--
-- Table structure for table `ap_users`
--

CREATE TABLE `ap_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_small` varchar(255) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `created_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_users`
--

INSERT INTO `ap_users` (`id`, `ip_address`, `username`, `last_name`, `first_name`, `full_name`, `email`, `phone`, `password`, `salt`, `thumbnail`, `thumbnail_small`, `company`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `active`, `address`, `last_login`, `updated_time`, `created_time`) VALUES
(1, '127.0.0.1', 'admin', 'admin', 'admin', 'Đinh Văn Khương', 'admin@gmail.com', '0973683037', '$2y$08$KrBa7LvLlW08hMuGayf.xuX.rpYa/eSVt09HtSwSwvN9.fiL36vlu', '', 'author.jpg', NULL, 'Năm 1998', '', NULL, NULL, NULL, 1268889823, 1, NULL, 1567002404, '2019-01-31 17:30:53', '2017-12-17 00:49:09'),
(59, '14.177.235.179', 'vnpt', 'Nguyen Thi', 'Ha', NULL, 'admin@vnpt.vn', '0970709700', '$2y$08$8ETgkHAubNy4pc4NukmhYuM2QTYLr8YPdnE1lm0UdrYFK6xVER5vu', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1559205333, 1, NULL, 1562840430, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_users_groups`
--

CREATE TABLE `ap_users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ap_users_groups`
--

INSERT INTO `ap_users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(217, 59, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ap_video`
--

CREATE TABLE `ap_video` (
  `id` int(11) NOT NULL,
  `link_video` varchar(500) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ap_account`
--
ALTER TABLE `ap_account`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_account_groups`
--
ALTER TABLE `ap_account_groups`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`) USING BTREE,
  ADD KEY `fk_users_groups_users1_idx` (`user_id`) USING BTREE,
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`) USING BTREE;

--
-- Indexes for table `ap_agency`
--
ALTER TABLE `ap_agency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_agency_translations`
--
ALTER TABLE `ap_agency_translations`
  ADD UNIQUE KEY `ap_agency_translations_id_language_code_pk` (`id`,`language_code`) USING BTREE;

--
-- Indexes for table `ap_banner`
--
ALTER TABLE `ap_banner`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_banner_translations`
--
ALTER TABLE `ap_banner_translations`
  ADD UNIQUE KEY `ap_banner_translations_id_language_code_pk` (`id`,`language_code`) USING BTREE;

--
-- Indexes for table `ap_category`
--
ALTER TABLE `ap_category`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_category_translations`
--
ALTER TABLE `ap_category_translations`
  ADD UNIQUE KEY `ap_category_translations_id_language_code_pk` (`id`,`language_code`) USING BTREE;

--
-- Indexes for table `ap_contact`
--
ALTER TABLE `ap_contact`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_faqs`
--
ALTER TABLE `ap_faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_faqs_translations`
--
ALTER TABLE `ap_faqs_translations`
  ADD UNIQUE KEY `ap_faqs_translations_id_language_code_pk` (`id`,`language_code`) USING BTREE;

--
-- Indexes for table `ap_groups`
--
ALTER TABLE `ap_groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_group_members`
--
ALTER TABLE `ap_group_members`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_location_city`
--
ALTER TABLE `ap_location_city`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_location_district`
--
ALTER TABLE `ap_location_district`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_location_ward`
--
ALTER TABLE `ap_location_ward`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_logged_device`
--
ALTER TABLE `ap_logged_device`
  ADD UNIQUE KEY `ap_logged_device_user_id_ip_address_user_agent_pk` (`user_id`,`ip_address`,`user_agent`) USING BTREE;

--
-- Indexes for table `ap_login_attempts`
--
ALTER TABLE `ap_login_attempts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_log_action`
--
ALTER TABLE `ap_log_action`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_menus`
--
ALTER TABLE `ap_menus`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_newsletter`
--
ALTER TABLE `ap_newsletter`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_order`
--
ALTER TABLE `ap_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_page`
--
ALTER TABLE `ap_page`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_page_translations`
--
ALTER TABLE `ap_page_translations`
  ADD UNIQUE KEY `ap_page_translations_id_language_code_pk` (`id`,`language_code`) USING BTREE;

--
-- Indexes for table `ap_post`
--
ALTER TABLE `ap_post`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_post_category`
--
ALTER TABLE `ap_post_category`
  ADD PRIMARY KEY (`post_id`,`category_id`) USING BTREE;

--
-- Indexes for table `ap_post_translations`
--
ALTER TABLE `ap_post_translations`
  ADD UNIQUE KEY `ap_post_translations_id_language_code_pk` (`id`,`language_code`) USING BTREE;

--
-- Indexes for table `ap_product`
--
ALTER TABLE `ap_product`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_product_category`
--
ALTER TABLE `ap_product_category`
  ADD PRIMARY KEY (`product_id`,`category_id`) USING BTREE;

--
-- Indexes for table `ap_product_property`
--
ALTER TABLE `ap_product_property`
  ADD PRIMARY KEY (`product_id`,`property_id`) USING BTREE;

--
-- Indexes for table `ap_product_translations`
--
ALTER TABLE `ap_product_translations`
  ADD UNIQUE KEY `ap_product_translations_id_language_code_pk` (`id`,`language_code`) USING BTREE;

--
-- Indexes for table `ap_property`
--
ALTER TABLE `ap_property`
  ADD PRIMARY KEY (`id`,`color_hex`) USING BTREE;

--
-- Indexes for table `ap_property_translations`
--
ALTER TABLE `ap_property_translations`
  ADD UNIQUE KEY `ap_banner_translations_id_language_code_pk` (`id`,`language_code`) USING BTREE;

--
-- Indexes for table `ap_suggest_product`
--
ALTER TABLE `ap_suggest_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_system_menu`
--
ALTER TABLE `ap_system_menu`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_users`
--
ALTER TABLE `ap_users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ap_users_groups`
--
ALTER TABLE `ap_users_groups`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`) USING BTREE,
  ADD KEY `fk_users_groups_users1_idx` (`user_id`) USING BTREE,
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`) USING BTREE;

--
-- Indexes for table `ap_video`
--
ALTER TABLE `ap_video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ap_account`
--
ALTER TABLE `ap_account`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ap_account_groups`
--
ALTER TABLE `ap_account_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ap_agency`
--
ALTER TABLE `ap_agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ap_banner`
--
ALTER TABLE `ap_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ap_category`
--
ALTER TABLE `ap_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ap_contact`
--
ALTER TABLE `ap_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ap_faqs`
--
ALTER TABLE `ap_faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ap_groups`
--
ALTER TABLE `ap_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ap_group_members`
--
ALTER TABLE `ap_group_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ap_location_city`
--
ALTER TABLE `ap_location_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ap_location_district`
--
ALTER TABLE `ap_location_district`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ap_location_ward`
--
ALTER TABLE `ap_location_ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ap_login_attempts`
--
ALTER TABLE `ap_login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ap_log_action`
--
ALTER TABLE `ap_log_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1916;

--
-- AUTO_INCREMENT for table `ap_menus`
--
ALTER TABLE `ap_menus`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `ap_newsletter`
--
ALTER TABLE `ap_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `ap_order`
--
ALTER TABLE `ap_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ap_page`
--
ALTER TABLE `ap_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ap_post`
--
ALTER TABLE `ap_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ap_product`
--
ALTER TABLE `ap_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ap_property`
--
ALTER TABLE `ap_property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ap_suggest_product`
--
ALTER TABLE `ap_suggest_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ap_system_menu`
--
ALTER TABLE `ap_system_menu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `ap_users`
--
ALTER TABLE `ap_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `ap_users_groups`
--
ALTER TABLE `ap_users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT for table `ap_video`
--
ALTER TABLE `ap_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ap_users_groups`
--
ALTER TABLE `ap_users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `ap_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `ap_users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
